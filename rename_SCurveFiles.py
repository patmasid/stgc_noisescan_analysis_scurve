#!/usr/bin/python                                                                                                                                                                                          

import sys,string,random,os,fileinput, argparse, time
import ROOT
ROOT.gROOT.SetBatch()

def options():

    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--thresholds", nargs='+',           help="Threshold above baseline + 30 mV in DAC Counts")
    parser.add_argument("--rootFileNames", nargs='+',        help="root files corresponding to the given thresholds in the same order")
    parser.add_argument("--pathtoRootFiles",                 help="path to the root files")
    return parser.parse_args()

def fatal(msg):
    sys.exit("Fatal: %s" % msg)

def rename_SCurveFiles():

    ops = options()
    if not ops.thresholds:        fatal("Please give --thresholds argument")
    if not ops.rootFileNames:     fatal("Please give --rootFileNames argument")
    if not ops.pathtoRootFiles:   fatal("Please give --pathtoRootFiles argument")

    #s_thresholds = ops.thresholds                                                                                                                                                                          
    l_thresholds = ops.thresholds
    l_rootfiles = ops.rootFileNames

    run_path = ops.pathtoRootFiles

    l_newRootFiles = []

    num_Thres = 0

    for thres in l_thresholds:

        curr_rootfile = run_path+l_rootfiles[num_Thres]

        new_rootfile = run_path+"Baseline+30mV"+thres+".root"
        l_newRootFiles.append(new_rootfile)

        os.system("cp "+curr_rootfile+" "+new_rootfile)

        num_Thres = num_Thres+1

    print("all files copied to new names.......")

    os.mkdir(run_path+"decoded_root")
    os.mkdir(run_path+"noiseRate_Vs_Channels_plots")
    os.mkdir(run_path+"SCurve")
    
    for new_rootfile in l_newRootFiles:
        
        output_rootfile_HO = new_rootfile
        output_rootfile_HO = output_rootfile_HO.replace(run_path,run_path+"decoded_root/")
        output_rootfile_HO = output_rootfile_HO.replace(".root","_HO_decoded.root")
        
        output_rootfile_IP = new_rootfile
        output_rootfile_IP = output_rootfile_IP.replace(run_path,run_path+"decoded_root/")
        output_rootfile_IP = output_rootfile_IP.replace(".root","_IP_decoded.root")
        
        os.system("source /afs/cern.ch/user/p/patmasid/public/swROD_analysis/felixdata_minitreeanalysis/setupEnv.sh")
        print("Completed sourcing /afs/cern.ch/user/p/patmasid/public/swROD_analysis/felixdata_minitreeanalysis/setupEnv.sh")
        
        print("Starting .. " + "/afs/cern.ch/user/p/patmasid/public/swROD_analysis/x86_64-centos7-gcc8-opt/felixdata_minitreeanalysis/analyzeHit "+new_rootfile+" /afs/cern.ch/user/t/tzhao/public/ElinkMapping_HO_b191.txt "+ output_rootfile_HO + " S P " + "noTP " + "no-ELinkID")
        os.system("/afs/cern.ch/user/p/patmasid/public/swROD_analysis/x86_64-centos7-gcc8-opt/felixdata_minitreeanalysis/analyzeHit "+new_rootfile+" /afs/cern.ch/user/t/tzhao/public/ElinkMapping_HO_b191.txt "+ output_rootfile_HO + " S P " + "noTP " + "no-ELinkID")
        
        print("Starting .. " + "/afs/cern.ch/user/p/patmasid/public/swROD_analysis/x86_64-centos7-gcc8-opt/felixdata_minitreeanalysis/analyzeHit "+new_rootfile+" /afs/cern.ch/user/t/tzhao/public/ElinkMapping_IP_b191.txt "+ output_rootfile_IP + " S C " + "noTP " + "no-ELinkID")
        os.system("/afs/cern.ch/user/p/patmasid/public/swROD_analysis/x86_64-centos7-gcc8-opt/felixdata_minitreeanalysis/analyzeHit "+new_rootfile+" /afs/cern.ch/user/t/tzhao/public/ElinkMapping_IP_b191.txt "+ output_rootfile_IP + " S C " + "noTP " + "no-ELinkID")
    
if __name__ == "__main__":
    rename_SCurveFiles()
