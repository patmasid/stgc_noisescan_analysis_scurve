// @Siyuan.Sun@cern.ch

#ifndef ABMapping_H
#define ABMapping_H

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <iterator>

#include <vector>
#include <map>
#include <utility>

#include <TString.h>

#include "StripChannelMap.h"
#include "PadChannelMap.h"
#include "WireChannelMap.h"

class ABMapping {

 public :
  ABMapping();
  virtual ~ABMapping(){};

  std::stringstream m_sx;

  std::string input_strip_file_raw;
  std::string input_pad_file_raw;
  std::string input_Benoit_Mapping_raw;

  bool verbose;

  StripChannelMap * Strip_Mapping;
  PadChannelMap   * Pad_Mapping;
  WireChannelMap  * Wire_Mapping;

  void LoadBenoitMapping();
  void LoadStripMapping();
  void LoadPadMapping();

  void SetVerbose(bool v);

  //---------------------------------------------//
  //---------------------------------------------//

  bool IsConnected_pFEB( int  ith_vmm, int  ith_vmm_chan,
			 bool isSmall, bool isPivot,
			 int  quadNum, int  layerNum );

  bool IsConnected_sFEB( int  ith_vmm, int  ith_vmm_chan,
                         bool isSmall, bool isPivot,
                         int  quadNum, int  layerNum );

  //--------------------------------------------//

  bool ReturnNStripsTotal( bool isSmall, bool isPivot,
			   int quadNum, int layerNum,
			   int &n_tot_strips );

  bool ReturnStripChannelNumber( int ith_vmm,  int ith_vmm_chan,
                                 bool isSmall, bool isPivot,
                                 int quadNum, int layerNum, 
				 int &stripNumConstruction,
				 int &stripNumAssembly) ;

  bool ReturnStripElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
							 bool isSmall, bool isPivot,
							 int quadNum, int layerNum, 
							 int stripNumConstruction,
							 int &stripNumAssembly) ;

  bool ReturnStripElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
                                            bool isSmall, bool isPivot,
                                            int quadNum, int layerNum,
                                            int &stripNumConstruction,
                                            int stripNumAssembly) ;

  //---------------------------------------------//

  bool ReturnNWiresTotal( bool isSmall, bool isPivot,
			  int quadNum, int layerNum,
			  int &n_tot_wires ) ;

  bool ReturnWireChannelNumber( int ith_vmm,  int ith_vmm_chan,
				bool isSmall, bool isPivot,
				int quadNum, int layerNum, 
				int &wireNumConstruction,
				int &wireNumAssembly) ;

  bool ReturnWireElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
							bool isSmall, bool isPivot,
							int quadNum, int layerNum, 
							int wireNumConstruction,
							int &wireNumAssembly) ;
  
  bool ReturnWireElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
						    bool isSmall, bool isPivot,
						    int quadNum, int layerNum,
						    int &wireNumConstruction,
						    int wireNumAssembly) ;


  //---------------------------------------------//

  bool ReturnNPadPerRow( bool isSmall, bool isPivot,
                         int quadNum, int layerNum,
                         int &n_pads_per_row ) ;

  bool ReturnNPadsTotal( bool isSmall, bool isPivot,
                         int quadNum, int layerNum,
                         int &n_tot_pads ) ;

  bool ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
                               bool isSmall, bool isPivot,
                               int quadNum, int layerNum, 
			       int &padNumConstruction,
			       int &padNumAssembly) ;

  bool ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
                               bool isSmall, bool isPivot,
                               int quadNum, int layerNum, 
			       int &padXConstruction, int &padYConstruction, 
                               int &padXAssembly,     int &padYAssembly,
			       int &n_pads_per_row ) ;


  bool ReturnPadElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
						       bool isSmall, bool isPivot,
						       int quadNum, int layerNum, 
						       int padXConstruction, int padYConstruction,
						       int &padXAssembly,     int &padYAssembly ) ;

  bool ReturnPadElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
						   bool isSmall, bool isPivot,
						   int quadNum, int layerNum, 
						   int &padXConstruction, int &padYConstruction,
						   int padXAssembly,     int padYAssembly ) ;

  bool ReturnPadElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
						       bool isSmall, bool isPivot,
						       int quadNum, int layerNum, 
						       int padNumConstruction,
						       int &padNumAssembly) ;
  

  bool ReturnPadElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
						   bool isSmall, bool isPivot,
						   int quadNum, int layerNum, 
						   int &padNumConstruction,
						   int padNumAssembly) ;

  //---------------------------------------------------------//

  std::pair< std::vector<int>, std::vector<int> > NumChan_perVmm_PhysConnected(bool isSmall, bool isPivot,
									       int quadNum, int layerNum, bool ispFEB,
									       bool isPad, int total_chan_perQuad_perLayer);
  
  
};

#endif
