// @Siyuan.Sun@cern.ch
#ifndef StripChannelMap_H
#define StripChannelMap_H

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <vector>
#include <utility>
#include <map>

#include <iterator>

class StripChannelMap {

 public :
  StripChannelMap();
  virtual ~StripChannelMap(){};

  std::map<std::string, std::map<std::pair<int,int>,std::pair<int,int>> > DetectorStripMapping;
  std::map<std::string, int>                                              DetectorNStripsTotal;

  std::stringstream m_sx;
  bool verbose;

  void Init();
  void Clear();
  
  void SetVerbose(bool v);

  bool SetNStripChannelMap( bool isSmall, bool isPivot,
			    int quadNum, int layerNum,
			    int n_tot_strips );

  bool SetStripChannelMap( int ith_vmm,  int ith_vmm_chan,
			   bool isSmall, bool isPivot,
			   int quadNum, int layerNum, 
			   int stripNumConstruction, int stripNumAssembly );
  
  bool ReturnNStripsTotal( bool isSmall, bool isPivot,
			   int quadNum, int layerNum,
			   int &n_tot_strips ) ;

  bool ReturnStripChannelNumber( int ith_vmm,  int ith_vmm_chan,
				 bool isSmall, bool isPivot,
				 int quadNum, int layerNum, 
				 int &stripNumConstruction, int &stripNumAssembly  ) ;
  
  bool ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
						    bool isSmall, bool isPivot,
						    int quadNum, int layerNum, 
						    int stripNumConstruction, int &stripNumAssembly ) ;
  
  bool ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
						bool isSmall, bool isPivot,
						int quadNum, int layerNum,
						int &stripNumConstruction, int stripNumAssembly ) ;

};

#endif
