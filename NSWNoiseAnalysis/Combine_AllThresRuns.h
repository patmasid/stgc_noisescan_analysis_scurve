#ifndef Combine_AllThresRuns_H
#define Combine_AllThresRuns_H

#include <iostream>
#include <fstream>
#include <cmath>
#include "Variables.h"
#include "AnalyzeNoiseThres.h"
#include "ThresholdRunInfo.h"
#include "Combine_AllThresRuns.h"
#include "ABMapping.h"
#include "PadChannelMap.h"
#include "WireChannelMap.h"
#include "StripChannelMap.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include <vector>
#include "TGraph.h"
#include <fstream>
#include "TCanvas.h"
#include "TLegend.h"
#include <map>

#include "TChain.h"
#include "TString.h"

#include "TProfile.h"
#include <TStyle.h>
#include <TLine.h>

class Combine_AllThresRuns{
    
 public:

  Combine_AllThresRuns(const char *commonName, const char *inputRunName, const char *decodedRunName, const char* outputRunName, const char* outputSCurve, const char *inputThres, const char *SorL, const char *PorC, const char *sisELinkID, const char *sisswROD);
    ~Combine_AllThresRuns();
    void InitPlots();
    void InitDirectories();
    void PlotSCurves();
    void SavePlots();
    void End();
    void SetAttGraph(TGraph & graph, Size_t MarkerSize, TCanvas & Canvas);
    void OverlaySCurves(std::vector<TGraph *> v_graphs, TCanvas & OverlayCanvas);
    
    AnalyzeNoiseThres * AnalyzeNoiseThres_Obj_;
    
    //std::string s_inputRunName_, s_decodedRunName_, s_outputRunName_, s_inputThres_;
    
    ABMapping * m_ABMap;
    
    std::string s_SorL, s_PorC;
    
    bool isSmall, isPivot;
    bool isELinkID;
    bool isswROD;
    
    std::string s_isELinkID;
    std::string s_isswROD;
    
    TFile * outputFile;
    std::string s_commonName, s_inputRunName, s_decodedRunName, s_outputRunName, s_outputSCurve, s_SCurveDir;
    
    int numThresAboveBaseline;
    
    int total_quads;
    int total_layers;
    int total_types_FEBs;
    int total_vmms_per_sFEB;
    int total_vmms_per_pFEB;
    int total_chans_pervmm;

    std::vector<Color_t> VMM_Colors;

    std::vector<int> total_PhysicalChannels_Strip;

    std::vector<std::vector<int>> total_PhysicalChannels_Wire;
    std::vector<std::vector<int>> total_PhysicalChannels_Pad;
    
    std::vector<ThresholdRunInfo *> v_ThresholdRunInfo_Obj_;
    
    std::vector<int> v_ThresAboveBaseline_;

    //================ SCurves saved per channel
    std::vector<std::vector<std::vector<std::vector<std::vector< TGraph *>>>>> g_SCurves_perElecChannel;
    std::vector<std::vector<std::vector<std::vector<std::vector< TCanvas *>>>>> c_SCurves_perElecChannel;
    std::vector<std::vector<std::vector< TGraph * >>> g_Strip_SCurves_perPhysChannel;
    std::vector<std::vector<std::vector< TCanvas * >>> c_Strip_SCurves_perPhysChannel;
    std::vector<std::vector<std::vector< TGraph * >>> g_Wire_SCurves_perPhysChannel;
    std::vector<std::vector<std::vector< TCanvas * >>> c_Wire_SCurves_perPhysChannel;
    std::vector<std::vector<std::vector< TGraph * >>> g_Pad_SCurves_perPhysChannel;
    std::vector<std::vector<std::vector< TCanvas * >>> c_Pad_SCurves_perPhysChannel;
    
    //================ SCurves overlayed 
    std::vector<std::vector<std::vector<std::vector< TCanvas * >>>> c_Overlay_SCurves_perVmm;
    std::vector<std::vector< TCanvas * >> c_Overlay_SCurves_Strips;
    std::vector<std::vector< TCanvas * >> c_Overlay_SCurves_Pads;
    std::vector<std::vector<std::vector< TCanvas * >>> c_Overlay_SCurves_perFEB_unconnected;
    
    std::vector<std::string> quadDirName;
    std::vector< std::vector<std::string>> LayerDirName;

    std::vector< std::vector< std::vector <std::string>>> febTypeDirName;
    std::vector< std::vector< std::vector< std::vector< std::string>>>> vmmDirName;
    std::vector< std::vector< std::vector< std::vector< std::string>>>> SCurves_ElecChanDirName, OverlaySCurevs_DirName;
    
    std::vector< std::vector<std::string>> StripDirName, WireDirName, PadDirName;
    std::vector< std::vector<std::string>> StripSCurves_PhysChanDirName, WireSCurves_PhysChanDirName, PadSCurves_PhysChanDirName;
    

};


#endif
