#ifndef AnalyzeNoiseThres_H
#define AnalyzeNoiseThres_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include "Variables.h"
#include "ThresholdRunInfo.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include <vector>
#include "TGraph.h"
#include <fstream>
#include "TCanvas.h"
#include "TLegend.h"
#include <map>

#include "TChain.h"
#include "TString.h"

#include "TProfile.h"
#include <TStyle.h>
#include <TLine.h>
#include "ABMapping.h"

//using namespace std;

class AnalyzeNoiseThres{

 public:
  AnalyzeNoiseThres(const char *inputRunName, const char *decodedRunName, const char* outputRunName, const char *inputThres, bool isSmall, bool isPivot, bool isELinkID, bool isswROD);
    ~AnalyzeNoiseThres();
    /*Bool_t   FillChain(TChain *chain, const char *inputFileList);
    Long64_t LoadTree(Long64_t entry);
    void     InitMapHistograms(); //, const char* VMMidMapFile);                                                                                  
    void     CreateHistDir();*/
    void     Init();
    void     EventLoop();
    void     End();
    void     SaveHistograms();
    void     PlotHists();

    int total_quads;
    int total_layers;
    int total_types_FEBs;
    int total_vmms_per_sFEB;
    int total_vmms_per_pFEB;
    int total_chans_pervmm;

    std::vector<int> total_PhysicalChannels_Strip;

    std::vector<std::vector<int>> total_PhysicalChannels_Wire;
    std::vector<std::vector<int>> total_PhysicalChannels_Pad;

    std::string inputRunName_, decodedRunName_, outputRunName_;
    
    std::vector<int > v_ThresAboveBaseline; 
    
    int testThresSize;
    
    std::vector<ThresholdRunInfo *> v_ThresholdRuns;

    bool isSmall_, isPivot_;
    bool isELinkID_;
    bool isswROD_;

    ABMapping * a_ABMap;
    
};
#endif
