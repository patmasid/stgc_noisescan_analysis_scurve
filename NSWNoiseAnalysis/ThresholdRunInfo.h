#ifndef ThresholdRunInfo_H
#define ThresholdRunInfo_H

#include <iostream>
#include <fstream>
#include <cmath>
#include "Variables.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include <vector>
#include "TGraph.h"
#include <fstream>
#include "TCanvas.h"
#include "TLegend.h"
#include <map>

#include "TChain.h"
#include "TString.h"

#include "TProfile.h"
#include <TStyle.h>
#include <TLine.h>
#include "ABMapping.h"

class ThresholdRunInfo : public Variables {

 public:

  ThresholdRunInfo(const char *inputHistFile, const char *inputTree, const char *outFileName, bool isSmall, bool isPivot, bool isELinkID, bool isswROD);
    ~ThresholdRunInfo();
  
    //========= Functions ================//

    Bool_t   FillChain(TChain *chain, const char *inputFileList);
    Long64_t LoadTree(Long64_t entry);
    void     InitHistograms();
    void     ClearHistograms();
    void     InitNoiseRateValues();
    void     ClearNoiseRateValues();
    void     CreateHistDir();

    //=============== Variables ==============//

    TFile *oFile, *inFile;
  
    UInt_t ThresholdAboveBaseline;

    bool b_isSmall, b_isPivot;
    bool b_isELinkID;
    bool b_isswROD;

    std::string inputFileName, inputDecodedFileName, outputFileName;
    
    bool isWholeWedge;
    
    std::vector<std:: string> Boards_Tested;

    int numElinks;

    std::map< int, int > m_ElinkToNPackets;

    //============== per layer plots ==============//

    std::vector<TH2F *> h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer;
    std::vector<TH2F *> h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer;
    std::vector<TCanvas *> c_Strip_NoiseRate_Vs_PhysChannelNo_perLayer;
    std::vector<TCanvas *> c_Pad_NoiseRate_Vs_PhysChannelNo_perLayer;
    
    std::vector<std::vector<std::vector<TH2F *>>> h_NoiseRate_Vs_ChannelNo;
    std::vector<std::vector<std::vector<TH2F *>>> h_NoiseRate_Vs_ChannelNo_pFEB_vmm0;
    std::vector<std::vector<std::vector<TH2F *>>> h_NoiseRate_Vs_ChannelNo_pFEB_vmm12;
    std::vector<std::vector<TH2F *>> h_Strip_NoiseRate_Vs_PhysChannelNo;
    std::vector<std::vector<TH2F *>> h_Wire_NoiseRate_Vs_PhysChannelNo;
    std::vector<std::vector<TH2F *>> h_Pad_NoiseRate_Vs_PhysChannelNo;
    
    std::vector<std::vector<std::vector<TCanvas *>>> c_NoiseRate_Vs_ChannelNo;
    std::vector<std::vector<std::vector<TCanvas *>>> c_NoiseRate_Vs_ChannelNo_pFEB_vmm0;
    std::vector<std::vector<std::vector<TCanvas *>>> c_NoiseRate_Vs_ChannelNo_pFEB_vmm12;
    std::vector<std::vector<TCanvas *>> c_Strip_NoiseRate_Vs_PhysChannelNo;
    std::vector<std::vector<TCanvas *>> c_Wire_NoiseRate_Vs_PhysChannelNo;
    std::vector<std::vector<TCanvas *>> c_Pad_NoiseRate_Vs_PhysChannelNo;

    int total_quads;
    int total_layers;
    int total_types_FEBs;
    int total_vmms_per_sFEB;
    int total_vmms_per_pFEB;
    int total_chans_pervmm;
    
    ABMapping *t_ABMap;

    std::vector<int> total_PhysicalChannels_Strip;

    std::vector<std::vector<int>> total_PhysicalChannels_Wire;
    std::vector<std::vector<int>> total_PhysicalChannels_Pad;

    TChain *tree, *tree_pac;

    int NL1Packets;
    
    //============= creating directories ===========//                                              
    std::vector<std::string> PerlayerPlotsDirName;
    std::vector<std::string> quadDirName;
    std::vector< std::vector<std::string>> LayerDirName;
    std::vector< std::vector< std::vector <std::string>>> febTypeDirName;
    std::vector< std::vector< std::vector <std::string>>> NoiseRate_Vs_ChannelNo_DirName, NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName, NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName;

    std::vector< std::vector<std::string>> StripDirName, WireDirName, PadDirName, Strip_NoiseRate_Vs_PhysChannelNo_DirName, Wire_NoiseRate_Vs_PhysChannelNo_DirName, Pad_NoiseRate_Vs_PhysChannelNo_DirName;

    //============== Saving Noise Rates for every Channel ===================//
    
    std::vector<std::vector<std::vector<std::vector<std::vector<double >>>>> v_NoiseRate_ElectronicChan;
    std::vector<std::vector<std::vector<double>>> v_NoiseRate_Strip_PhysChan;
    std::vector<std::vector<std::vector<double>>> v_NoiseRate_Wire_PhysChan;
    std::vector<std::vector<std::vector<double>>> v_NoiseRate_Pad_PhysChan;

};
#endif
