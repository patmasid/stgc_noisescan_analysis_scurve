// @Siyuan.Sun@cern.ch
#ifndef WireChannelMap_H
#define WireChannelMap_H

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <vector>
#include <utility>
#include <map>

#include <iterator>

class WireChannelMap {

 public :
  WireChannelMap();
  virtual ~WireChannelMap(){};

  std::map<std::string, std::map<std::pair<int,int>,std::pair<int,int>> > DetectorWireMapping;
  std::map<std::string, int>                                              DetectorNWiresTotal;

  std::stringstream m_sx;
  bool verbose;

  void Init();
  void Clear();
  
  void SetVerbose(bool v);

  bool SetNWireChannelMap( bool isSmall, bool isPivot,
			   int quadNum, int layerNum,
			   int n_tot_wires );

  bool SetWireChannelMap( int ith_vmm,  int ith_vmm_chan,
			  bool isSmall, bool isPivot,
			  int quadNum, int layerNum, 
			  int wireNumConstruction, int wireNumAssembly );
  
  bool ReturnNWiresTotal( bool isSmall, bool isPivot,
			  int quadNum, int layerNum,
			  int &n_tot_wires ) ;

  bool ReturnWireChannelNumber( int ith_vmm,  int ith_vmm_chan,
				bool isSmall, bool isPivot,
				int quadNum, int layerNum, 
				int &wireNumConstruction,
				int &wireNumAssembly) ;
  
  bool ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
						    bool isSmall, bool isPivot,
						    int quadNum, int layerNum, 
						    int wireNumConstruction,
						    int &wireNumAssembly) ;
  

  bool ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
						bool isSmall, bool isPivot,
						int quadNum, int layerNum,
						int &wireNumConstruction,
						int wireNumAssembly) ;
  
};

#endif
