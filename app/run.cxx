#define run_cxx

#include <iostream>
#include <vector>
#include <cstring>
#include "NSWNoiseAnalysis/AnalyzeNoiseThres.h"
#include "NSWNoiseAnalysis/Combine_AllThresRuns.h"
#include "NSWNoiseAnalysis/Variables.h"
//#include "NSWNoiseAnalysis/ElinkFEBFiberMap.h"
#include <cmath>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <algorithm>
//#include <TH2.h>                                                                                              
#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <array>

#include <iomanip>
#include "THStack.h"
#include <math.h>

//#include "TApplication.h"
#include <TSystem.h>

using namespace std;

int main(int argc, char* argv[])
{

  if (argc < 11) {
    cerr << "Please give 10 arguments: " << " Common name, " <<" input TTree root file, " <<  "input histogram file, " <<" outputFileName, " << " output SCurve File name," << " input thres values, " << " SmallOrLarge, " << " PivotOrConfirm, " << " ELinkID or no-ELinkID (1 or 0), " << "swrod or not-swrod(netio) 1 or 0)" << endl;
    return -1;
  }
  const char *commonName       = argv[1];
  const char *inputTTree       = argv[2];
  const char *inputHistFile    = argv[3];
  const char *outFileName      = argv[4];
  const char *outFileSCurve    = argv[5];
  const char *inputThres       = argv[6];
  const char *SmallOrLarge     = argv[7];
  const char *PivotOrConfirm   = argv[8];
  const char *isELinkID        = argv[9];
  const char *isswROD          = argv[10];

  Combine_AllThresRuns ana(commonName, inputTTree, inputHistFile, outFileName, outFileSCurve, inputThres, SmallOrLarge, PivotOrConfirm, isELinkID, isswROD);
  
  ana.InitPlots();
  ana.InitDirectories();
  ana.PlotSCurves();
  ana.SavePlots();
  ana.End();
  
  return 0;
}
