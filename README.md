# NSWSTGCMapping

* Create a work directory and create following ```CMakeLists.txt```
$ cmake_config

```bash
mkdir noisescan_Analysis
cd noisescan_Analysis
git clone < url >
source ./stgc_noiseData_analysis/setupEnv.sh
printf "cmake_minimum_required(VERSION 3.4.3)\nfind_package(TDAQ)\ninclude(CTest)\ntdaq_project(NSWDAQ 1.0.0 USES tdaq 8.2.0)\n" > CMakeLists.txt
```

* Build the package

cmake_config   # Create build configuration
cd $CMTCONFIG  # Go to the folder such as x86_64-centos7-gcc8-opt/
make -j        # Build all the programs and libraries

# Some function illustration 
(include directory has all the header files
src directory has all the .cxx files and also the mapping files)

# Master Branch

# How to execute: 

```bash
cd stgc_noiseData_analysis
source setupEnv.sh
cd noisescan_Analysis/x86_64-centos7-gcc8-opt/
make
```

(It creates an executable called analyzeHit in noisescan_Analysis/x86_64-centos7-gcc8-opt/stgc_noiseData_analysis/)

to Execute (8 arguments):

```bash
noisescan_Analysis/x86_64-centos7-gcc8-opt/stgc_noiseData_analysis/analyzeHit 

1. path_to_the_input_minitree root files: for example if file names are Baseline+30mV+10.root (other names are similar to this. Instead of +10 something else like -20, -5, +5, etc) then this argument should be 
Argument 1 <path to the minitree files>/Baseline+30mV
  
2. path_to_input_decoded (using https://gitlab.cern.ch/patmasid/felixdata_minitreeanalysis) root files: for example if file names are Baseline+30mV+10_decoded.root (other names are similar to this. Instead of +10 something else like -20, -5, +5, etc) then this argument should be
Argument 2 <path to the files>/decoded_root/Baseline+30mV 

(Please save the files with *_decoded.root)

3. path_to_noiserateplots (using https://gitlab.cern.ch/patmasid/felixdata_minitreeanalysis) root files: for example if file names are Baseline+30mV+10_decoded.root (other names are similar to this. Instead of +10 something else like -20, -5, +5, etc) then this argument should be
Argument 3 <path to the files>/noiseratePlots/Baseline+30mV

4. path_to_SCurves (using https://gitlab.cern.ch/patmasid/felixdata_minitreeanalysis) root files: for example if file names are Baseline+30mV+10_decoded.root (other names are similar to this. Inst\
ead of +10 something else like -20, -5, +5, etc) then this argument should be
Argument 4 <path to the files>/SCurve/Baseline+30mV

5. string_of_threshold_values (e.g. -20,-10,-5,0,5,10 or 2,4,6,8,,10,20,30)

6. S or L (Small or Large wedge)

7. P or C (Pivot(HO) or Confirm Wedge(IP))
   
8. 0 or 1 - ELinkID or no-ELinkID (if ELinkID branch is stored or not)

```