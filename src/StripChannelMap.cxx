// @Siyuan.Sun@cern.ch
#include "NSWNoiseAnalysis/StripChannelMap.h"

using namespace std;

StripChannelMap::StripChannelMap(){
  Init();
  verbose = false;
}

void StripChannelMap::SetVerbose(bool v) {
  verbose = v;
}

void StripChannelMap::Init() {

  std::map<std::pair<int, int>, std::pair<int,int>> ichan_map;

  for (uint ivmm=0; ivmm < 8; ivmm++ ){
    for( uint ichan = 0; ichan< 64; ichan++ ) {
      ichan_map.insert( std::make_pair(std::make_pair(ivmm, ichan), std::make_pair(-1,-1)) );
    }
  }

  DetectorStripMapping.insert(std::make_pair("QS1P1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS1P2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS1P3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS1P4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2P1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2P2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2P3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2P4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3P1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3P2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3P3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3P4", ichan_map));
  
  DetectorStripMapping.insert(std::make_pair("QL1P1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL1P2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL1P3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL1P4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2P1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2P2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2P3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2P4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3P1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3P2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3P3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3P4", ichan_map));
  
  DetectorStripMapping.insert(std::make_pair("QS1C1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS1C2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS1C3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS1C4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2C1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2C2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2C3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS2C4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3C1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3C2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3C3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QS3C4", ichan_map));
  
  DetectorStripMapping.insert(std::make_pair("QL1C1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL1C2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL1C3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL1C4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2C1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2C2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2C3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL2C4", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3C1", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3C2", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3C3", ichan_map));
  DetectorStripMapping.insert(std::make_pair("QL3C4", ichan_map));
  
  //------------------------------------------------------//

  DetectorNStripsTotal.insert( std::make_pair("QS1P1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS1P2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS1P3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS1P4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2P1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2P2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2P3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2P4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3P1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3P2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3P3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3P4", -1) );

  DetectorNStripsTotal.insert( std::make_pair("QL1P1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL1P2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL1P3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL1P4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2P1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2P2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2P3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2P4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3P1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3P2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3P3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3P4", -1) );

  //------------------------------------------------------//

  DetectorNStripsTotal.insert( std::make_pair("QS1C1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS1C2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS1C3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS1C4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2C1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2C2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2C3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS2C4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3C1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3C2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3C3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QS3C4", -1) );

  DetectorNStripsTotal.insert( std::make_pair("QL1C1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL1C2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL1C3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL1C4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2C1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2C2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2C3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL2C4", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3C1", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3C2", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3C3", -1) );
  DetectorNStripsTotal.insert( std::make_pair("QL3C4", -1) );

}

void StripChannelMap::Clear() {

  for (uint ivmm=0; ivmm < 8; ivmm++ ){
    for( uint ichan = 0; ichan< 64; ichan++ ) {

      DetectorStripMapping["QS1P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS1P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS1P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS1P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

      DetectorStripMapping["QL1P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL1P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL1P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL1P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

      DetectorStripMapping["QS1C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS1C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS1C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS1C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS2C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QS3C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

      DetectorStripMapping["QL1C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL1C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL1C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL1C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL2C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorStripMapping["QL3C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

    }
  }

  //------------------------------//

  DetectorNStripsTotal["QS1P1"] = -1;
  DetectorNStripsTotal["QS1P2"] = -1;
  DetectorNStripsTotal["QS1P3"] = -1;
  DetectorNStripsTotal["QS1P4"] = -1;
  DetectorNStripsTotal["QS2P1"] = -1;
  DetectorNStripsTotal["QS2P2"] = -1;
  DetectorNStripsTotal["QS2P3"] = -1;
  DetectorNStripsTotal["QS2P4"] = -1;
  DetectorNStripsTotal["QS3P1"] = -1;
  DetectorNStripsTotal["QS3P2"] = -1;
  DetectorNStripsTotal["QS3P3"] = -1;
  DetectorNStripsTotal["QS3P4"] = -1;

  DetectorNStripsTotal["QL1P1"] = -1;
  DetectorNStripsTotal["QL1P2"] = -1;
  DetectorNStripsTotal["QL1P3"] = -1;
  DetectorNStripsTotal["QL1P4"] = -1;
  DetectorNStripsTotal["QL2P1"] = -1;
  DetectorNStripsTotal["QL2P2"] = -1;
  DetectorNStripsTotal["QL2P3"] = -1;
  DetectorNStripsTotal["QL2P4"] = -1;
  DetectorNStripsTotal["QL3P1"] = -1;
  DetectorNStripsTotal["QL3P2"] = -1;
  DetectorNStripsTotal["QL3P3"] = -1;
  DetectorNStripsTotal["QL3P4"] = -1;

  DetectorNStripsTotal["QS1C1"] = -1;
  DetectorNStripsTotal["QS1C2"] = -1;
  DetectorNStripsTotal["QS1C3"] = -1;
  DetectorNStripsTotal["QS1C4"] = -1;
  DetectorNStripsTotal["QS2C1"] = -1;
  DetectorNStripsTotal["QS2C2"] = -1;
  DetectorNStripsTotal["QS2C3"] = -1;
  DetectorNStripsTotal["QS2C4"] = -1;
  DetectorNStripsTotal["QS3C1"] = -1;
  DetectorNStripsTotal["QS3C2"] = -1;
  DetectorNStripsTotal["QS3C3"] = -1;
  DetectorNStripsTotal["QS3C4"] = -1;

  DetectorNStripsTotal["QL1C1"] = -1;
  DetectorNStripsTotal["QL1C2"] = -1;
  DetectorNStripsTotal["QL1C3"] = -1;
  DetectorNStripsTotal["QL1C4"] = -1;
  DetectorNStripsTotal["QL2C1"] = -1;
  DetectorNStripsTotal["QL2C2"] = -1;
  DetectorNStripsTotal["QL2C3"] = -1;
  DetectorNStripsTotal["QL2C4"] = -1;
  DetectorNStripsTotal["QL3C1"] = -1;
  DetectorNStripsTotal["QL3C2"] = -1;
  DetectorNStripsTotal["QL3C3"] = -1;
  DetectorNStripsTotal["QL3C4"] = -1;

}

bool StripChannelMap::SetNStripChannelMap( bool isSmall, bool isPivot,
					   int quadNum, int layerNum,
					   int n_tot_strips ) {

  bool is_okay = true;

  //-----------------------------------------------//
  //-----------------------------------------------//

  if ( !( quadNum >= 1 && quadNum <= 3 ) ) {
    is_okay = false;
  }

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
  }

  //---------------------------------------------//

  if ( !( n_tot_strips >= 1 ) ) {
    is_okay = false;
  }

  //--------------------------------------------//

  //-------------------------------------------------------------//                                                                                                                                     
  //      Swap 1 & 4, 2 & 3 layer number for confirm wedge                                                                                                                                              
  //   Because Benoit saves gas gap number and not layer number                                                                                                                                         
  //-------------------------------------------------------------//                                                                                                                                     
  if(isPivot == false){
    if      ( layerNum == 1 ) layerNum = 4;
    else if ( layerNum == 2 ) layerNum = 3;
    else if ( layerNum == 3 ) layerNum = 2;
    else if ( layerNum == 4 ) layerNum = 1;
    else {
      std::cout << "FATAL: No such layer number : " << layerNum << std::endl;
    }
  }

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorNStripsTotal.find(m_sx.str()) != DetectorNStripsTotal.end() ) {
    DetectorNStripsTotal [m_sx.str()] = n_tot_strips;
  }

  return is_okay;

}

bool StripChannelMap::SetStripChannelMap( int ith_vmm,  int ith_vmm_chan,
					  bool isSmall, bool isPivot,
					  int quadNum, int layerNum, 
					  int stripNumConstruction, int stripNumAssembly ) {
  
  bool is_okay = true;

  int ichan = -1;

  int n_tot_strips;
  is_okay = ReturnNStripsTotal( isSmall, isPivot,
				quadNum, layerNum,
				n_tot_strips );

  if ( !is_okay ) {
    std::cout << "Error: cannot find n total strips " << std::endl;
    return is_okay;
  }

  //-----------------------------------------------//

  if ( !( ith_vmm >= 0 && ith_vmm <= 7 ) ) {
    is_okay = false;
  }

  if ( ith_vmm_chan >=  0 ) ichan = ith_vmm_chan % 64;

  if ( !( ichan >= 0 && ichan <= 63 ) ) {
    is_okay = false;
  }

  //-----------------------------------------------//
  
  if ( !( quadNum >= 1 && quadNum <= 3 ) ) {
    is_okay = false;
  }

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
  }

  //---------------------------------------------//

  if ( !( n_tot_strips >= 1 ) ) {
    is_okay = false;
  }

  if ( !( stripNumConstruction >= 1 && stripNumConstruction <= n_tot_strips ) && 
       stripNumConstruction != -1 ) {
    is_okay = false;
  }
  if ( !( stripNumAssembly >= 1     && stripNumAssembly <= n_tot_strips ) && 
       stripNumAssembly   != -1 ) {
    is_okay = false;
  }

  //--------------------------------------------//

  if ( !is_okay ) {
    std::cout << "Mapping Failed" << std::endl;
    std::cout << "ivmm " << ith_vmm << " ichan " << ith_vmm_chan 
	      << " Quad " << quadNum << " Layer " << layerNum 
	      << " strip ( construction conv.) " << stripNumConstruction
	      << " strip ( assembly conv. ) " << stripNumAssembly
	      << " n_tot_strips " << n_tot_strips << std::endl;
  }
  else {

    if ( verbose ) {
      std::cout << "Setting Strip Channel: ivmm " << ith_vmm << " ith_vmm_chan " << ichan;
      if ( isSmall ) std::cout << " QS ";
      else           std::cout << " QL ";
      std::cout << quadNum;
      if ( isPivot ) std::cout << " P ";
      else           std::cout << " C ";
      std::cout << layerNum;
      std::cout << " strip (construction conv.) " << stripNumConstruction
		<< " strip (assembly conv.) "     << stripNumAssembly << std::endl;
    }

    m_sx.str("");

    if ( isSmall ) m_sx << "QS" << quadNum;
    else           m_sx << "QL" << quadNum;
    if ( isPivot ) m_sx << "P"  << layerNum;
    else           m_sx << "C"  << layerNum;

    if ( DetectorStripMapping.find(m_sx.str()) != DetectorStripMapping.end() ) {
      DetectorStripMapping [m_sx.str()][ std::make_pair(ith_vmm, ichan) ] = 
	std::make_pair(stripNumConstruction, stripNumAssembly);
    }
    if ( DetectorNStripsTotal.find(m_sx.str()) != DetectorNStripsTotal.end() ) {
      DetectorNStripsTotal [m_sx.str()] = n_tot_strips;
    }
    else {
      std::cout << m_sx.str().c_str() << " not found " << std::endl;
    }
  }

  //------------------------------------------//

  return is_okay;

}

bool StripChannelMap::ReturnNStripsTotal( bool isSmall, bool isPivot,
					  int quadNum, int layerNum,
					  int &n_tot_strips ) {

  bool found = false;

  n_tot_strips = -1;

  m_sx.str("");
  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorNStripsTotal.find(m_sx.str()) != DetectorNStripsTotal.end() ) {
    if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;
    n_tot_strips = DetectorNStripsTotal[m_sx.str()];
    found = true;
  }

  return found;

}

bool StripChannelMap::ReturnStripChannelNumber( int ith_vmm,  int ith_vmm_chan,
						bool isSmall, bool isPivot,
						int quadNum, int layerNum, 
						int &stripNumConstruction, int &stripNumAssembly ) {
  
  bool foundNum = false;

  std::map<std::pair<int,int>, std::pair<int,int>> ichan_map;

  m_sx.str("");
  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( verbose ) std::cout << "finding " << m_sx.str().c_str() << std::endl;

  if ( DetectorStripMapping.find(m_sx.str()) != DetectorStripMapping.end() ) {

    if ( verbose ) {
      std::cout << m_sx.str().c_str() << " found \n"
		<< "strip number: " 
		<< DetectorStripMapping[m_sx.str()][ std::make_pair(ith_vmm, ith_vmm_chan) ].first  << " (construction conv.) "
		<< DetectorStripMapping[m_sx.str()][ std::make_pair(ith_vmm, ith_vmm_chan) ].second << " (assembly conv.) " << std::endl;
    }

    ichan_map = DetectorStripMapping[m_sx.str()] ;

    stripNumConstruction = ichan_map[ std::make_pair(ith_vmm, ith_vmm_chan) ].first;
    stripNumAssembly     = ichan_map[ std::make_pair(ith_vmm, ith_vmm_chan) ].second;
    
    foundNum = true;

    
  }

  return ( foundNum );

}

bool StripChannelMap::ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
							       bool isSmall, bool isPivot,
							       int quadNum, int layerNum, 
							       int &stripNumConstruction, int stripNumAssembly ) {

  
  bool found = false;

  std::map<std::pair<int,int>, int> ichan_map;

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  for ( int ivmm=0; ivmm < 8; ivmm++ ) {
    for ( int ich=0; ich < 64; ich++ ) {
      
      int istripAssembly = -1;
      int istripConstruction = -1;

      bool ifound = ReturnStripChannelNumber( ivmm, ich,
					      isSmall, isPivot,
					      quadNum, layerNum,
					      istripConstruction, istripAssembly );

      if ( ifound && istripAssembly == stripNumAssembly ) {
	ith_vmm = ivmm;
	ith_vmm_chan = ich;
	stripNumConstruction = istripConstruction;
	found = true;
	break;
      }
    } // loop over chan

    if ( found ) break;

  }

  if ( !found ) {
    ith_vmm = -1;
    ith_vmm_chan = -1;
    stripNumConstruction = -1;
  }

  return found;

}

bool StripChannelMap::ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
								   bool isSmall, bool isPivot,
								   int quadNum, int layerNum,
								   int stripNumConstruction, int &stripNumAssembly ) {

  bool found = false;

  std::map<std::pair<int,int>, int> ichan_map;

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  for ( int ivmm=0; ivmm < 8; ivmm++ ) {
    for ( int ich=0; ich < 64; ich++ ) {

      int istripAssembly = -1;
      int istripConstruction = -1;

      bool ifound = ReturnStripChannelNumber( ivmm, ich,
                                              isSmall, isPivot,
                                              quadNum, layerNum,
                                              istripConstruction, istripAssembly );

      if ( ifound && istripConstruction == stripNumConstruction ) {
        ith_vmm = ivmm;
        ith_vmm_chan = ich;
	stripNumAssembly = istripAssembly;
        found = true;
        break;
      }
    } // loop over chan                                                                                                                                                          

    if ( found ) break;

  }

  if ( !found ) {
    ith_vmm = -1;
    ith_vmm_chan = -1;
    stripNumAssembly = -1;
  }

  return found;

}


