#define ThresholdRunInfo_cxx
#include "NSWNoiseAnalysis/ThresholdRunInfo.h"

ThresholdRunInfo::ThresholdRunInfo(const char *inputHistFile, const char *inputTree, const char *outFileName, bool isSmall, bool isPivot, bool isELinkID, bool isswROD) {

    b_isSmall = isSmall;
    b_isPivot = isPivot;

    b_isELinkID = isELinkID;
    b_isswROD = isswROD;

    t_ABMap = new ABMapping();
    t_ABMap->SetVerbose(false);
    t_ABMap->LoadBenoitMapping();                                                                         

    oFile = new TFile(outFileName, "recreate");

    inFile = TFile::Open(inputHistFile);

    if(b_isswROD){
      tree_pac = new TChain("pac");
      
      if( ! FillChain(tree_pac, inputTree) ) {
        std::cerr << "Cannot get the tree " << std::endl;
      } else {
        std::cout << "Initiating the analysis for 'tree'" << std::endl;
      }
      Variables::Init(tree_pac, b_isELinkID);
      NL1Packets = tree_pac->GetEntries();
      std::cout << "NL1Packets: " << NL1Packets << std::endl;
    }
    tree = new TChain("nsw"); 

    if( ! FillChain(tree, inputTree) ) {
      std::cerr << "Cannot get the tree " << std::endl;
    } else {
      std::cout << "Initiating the analysis for 'tree_pac'" << std::endl;
    }

    Variables::Init(tree, b_isELinkID);

    std::cout << "NL1Packets: " << NL1Packets << std::endl;

    std::cout<<"ThresholdRunInfo: End of construction "<<std::endl;

}

ThresholdRunInfo::~ThresholdRunInfo() {

    for(int iL=0; iL<total_layers; iL++){

        oFile->cd(PerlayerPlotsDirName[iL].c_str());
        c_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Write("",TObject::kOverwrite);
        delete c_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL];
        delete h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL];
        
        c_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Write("",TObject::kOverwrite);
        delete c_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL];
        delete h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL];
   
    }

    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){

            oFile->cd(Strip_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());
            c_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Write("",TObject::kOverwrite);
            delete c_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            delete h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL];

            oFile->cd(Wire_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());
            c_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Write("",TObject::kOverwrite);
            delete c_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            delete h_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL];

            oFile->cd(Pad_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());
            c_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Write("",TObject::kOverwrite);                                                                                                                     
            delete c_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            delete h_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL];

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                oFile->cd(NoiseRate_Vs_ChannelNo_DirName[iQ][iL][iispFEB].c_str());
                c_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
                delete c_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB];
                delete h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB];

                if(iispFEB==1){
                    oFile->cd(NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName[iQ][iL][iispFEB].c_str());
                    c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
                    delete c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB];
                    delete h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB];

                    oFile->cd(NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName[iQ][iL][iispFEB].c_str());
                    c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
                    delete c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB];
                    delete h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB];
                }

            }

        }
    }

    inFile->Close();

    oFile->Close();

    delete oFile, inFile;

    delete tree;

    if (!fChain) return;
    delete fChain->GetCurrentFile();

    std::cout<<"ThresholdRunInfo: Destructor has been called!! "<<std::endl;

}

Bool_t ThresholdRunInfo::FillChain(TChain *chain, const char *inputFileList){

    const char *buffer;
    buffer = inputFileList;

    std::cout << "TreeUtilities : FillChain " << std::endl;
    std::cout << "Adding tree from " << buffer << std::endl;
    chain->Add(buffer);

    std::cout << "No. of Entries in this tree : " << chain->GetEntries() << std::endl;
    return kTRUE;
}

Long64_t ThresholdRunInfo::LoadTree(Long64_t entry) {
    // Set the environment to read one entry                                                                                                                                 
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (!fChain->InheritsFrom(TChain::Class()))  return centry;
    TChain *chain = (TChain*)fChain;
    if (chain->GetTreeNumber() != fCurrent) {
        fCurrent = chain->GetTreeNumber();
        //    Notify();                                                           
    }
    return centry;
}

void ThresholdRunInfo::ClearNoiseRateValues(){
    
    v_NoiseRate_ElectronicChan.resize(0);
    v_NoiseRate_Strip_PhysChan.resize(0);
    v_NoiseRate_Wire_PhysChan.resize(0);
    v_NoiseRate_Pad_PhysChan.resize(0);
    
    /*std::vector<std::vector<std::vector<std::vector<double >>>> *NoiseRate_ElectronicChan_NullPtr = new std::vector<std::vector<std::vector<std::vector<double >>>>;
    std::vector<std::vector<double >> *NoiseRate_Strip_PhysChan_NullPtr = new std::vector<std::vector<double >>;
    std::vector<std::vector<double >> *NoiseRate_Wire_PhysChan_NullPtr = new std::vector<std::vector<double >>;
    std::vector<std::vector<double >> *NoiseRate_Pad_PhysChan_NullPtr = new std::vector<std::vector<double >>;

    v_NoiseRate_ElectronicChan->push_back(*NoiseRate_ElectronicChan_NullPtr);
    v_NoiseRate_Strip_PhysChan->push_back(*NoiseRate_Strip_PhysChan_NullPtr);
    v_NoiseRate_Wire_PhysChan->push_back(*NoiseRate_Wire_PhysChan_NullPtr);
    v_NoiseRate_Pad_PhysChan->push_back(*NoiseRate_Pad_PhysChan_NullPtr);*/
    
}

void ThresholdRunInfo::InitNoiseRateValues(){

    total_quads = 3;
    total_layers = 4;
    total_types_FEBs = 2;
    total_vmms_per_sFEB = 8;
    total_vmms_per_pFEB = 3;
    total_chans_pervmm = 64;

    total_PhysicalChannels_Strip.resize(total_quads);
    total_PhysicalChannels_Wire.resize(total_quads);
    total_PhysicalChannels_Pad.resize(total_quads);

    for(int iQ=0; iQ<total_quads; iQ++){

        total_PhysicalChannels_Wire[iQ].resize(total_layers);
        total_PhysicalChannels_Pad[iQ].resize(total_layers);
    }

    for(int iQ=0; iQ<total_quads; iQ++){

      for(int iL=0; iL<total_layers; iL++){

	int num_Strips, num_Wires, num_Pads;
	
	if( !(t_ABMap->ReturnNStripsTotal(b_isSmall, b_isPivot, iQ+1, iL+1, num_Strips)) ) std::cerr<<"Cannot determine no of strips for given Quad "<< iQ+1 << " and given layer " << iL+1 << std::endl;
	if( !(t_ABMap->ReturnNWiresTotal(b_isSmall, b_isPivot, iQ+1, iL+1, num_Wires)) ) std::cerr<<"Cannot determine no of wires for given Quad "<< iQ+1 << " and given layer " <<iL+1 <<std::endl;
	if( !(t_ABMap->ReturnNPadsTotal(b_isSmall, b_isPivot, iQ+1, iL+1, num_Pads)) ) std::cerr<<"Cannot determine no of pads for given Quad "<< iQ+1 << " and given layer " <<iL+1 <<std::endl;
	
	total_PhysicalChannels_Strip[iQ] = num_Strips;
	
	total_PhysicalChannels_Wire[iQ][iL] = num_Wires;
	
	total_PhysicalChannels_Pad[iQ][iL] = num_Pads;
      }
    }

    /*total_PhysicalChannels_Strip[0]=406;
    total_PhysicalChannels_Strip[1]=365;
    total_PhysicalChannels_Strip[2]=307;

    total_PhysicalChannels_Wire[0][0]=19;
    total_PhysicalChannels_Wire[1][0]=29;
    total_PhysicalChannels_Wire[2][0]=38;
    total_PhysicalChannels_Wire[0][1]=19;
    total_PhysicalChannels_Wire[1][1]=29;
    total_PhysicalChannels_Wire[2][1]=38;
    total_PhysicalChannels_Wire[0][2]=19;
    total_PhysicalChannels_Wire[1][2]=29;
    total_PhysicalChannels_Wire[2][2]=37;
    total_PhysicalChannels_Wire[0][3]=20;
    total_PhysicalChannels_Wire[1][3]=30;
    total_PhysicalChannels_Wire[2][3]=38;

    //========= P ==========

    if(b_isPivot){
        
        std::cout << "Pivot: " << b_isPivot << std::endl;

        total_PhysicalChannels_Pad[0][0]=68;
        total_PhysicalChannels_Pad[1][0]=30;
        total_PhysicalChannels_Pad[2][0]=24;
        total_PhysicalChannels_Pad[0][1]=68;
        total_PhysicalChannels_Pad[1][1]=30;
        total_PhysicalChannels_Pad[2][1]=24;
        total_PhysicalChannels_Pad[0][2]=51;
        total_PhysicalChannels_Pad[1][2]=45;
        total_PhysicalChannels_Pad[2][2]=39;
        total_PhysicalChannels_Pad[0][3]=51;
        total_PhysicalChannels_Pad[1][3]=45;
        total_PhysicalChannels_Pad[2][3]=39;

    }

    //======== C =========
    
    else if(!b_isPivot){
        
        std::cout << "Confirm: " << b_isPivot << std::endl;

        total_PhysicalChannels_Pad[0][0]=72;
        total_PhysicalChannels_Pad[1][0]=45;
        total_PhysicalChannels_Pad[2][0]=42;
        total_PhysicalChannels_Pad[0][1]=72;
        total_PhysicalChannels_Pad[1][1]=45;
        total_PhysicalChannels_Pad[2][1]=42;
        total_PhysicalChannels_Pad[0][2]=68;
        total_PhysicalChannels_Pad[1][2]=48;
        total_PhysicalChannels_Pad[2][2]=39;
        total_PhysicalChannels_Pad[0][3]=68;
        total_PhysicalChannels_Pad[1][3]=48;
        total_PhysicalChannels_Pad[2][3]=39;

    }*/

    v_NoiseRate_ElectronicChan.resize(total_quads);
    v_NoiseRate_Strip_PhysChan.resize(total_quads);
    v_NoiseRate_Wire_PhysChan.resize(total_quads);
    v_NoiseRate_Pad_PhysChan.resize(total_quads);
    
    for(int iQ=0; iQ<total_quads; iQ++){

        v_NoiseRate_ElectronicChan[iQ].resize(total_layers);
        v_NoiseRate_Strip_PhysChan[iQ].resize(total_layers);
        v_NoiseRate_Wire_PhysChan[iQ].resize(total_layers);
        v_NoiseRate_Pad_PhysChan[iQ].resize(total_layers);
        
        for(int iL=0; iL<total_layers; iL++){
            
            v_NoiseRate_ElectronicChan[iQ][iL].resize(total_types_FEBs);
            v_NoiseRate_Strip_PhysChan[iQ][iL].resize(total_PhysicalChannels_Strip[iQ]);
            v_NoiseRate_Wire_PhysChan[iQ][iL].resize(total_PhysicalChannels_Wire[iQ][iL]);
            v_NoiseRate_Pad_PhysChan[iQ][iL].resize(total_PhysicalChannels_Pad[iQ][iL]);

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                if(iispFEB==0){
                    
                    v_NoiseRate_ElectronicChan[iQ][iL][iispFEB].resize(total_vmms_per_sFEB);

                    for(int iVMM=0; iVMM<total_vmms_per_sFEB; iVMM++){
                        v_NoiseRate_ElectronicChan[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
                    }

                }
                
                else if(iispFEB==1){
                    
                    v_NoiseRate_ElectronicChan[iQ][iL][iispFEB].resize(total_vmms_per_pFEB);
                    
                    for(int iVMM=0; iVMM<total_vmms_per_pFEB; iVMM++){
                        v_NoiseRate_ElectronicChan[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
                    } 

                }
            
            }
            
        }

    }

}

void ThresholdRunInfo::InitHistograms(){

    total_PhysicalChannels_Strip.resize(total_quads);
    total_PhysicalChannels_Wire.resize(total_quads);
    total_PhysicalChannels_Pad.resize(total_quads);

    //=============== per layer plots

    h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer.resize(total_layers);
    h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer.resize(total_layers);
 
    c_Strip_NoiseRate_Vs_PhysChannelNo_perLayer.resize(total_layers);
    c_Pad_NoiseRate_Vs_PhysChannelNo_perLayer.resize(total_layers);

    h_NoiseRate_Vs_ChannelNo.resize(total_quads);
    h_NoiseRate_Vs_ChannelNo_pFEB_vmm0.resize(total_quads);
    h_NoiseRate_Vs_ChannelNo_pFEB_vmm12.resize(total_quads);

    h_Strip_NoiseRate_Vs_PhysChannelNo.resize(total_quads);
    h_Wire_NoiseRate_Vs_PhysChannelNo.resize(total_quads);
    h_Pad_NoiseRate_Vs_PhysChannelNo.resize(total_quads);

    c_NoiseRate_Vs_ChannelNo.resize(total_quads);
    c_NoiseRate_Vs_ChannelNo_pFEB_vmm0.resize(total_quads);
    c_NoiseRate_Vs_ChannelNo_pFEB_vmm12.resize(total_quads);

    c_Strip_NoiseRate_Vs_PhysChannelNo.resize(total_quads);
    c_Wire_NoiseRate_Vs_PhysChannelNo.resize(total_quads);
    c_Pad_NoiseRate_Vs_PhysChannelNo.resize(total_quads);

    PerlayerPlotsDirName.resize(total_layers);

    quadDirName.resize(total_quads);
    LayerDirName.resize(total_quads);
    febTypeDirName.resize(total_quads);
    NoiseRate_Vs_ChannelNo_DirName.resize(total_quads);
    NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName.resize(total_quads);
    NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName.resize(total_quads);
    StripDirName.resize(total_quads);
    WireDirName.resize(total_quads);
    PadDirName.resize(total_quads);
    Strip_NoiseRate_Vs_PhysChannelNo_DirName.resize(total_quads);
    Wire_NoiseRate_Vs_PhysChannelNo_DirName.resize(total_quads);
    Pad_NoiseRate_Vs_PhysChannelNo_DirName.resize(total_quads);

    for(int iQ=0; iQ<total_quads; iQ++){

        h_NoiseRate_Vs_ChannelNo[iQ].resize(total_layers);
        h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ].resize(total_layers);
        h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ].resize(total_layers);

        h_Strip_NoiseRate_Vs_PhysChannelNo[iQ].resize(total_layers);
        h_Wire_NoiseRate_Vs_PhysChannelNo[iQ].resize(total_layers);
        h_Pad_NoiseRate_Vs_PhysChannelNo[iQ].resize(total_layers);

        c_NoiseRate_Vs_ChannelNo[iQ].resize(total_layers);
        c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ].resize(total_layers);
        c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ].resize(total_layers);

        c_Strip_NoiseRate_Vs_PhysChannelNo[iQ].resize(total_layers);
        c_Wire_NoiseRate_Vs_PhysChannelNo[iQ].resize(total_layers);
        c_Pad_NoiseRate_Vs_PhysChannelNo[iQ].resize(total_layers);
        
        LayerDirName[iQ].resize(total_layers);
        febTypeDirName[iQ].resize(total_layers);
        NoiseRate_Vs_ChannelNo_DirName[iQ].resize(total_layers);
        NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName[iQ].resize(total_layers);
        NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName[iQ].resize(total_layers);
        StripDirName[iQ].resize(total_layers);
        WireDirName[iQ].resize(total_layers);
        PadDirName[iQ].resize(total_layers);
        Strip_NoiseRate_Vs_PhysChannelNo_DirName[iQ].resize(total_layers);
        Wire_NoiseRate_Vs_PhysChannelNo_DirName[iQ].resize(total_layers);
        Pad_NoiseRate_Vs_PhysChannelNo_DirName[iQ].resize(total_layers);
        
        for(int iL=0; iL<total_layers; iL++){

            h_NoiseRate_Vs_ChannelNo[iQ][iL].resize(total_types_FEBs);
            h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL].resize(total_types_FEBs);
            h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL].resize(total_types_FEBs);

            c_NoiseRate_Vs_ChannelNo[iQ][iL].resize(total_types_FEBs);
            c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL].resize(total_types_FEBs);
            c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL].resize(total_types_FEBs);

            febTypeDirName[iQ][iL].resize(total_types_FEBs);
            NoiseRate_Vs_ChannelNo_DirName[iQ][iL].resize(total_types_FEBs);
            NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName[iQ][iL].resize(total_types_FEBs);
            NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName[iQ][iL].resize(total_types_FEBs);

        }

    }

    for(int iL=0; iL<total_layers; iL++){
     
        std::string name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer, name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer;
        std::string c_name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer, c_name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer;

        name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer = "Strip_NoiseRate_Vs_PhysChannelNo_perLayer_L"+std::to_string(iL+1);
        name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer = "Pad_NoiseRate_Vs_PhysChannelNo_perLayer_L"+std::to_string(iL+1);
   
        c_name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer = "c_bl"+std::to_string(ThresholdAboveBaseline)+"Strip_NoiseRate_Vs_PhysChannelNo_perLayer_L"+std::to_string(iL+1);
        c_name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer = "c_bl"+std::to_string(ThresholdAboveBaseline)+"Pad_NoiseRate_Vs_PhysChannelNo_perLayer_L"+std::to_string(iL+1);

        h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL] = new TH2F(name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2], 0.5, total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2]+0.5, 1000, 0, 10);
        h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->SetDirectory(0);
        
        h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL] = new TH2F(name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), total_PhysicalChannels_Pad[0][iL]+total_PhysicalChannels_Pad[1][iL]+total_PhysicalChannels_Pad[2][iL], 0.5, total_PhysicalChannels_Pad[0][iL]+total_PhysicalChannels_Pad[1][iL]+total_PhysicalChannels_Pad[2][iL]+0.5, 1000,0,10);
        h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->SetDirectory(0);

        c_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL] = new TCanvas(c_name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), name_Strip_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), 800, 800);
        c_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL] = new TCanvas(c_name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), name_Pad_NoiseRate_Vs_PhysChannelNo_perLayer.c_str(), 800, 800);

    }

    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){

            std::string name_Strip_NoiseRate_Vs_PhysChannelNo, name_Wire_NoiseRate_Vs_PhysChannelNo, name_Pad_NoiseRate_Vs_PhysChannelNo, c_name_Strip_NoiseRate_Vs_PhysChannelNo, c_name_Wire_NoiseRate_Vs_PhysChannelNo, c_name_Pad_NoiseRate_Vs_PhysChannelNo;

            name_Strip_NoiseRate_Vs_PhysChannelNo = "Strip_NoiseRate_Vs_PhysChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);
            name_Wire_NoiseRate_Vs_PhysChannelNo = "Wire_NoiseRate_Vs_PhysChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);
            name_Pad_NoiseRate_Vs_PhysChannelNo = "Pad_NoiseRate_Vs_PhysChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);

            c_name_Strip_NoiseRate_Vs_PhysChannelNo = "c_bl"+std::to_string(ThresholdAboveBaseline)+"_Strip_NoiseRate_Vs_PhysChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);
            c_name_Wire_NoiseRate_Vs_PhysChannelNo = "c_bl"+std::to_string(ThresholdAboveBaseline)+"_Wire_NoiseRate_Vs_PhysChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);
            c_name_Pad_NoiseRate_Vs_PhysChannelNo = "c_bl"+std::to_string(ThresholdAboveBaseline)+"_Pad_NoiseRate_Vs_PhysChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);

            h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL] = new TH2F(name_Strip_NoiseRate_Vs_PhysChannelNo.c_str(), name_Strip_NoiseRate_Vs_PhysChannelNo.c_str(), total_PhysicalChannels_Strip[iQ], 0.5, total_PhysicalChannels_Strip[iQ]+0.5, 1000,0,10);
            h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]->SetDirectory(0);
            h_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL] = new TH2F(name_Wire_NoiseRate_Vs_PhysChannelNo.c_str(), name_Wire_NoiseRate_Vs_PhysChannelNo.c_str(), total_PhysicalChannels_Wire[iQ][iL], 0.5, total_PhysicalChannels_Wire[iQ][iL]+0.5, 1000,0,10);
            h_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]->SetDirectory(0);
            h_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL] = new TH2F(name_Pad_NoiseRate_Vs_PhysChannelNo.c_str(), name_Pad_NoiseRate_Vs_PhysChannelNo.c_str(), total_PhysicalChannels_Pad[iQ][iL], 0.5, total_PhysicalChannels_Pad[iQ][iL]+0.5, 1000,0,10);
            h_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]->SetDirectory(0);

            c_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL] = new TCanvas(c_name_Strip_NoiseRate_Vs_PhysChannelNo.c_str(), name_Strip_NoiseRate_Vs_PhysChannelNo.c_str(), 800, 800);
            c_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL] = new TCanvas(c_name_Wire_NoiseRate_Vs_PhysChannelNo.c_str(), name_Wire_NoiseRate_Vs_PhysChannelNo.c_str(), 800, 800);
            c_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL] = new TCanvas(c_name_Pad_NoiseRate_Vs_PhysChannelNo.c_str(), name_Pad_NoiseRate_Vs_PhysChannelNo.c_str(), 800, 800);

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                std::string FEBType;
                int numVMMs = -1;

                if(iispFEB==0) {FEBType = "sFEB"; numVMMs=8;}
                else if(iispFEB==1) {FEBType = "pFEB"; numVMMs=3;}
                else std::cout<<"Invalid value for ispFEB"<<std::endl;

                std::string name_NoiseRate_Vs_ChannelNo, name_NoiseRate_Vs_ChannelNo_pFEB_vmm0, name_NoiseRate_Vs_ChannelNo_pFEB_vmm12, c_name_NoiseRate_Vs_ChannelNo, c_name_NoiseRate_Vs_ChannelNo_pFEB_vmm0, c_name_NoiseRate_Vs_ChannelNo_pFEB_vmm12;
                
                if(iispFEB==1){

                    name_NoiseRate_Vs_ChannelNo_pFEB_vmm0 = "NoiseRate_Vs_ChannelNo_pFEB_vmm0_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);
                    name_NoiseRate_Vs_ChannelNo_pFEB_vmm12 = "NoiseRate_Vs_ChannelNo_pFEB_vmm12_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);

                    c_name_NoiseRate_Vs_ChannelNo_pFEB_vmm0 = "c_bl"+std::to_string(ThresholdAboveBaseline)+"_NoiseRate_Vs_ChannelNo_pFEB_vmm0_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);
                    c_name_NoiseRate_Vs_ChannelNo_pFEB_vmm12 = "c_bl"+std::to_string(ThresholdAboveBaseline)+"_NoiseRate_Vs_ChannelNo_pFEB_vmm12_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1);

                    c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB] = new TCanvas(c_name_NoiseRate_Vs_ChannelNo_pFEB_vmm0.c_str(),name_NoiseRate_Vs_ChannelNo_pFEB_vmm0.c_str(), 800, 800);
                    c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB] = new TCanvas(c_name_NoiseRate_Vs_ChannelNo_pFEB_vmm12.c_str(),name_NoiseRate_Vs_ChannelNo_pFEB_vmm12.c_str(), 800, 800);

                    h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB] = new TH2F(name_NoiseRate_Vs_ChannelNo_pFEB_vmm0.c_str(), name_NoiseRate_Vs_ChannelNo_pFEB_vmm0.c_str(), 64, -0.5, 63.5, 1000,0,10);
                    h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB]->SetDirectory(0);
                    h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB] = new TH2F(name_NoiseRate_Vs_ChannelNo_pFEB_vmm12.c_str(), name_NoiseRate_Vs_ChannelNo_pFEB_vmm12.c_str(), 128, 63.5, 191.5, 1000,0, 10);
                    h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB]->SetDirectory(0);

                    name_NoiseRate_Vs_ChannelNo = "NoiseRate_Vs_ChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType;

                    c_name_NoiseRate_Vs_ChannelNo = "c_bl"+std::to_string(ThresholdAboveBaseline)+"_NoiseRate_Vs_ChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType;

                    c_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB] = new TCanvas(c_name_NoiseRate_Vs_ChannelNo.c_str(),name_NoiseRate_Vs_ChannelNo.c_str(), 800, 800);

                    h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB] = new TH2F(name_NoiseRate_Vs_ChannelNo.c_str(), name_NoiseRate_Vs_ChannelNo.c_str(), 192, -0.5, 191.5, 1000,0,10);
                    h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB]->SetDirectory(0);

                }
                
                else if(iispFEB==0){

                    name_NoiseRate_Vs_ChannelNo = "NoiseRate_Vs_ChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType;

                    c_name_NoiseRate_Vs_ChannelNo = "c_bl"+std::to_string(ThresholdAboveBaseline)+"_NoiseRate_Vs_ChannelNo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType;

                    c_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB] = new TCanvas(c_name_NoiseRate_Vs_ChannelNo.c_str(),name_NoiseRate_Vs_ChannelNo.c_str(), 800, 800);

                    h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB] = new TH2F(name_NoiseRate_Vs_ChannelNo.c_str(), name_NoiseRate_Vs_ChannelNo.c_str(), 512, -0.5, 511.5, 1000,0,10);
                    h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB]->SetDirectory(0);
                }

            }

        }

    }
}

void ThresholdRunInfo::ClearHistograms(){

    tree = new TChain();
    
    NL1Packets = 0;

    m_ElinkToNPackets.clear();

    Boards_Tested.resize(0);
    
    numElinks=0;

    oFile = new TFile();
    inFile = new TFile();

    total_quads = 0;
    total_layers = 0;
    total_types_FEBs = 0;
    total_vmms_per_sFEB = 0;
    total_vmms_per_pFEB = 0;
    total_chans_pervmm = 0;

    total_PhysicalChannels_Strip.resize(0);
    total_PhysicalChannels_Wire.resize(0);
    total_PhysicalChannels_Pad.resize(0);

    h_NoiseRate_Vs_ChannelNo.resize(0);
    h_NoiseRate_Vs_ChannelNo_pFEB_vmm0.resize(0);
    h_NoiseRate_Vs_ChannelNo_pFEB_vmm12.resize(0);

    h_Strip_NoiseRate_Vs_PhysChannelNo.resize(0);
    h_Wire_NoiseRate_Vs_PhysChannelNo.resize(0);
    h_Pad_NoiseRate_Vs_PhysChannelNo.resize(0);

    c_NoiseRate_Vs_ChannelNo.resize(0);
    c_NoiseRate_Vs_ChannelNo_pFEB_vmm0.resize(0);
    c_NoiseRate_Vs_ChannelNo_pFEB_vmm12.resize(0);

    c_Strip_NoiseRate_Vs_PhysChannelNo.resize(0);
    c_Wire_NoiseRate_Vs_PhysChannelNo.resize(0);
    c_Pad_NoiseRate_Vs_PhysChannelNo.resize(0);
    
    quadDirName.resize(0);
    LayerDirName.resize(0);
    febTypeDirName.resize(0);
    NoiseRate_Vs_ChannelNo_DirName.resize(0);
    NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName.resize(0);
    NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName.resize(0);
    StripDirName.resize(0);
    WireDirName.resize(0);
    PadDirName.resize(0);
    Strip_NoiseRate_Vs_PhysChannelNo_DirName.resize(0);
    Wire_NoiseRate_Vs_PhysChannelNo_DirName.resize(0);
    Pad_NoiseRate_Vs_PhysChannelNo_DirName.resize(0);

}

void ThresholdRunInfo::CreateHistDir(){

    for(int iL=0; iL<total_layers; iL++){

        PerlayerPlotsDirName[iL] = "PerLayerPlots_Layer_"+std::to_string(iL+1)+"/";
        oFile->mkdir(PerlayerPlotsDirName[iL].c_str());

    }

    for(int iQ=0; iQ<total_quads; iQ++){

        //======== Creating the quadruplet directory =========//                                                                                                                                            

        quadDirName[iQ] = "Quad_"+std::to_string(iQ+1)+"/";
        oFile->mkdir(quadDirName[iQ].c_str());

        for(int iL=0; iL<total_layers; iL++){

            LayerDirName[iQ][iL] = quadDirName[iQ]+"Layer_"+std::to_string(iL+1)+"/";
            oFile->mkdir(LayerDirName[iQ][iL].c_str());

            StripDirName[iQ][iL] = LayerDirName[iQ][iL]+"Strip/";
            oFile->mkdir(StripDirName[iQ][iL].c_str());
            Strip_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL] = StripDirName[iQ][iL]+"Strip_Hits_Vs_PhysChannel/";
            oFile->mkdir(Strip_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());

            WireDirName[iQ][iL] = LayerDirName[iQ][iL]+"Wire/";
            oFile->mkdir(WireDirName[iQ][iL].c_str());
            Wire_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL] = WireDirName[iQ][iL]+"Wire_Hits_Vs_PhysChannel/";
            oFile->mkdir(Wire_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());

            PadDirName[iQ][iL] = LayerDirName[iQ][iL]+"Pad/";
            oFile->mkdir(PadDirName[iQ][iL].c_str());
            Pad_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL] = PadDirName[iQ][iL]+"Pad_Hits_Vs_PhysChannel/";
            oFile->mkdir(Pad_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                std::string FEBType;
                if(iispFEB==0) FEBType = "sFEB";
                else if(iispFEB==1) FEBType = "pFEB";

                febTypeDirName[iQ][iL][iispFEB] = LayerDirName[iQ][iL]+FEBType+"/";
                oFile->mkdir(febTypeDirName[iQ][iL][iispFEB].c_str());

                NoiseRate_Vs_ChannelNo_DirName[iQ][iL][iispFEB] = febTypeDirName[iQ][iL][iispFEB]+"Hits_Vs_Channel/";
                oFile->mkdir(NoiseRate_Vs_ChannelNo_DirName[iQ][iL][iispFEB].c_str());

                NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName[iQ][iL][iispFEB] = febTypeDirName[iQ][iL][iispFEB]+"Hits_Vs_Channel/";
                oFile->mkdir(NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName[iQ][iL][iispFEB].c_str());
                
                NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName[iQ][iL][iispFEB] = febTypeDirName[iQ][iL][iispFEB]+"Hits_Vs_Channel/";
                oFile->mkdir(NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName[iQ][iL][iispFEB].c_str());

            }

        }
    }


}

