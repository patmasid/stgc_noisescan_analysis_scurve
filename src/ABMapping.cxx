// @Siyuan.Sun@cern.ch

#include "NSWNoiseAnalysis/ABMapping.h"

using namespace std;

ABMapping::ABMapping(){
  input_strip_file_raw = "/afs/cern.ch/user/p/patmasid/public/noise_runs_swrod/stgc_noiseData_analysis/data/AdapterBoardMapping/sTGC_Strip_AB_Mapping.txt";
  input_pad_file_raw   = "/afs/cern.ch/user/p/patmasid/public/noise_runs_swrod/stgc_noiseData_analysis/data/AdapterBoardMapping/sTGC_Pad_AB_Mapping.txt";
  input_Benoit_Mapping_raw = "/afs/cern.ch/user/p/patmasid/public/noise_runs_swrod/stgc_noiseData_analysis/data/AdapterBoardMapping/sTGC_AB_Mapping.txt";

  verbose = false;
  Strip_Mapping = new StripChannelMap();
  Pad_Mapping   = new PadChannelMap();
  Wire_Mapping  = new WireChannelMap();

  SetVerbose( verbose );


}

void ABMapping::SetVerbose(bool v) {
  verbose = v;

  Strip_Mapping->SetVerbose( verbose );
  Wire_Mapping ->SetVerbose( verbose );
  Pad_Mapping  ->SetVerbose( verbose );

}

//--------------------------------------//

void ABMapping::LoadBenoitMapping() {

  //---------------------------------------------------------------------------//

  LoadPadMapping();
  LoadStripMapping();
  
  //--------------------------------------------------------------------------------------------//
  
  if (verbose) std::cout << "Loading Adapter Board Mapping " << input_pad_file_raw << std::endl;
  
  std::string line;
  
  std::ifstream myfile(input_Benoit_Mapping_raw.c_str());
  
  if ( myfile.is_open() ) {

    //std::cout<<"Benoit mapping"<<std::endl;
    
    if (verbose) std::cout << "Open Adapter Board Mapping " << input_pad_file_raw << std::endl;
    
    while ( std::getline ( myfile,line ) ) {
      
      if ( line == "" ) return;
      
      std::string data_str =  line; //line.substr( 16 );                                                                            
      
      if (verbose) std::cout << data_str << std::endl;
      
      std::istringstream iss(line);
      
      std::string detector_str;
      int quadNum=-1, gas_gap_Num, layerNum;
      std::string PorC_str;  // is Pivot or Confirm
      
      std::string detector_type_str;
      
      int ivmm;
      int ivmm_chan;
      int idet_chan_construction;
      int idet_chan_assembly;
      int idet_chan_NSWElectrodeID;

      std::string GFZConn_str, GFZPin_str, GFZChan_str, triggerpadID, PadTriggerPatterns;

      if ( !(iss
             >> detector_str       >> gas_gap_Num >> layerNum 
	     >> detector_type_str  >> ivmm >> ivmm_chan
             >> idet_chan_assembly >> idet_chan_construction
	     >> idet_chan_NSWElectrodeID 
	     >> GFZConn_str 
	     >> GFZPin_str >> GFZChan_str 
	     >> triggerpadID >> PadTriggerPatterns) ) { 
	
	std::cout << "Error: Mapping line failed parsing " << data_str << std::endl;
	continue; 

      }

      //std::cout<<" detector "<<detector_str<<" layer "<<layerNum <<" detector type "<<detector_type_str<<" vmm "<<ivmm<<" vmm channel "<<ivmm_chan<<" idet_chan_assembly "<<idet_chan_assembly<<" idet_chan_construction "<<idet_chan_construction<<std::endl;
      

      ivmm = ivmm - 1; // convention starts VMM at 0 and not 1 as in benoit's table

      TString det_str = detector_str;
      bool isSmall = false;

      //TString Pivot_or_Confirm_str = PorC_str;
      bool isPivot = false;      
      
      TString det_type_str = detector_type_str;
      
      //---------------------------------------------------------------//
      
      if ( det_str.Contains("QS") ) {
	isSmall = true;
      }
      else if ( det_str.Contains("QL") ) {
	isSmall = false;
      }
      else {
	std::cout << "Error:  Could not determine small or large wedge" << std::endl;
      }
      
      //--------------------------------------------------------------//
      
      if ( det_str.Contains("P") ) {
	isPivot = true;
      }
      else if ( det_str.Contains("C") ) {
        isPivot = false;
      }
      else {
	std::cout << "Error:  Could not determine Confirm or pivot wedge" << std::endl;
	std::cout << "\t " << line << std::endl;
      }
      
      //-------------------------------------------------------------//
      
      if ( det_str.Contains("1") ) {
        quadNum=1;
      }
      else if ( det_str.Contains("2") ) {
        quadNum = 2;
      }
      else if ( det_str.Contains("3") ) {
	quadNum = 3;
      }
      else {
	std::cout << "Error:  Could not determine quadraplet number" << std::endl;
      }

      //-------------------------------------------------------------//


      if ( det_type_str.EqualTo( "pad" ) ) {

        bool okay = Pad_Mapping->SetPadChannelMap( ivmm,    ivmm_chan,
						   isSmall, isPivot,
						   quadNum, layerNum,
						   idet_chan_construction, idet_chan_assembly);
        if ( !okay ) {
	  std::cout << "Error:  Mapping line entry not parsed correctly" << std::endl;
	  std::cout << "\t " << line << std::endl;
        }
	
	//if ( idet_chan_construction != idet_chan_assembly ) {
	//  std::cout << "Different const. and assembly pad number: " << line << std::endl;
	//}

      }
      else if ( det_type_str.EqualTo( "strip" ) ) {
        bool okay = Strip_Mapping->SetStripChannelMap( ivmm,    ivmm_chan,
						       isSmall, isPivot,
						       quadNum, layerNum,
						       idet_chan_construction, idet_chan_assembly);

        if ( !okay ) {
	  std::cout << "Error:  Mapping line entry not parsed correctly" << std::endl;
	  std::cout << "\t " << line << std::endl;
        }

      }
      else if ( det_type_str.EqualTo( "wire" ) ) {


        bool okay = Wire_Mapping->SetWireChannelMap( ivmm,    ivmm_chan, 
						     isSmall, isPivot,  
						     quadNum, layerNum, 

						     idet_chan_construction, idet_chan_assembly);

	if ( !okay ) {
	  std::cout << "Error:  Mapping line entry not parsed correctly" << std::endl;
	  std::cout << "\t " << line << std::endl;
	}


      }
      else {
	std::cout << "Error:  Mapping line entry not parsed correctly" << std::endl;
	std::cout << "\t " << line << std::endl;
      }
      
    }

  }
  else 
    std::cout<<"Error: Mapping File not open "<<std::endl;
  
}

void ABMapping::LoadPadMapping() {

  int QS1P_l1_n_tot_pads, QS1P_l2_n_tot_pads, QS1P_l3_n_tot_pads, QS1P_l4_n_tot_pads;
  int QS2P_l1_n_tot_pads, QS2P_l2_n_tot_pads, QS2P_l3_n_tot_pads, QS2P_l4_n_tot_pads;
  int QS3P_l1_n_tot_pads, QS3P_l2_n_tot_pads, QS3P_l3_n_tot_pads, QS3P_l4_n_tot_pads;
  int QL1P_l1_n_tot_pads, QL1P_l2_n_tot_pads, QL1P_l3_n_tot_pads, QL1P_l4_n_tot_pads;
  int QL2P_l1_n_tot_pads, QL2P_l2_n_tot_pads, QL2P_l3_n_tot_pads, QL2P_l4_n_tot_pads;
  int QL3P_l1_n_tot_pads, QL3P_l2_n_tot_pads, QL3P_l3_n_tot_pads, QL3P_l4_n_tot_pads;

  int QS1C_l1_n_tot_pads, QS1C_l2_n_tot_pads, QS1C_l3_n_tot_pads, QS1C_l4_n_tot_pads;
  int QS2C_l1_n_tot_pads, QS2C_l2_n_tot_pads, QS2C_l3_n_tot_pads, QS2C_l4_n_tot_pads;
  int QS3C_l1_n_tot_pads, QS3C_l2_n_tot_pads, QS3C_l3_n_tot_pads, QS3C_l4_n_tot_pads;
  int QL1C_l1_n_tot_pads, QL1C_l2_n_tot_pads, QL1C_l3_n_tot_pads, QL1C_l4_n_tot_pads;
  int QL2C_l1_n_tot_pads, QL2C_l2_n_tot_pads, QL2C_l3_n_tot_pads, QL2C_l4_n_tot_pads;
  int QL3C_l1_n_tot_pads, QL3C_l2_n_tot_pads, QL3C_l3_n_tot_pads, QL3C_l4_n_tot_pads;

  int QS1P_l1_n_padPerRow, QS1P_l2_n_padPerRow, QS1P_l3_n_padPerRow, QS1P_l4_n_padPerRow;
  int QS2P_l1_n_padPerRow, QS2P_l2_n_padPerRow, QS2P_l3_n_padPerRow, QS2P_l4_n_padPerRow;
  int QS3P_l1_n_padPerRow, QS3P_l2_n_padPerRow, QS3P_l3_n_padPerRow, QS3P_l4_n_padPerRow;
  int QL1P_l1_n_padPerRow, QL1P_l2_n_padPerRow, QL1P_l3_n_padPerRow, QL1P_l4_n_padPerRow;
  int QL2P_l1_n_padPerRow, QL2P_l2_n_padPerRow, QL2P_l3_n_padPerRow, QL2P_l4_n_padPerRow;
  int QL3P_l1_n_padPerRow, QL3P_l2_n_padPerRow, QL3P_l3_n_padPerRow, QL3P_l4_n_padPerRow;

  int QS1C_l1_n_padPerRow, QS1C_l2_n_padPerRow, QS1C_l3_n_padPerRow, QS1C_l4_n_padPerRow;
  int QS2C_l1_n_padPerRow, QS2C_l2_n_padPerRow, QS2C_l3_n_padPerRow, QS2C_l4_n_padPerRow;
  int QS3C_l1_n_padPerRow, QS3C_l2_n_padPerRow, QS3C_l3_n_padPerRow, QS3C_l4_n_padPerRow;
  int QL1C_l1_n_padPerRow, QL1C_l2_n_padPerRow, QL1C_l3_n_padPerRow, QL1C_l4_n_padPerRow;
  int QL2C_l1_n_padPerRow, QL2C_l2_n_padPerRow, QL2C_l3_n_padPerRow, QL2C_l4_n_padPerRow;
  int QL3C_l1_n_padPerRow, QL3C_l2_n_padPerRow, QL3C_l3_n_padPerRow, QL3C_l4_n_padPerRow;


  int QS1P_l1_n_tot_wires, QS1P_l2_n_tot_wires, QS1P_l3_n_tot_wires, QS1P_l4_n_tot_wires;
  int QS2P_l1_n_tot_wires, QS2P_l2_n_tot_wires, QS2P_l3_n_tot_wires, QS2P_l4_n_tot_wires;
  int QS3P_l1_n_tot_wires, QS3P_l2_n_tot_wires, QS3P_l3_n_tot_wires, QS3P_l4_n_tot_wires;
  int QL1P_l1_n_tot_wires, QL1P_l2_n_tot_wires, QL1P_l3_n_tot_wires, QL1P_l4_n_tot_wires;
  int QL2P_l1_n_tot_wires, QL2P_l2_n_tot_wires, QL2P_l3_n_tot_wires, QL2P_l4_n_tot_wires;
  int QL3P_l1_n_tot_wires, QL3P_l2_n_tot_wires, QL3P_l3_n_tot_wires, QL3P_l4_n_tot_wires;

  int QS1C_l1_n_tot_wires, QS1C_l2_n_tot_wires, QS1C_l3_n_tot_wires, QS1C_l4_n_tot_wires;
  int QS2C_l1_n_tot_wires, QS2C_l2_n_tot_wires, QS2C_l3_n_tot_wires, QS2C_l4_n_tot_wires;
  int QS3C_l1_n_tot_wires, QS3C_l2_n_tot_wires, QS3C_l3_n_tot_wires, QS3C_l4_n_tot_wires;
  int QL1C_l1_n_tot_wires, QL1C_l2_n_tot_wires, QL1C_l3_n_tot_wires, QL1C_l4_n_tot_wires;
  int QL2C_l1_n_tot_wires, QL2C_l2_n_tot_wires, QL2C_l3_n_tot_wires, QL2C_l4_n_tot_wires;
  int QL3C_l1_n_tot_wires, QL3C_l2_n_tot_wires, QL3C_l3_n_tot_wires, QL3C_l4_n_tot_wires;


  if (verbose) std::cout << "Loading Pad Mapping " << input_pad_file_raw << std::endl;

  std::string line;

  std::ifstream myfile(input_pad_file_raw.c_str());

  if ( myfile.is_open() ) {

    if (verbose) std::cout << "Open Pad Mapping " << input_pad_file_raw << std::endl;

    std::string n_pad_str, n_pad_per_str, n_wire_str;

    std::getline ( myfile,line );
    if (verbose) std::cout << line << std::endl;

    std::getline ( myfile,line );
    if (verbose) std::cout << line << std::endl;

    std::istringstream iss(line);

    if ( !(iss
           >> n_pad_str
           >> QS1P_l1_n_tot_pads >> QS1P_l2_n_tot_pads >> QS1P_l3_n_tot_pads >> QS1P_l4_n_tot_pads
           >> QS1C_l4_n_tot_pads >> QS1C_l3_n_tot_pads >> QS1C_l2_n_tot_pads >> QS1C_l1_n_tot_pads
           >> QS2P_l1_n_tot_pads >> QS2P_l2_n_tot_pads >> QS2P_l3_n_tot_pads >> QS2P_l4_n_tot_pads
           >> QS2C_l4_n_tot_pads >> QS2C_l3_n_tot_pads >> QS2C_l2_n_tot_pads >> QS2C_l1_n_tot_pads
           >> QS3P_l1_n_tot_pads >> QS3P_l2_n_tot_pads >> QS3P_l3_n_tot_pads >> QS3P_l4_n_tot_pads
           >> QS3C_l4_n_tot_pads >> QS3C_l3_n_tot_pads >> QS3C_l2_n_tot_pads >> QS3C_l1_n_tot_pads
           >> QL1P_l1_n_tot_pads >> QL1P_l2_n_tot_pads >> QL1P_l3_n_tot_pads >> QL1P_l4_n_tot_pads
           >> QL1C_l4_n_tot_pads >> QL1C_l3_n_tot_pads >> QL1C_l2_n_tot_pads >> QL1C_l1_n_tot_pads
           >> QL2P_l1_n_tot_pads >> QL2P_l2_n_tot_pads >> QL2P_l3_n_tot_pads >> QL2P_l4_n_tot_pads
           >> QL2C_l4_n_tot_pads >> QL2C_l3_n_tot_pads >> QL2C_l2_n_tot_pads >> QL2C_l1_n_tot_pads
           >> QL3P_l1_n_tot_pads >> QL3P_l2_n_tot_pads >> QL3P_l3_n_tot_pads >> QL3P_l4_n_tot_pads
	   >> QL3C_l4_n_tot_pads >> QL3C_l3_n_tot_pads >> QL3C_l2_n_tot_pads >> QL3C_l1_n_tot_pads) ) { return; }

    //-----------------------------------------------------------------------------------------------------//

    std::getline ( myfile,line );
    if (verbose) std::cout << line << std::endl;
 
    std::istringstream iss2(line);
    //    iss.str(line);

    if ( !(iss2
           >> n_pad_per_str
           >> QS1P_l1_n_padPerRow >> QS1P_l2_n_padPerRow >> QS1P_l3_n_padPerRow >> QS1P_l4_n_padPerRow
           >> QS1C_l4_n_padPerRow >> QS1C_l3_n_padPerRow >> QS1C_l2_n_padPerRow >> QS1C_l1_n_padPerRow
           >> QS2P_l1_n_padPerRow >> QS2P_l2_n_padPerRow >> QS2P_l3_n_padPerRow >> QS2P_l4_n_padPerRow
           >> QS2C_l4_n_padPerRow >> QS2C_l3_n_padPerRow >> QS2C_l2_n_padPerRow >> QS2C_l1_n_padPerRow
           >> QS3P_l1_n_padPerRow >> QS3P_l2_n_padPerRow >> QS3P_l3_n_padPerRow >> QS3P_l4_n_padPerRow
           >> QS3C_l4_n_padPerRow >> QS3C_l3_n_padPerRow >> QS3C_l2_n_padPerRow >> QS3C_l1_n_padPerRow
           >> QL1P_l1_n_padPerRow >> QL1P_l2_n_padPerRow >> QL1P_l3_n_padPerRow >> QL1P_l4_n_padPerRow
           >> QL1C_l4_n_padPerRow >> QL1C_l3_n_padPerRow >> QL1C_l2_n_padPerRow >> QL1C_l1_n_padPerRow
           >> QL2P_l1_n_padPerRow >> QL2P_l2_n_padPerRow >> QL2P_l3_n_padPerRow >> QL2P_l4_n_padPerRow
           >> QL2C_l4_n_padPerRow >> QL2C_l3_n_padPerRow >> QL2C_l2_n_padPerRow >> QL2C_l1_n_padPerRow
           >> QL3P_l1_n_padPerRow >> QL3P_l2_n_padPerRow >> QL3P_l3_n_padPerRow >> QL3P_l4_n_padPerRow
           >> QL3C_l4_n_padPerRow >> QL3C_l3_n_padPerRow >> QL3C_l2_n_padPerRow >> QL3C_l1_n_padPerRow) ) { return; }

    //---------------------------------------------------------------------------------------------------//

    std::getline ( myfile,line );
    if (verbose) std::cout << line << std::endl;

    std::istringstream iss3(line);

    if ( !(iss3
           >> n_wire_str
           >> QS1P_l1_n_tot_wires >> QS1P_l2_n_tot_wires >> QS1P_l3_n_tot_wires >> QS1P_l4_n_tot_wires
           >> QS1C_l4_n_tot_wires >> QS1C_l3_n_tot_wires >> QS1C_l2_n_tot_wires >> QS1C_l1_n_tot_wires
           >> QS2P_l1_n_tot_wires >> QS2P_l2_n_tot_wires >> QS2P_l3_n_tot_wires >> QS2P_l4_n_tot_wires
           >> QS2C_l4_n_tot_wires >> QS2C_l3_n_tot_wires >> QS2C_l2_n_tot_wires >> QS2C_l1_n_tot_wires
           >> QS3P_l1_n_tot_wires >> QS3P_l2_n_tot_wires >> QS3P_l3_n_tot_wires >> QS3P_l4_n_tot_wires
           >> QS3C_l4_n_tot_wires >> QS3C_l3_n_tot_wires >> QS3C_l2_n_tot_wires >> QS3C_l1_n_tot_wires
           >> QL1P_l1_n_tot_wires >> QL1P_l2_n_tot_wires >> QL1P_l3_n_tot_wires >> QL1P_l4_n_tot_wires
           >> QL1C_l4_n_tot_wires >> QL1C_l3_n_tot_wires >> QL1C_l2_n_tot_wires >> QL1C_l1_n_tot_wires
           >> QL2P_l1_n_tot_wires >> QL2P_l2_n_tot_wires >> QL2P_l3_n_tot_wires >> QL2P_l4_n_tot_wires
           >> QL2C_l4_n_tot_wires >> QL2C_l3_n_tot_wires >> QL2C_l2_n_tot_wires >> QL2C_l1_n_tot_wires
           >> QL3P_l1_n_tot_wires >> QL3P_l2_n_tot_wires >> QL3P_l3_n_tot_wires >> QL3P_l4_n_tot_wires
           >> QL3C_l4_n_tot_wires >> QL3C_l3_n_tot_wires >> QL3C_l2_n_tot_wires >> QL3C_l1_n_tot_wires) ) { return; }

    std::getline ( myfile,line );
    if (verbose) std::cout << line << "empty" << std::endl;

    //---------------------------------------------------------------------------------------------------//
    //
    //---------------------------------------------------------------------------------------------------//

    Pad_Mapping->SetNPadChannelMap( true, true,  1, 1, QS1P_l1_n_tot_pads, QS1P_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  1, 2, QS1P_l2_n_tot_pads, QS1P_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  1, 3, QS1P_l3_n_tot_pads, QS1P_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  1, 4, QS1P_l4_n_tot_pads, QS1P_l4_n_padPerRow );

    Pad_Mapping->SetNPadChannelMap( true, false, 1, 1, QS1C_l1_n_tot_pads, QS1C_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 1, 2, QS1C_l2_n_tot_pads, QS1C_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 1, 3, QS1C_l3_n_tot_pads, QS1C_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 1, 4, QS1C_l4_n_tot_pads, QS1C_l4_n_padPerRow );

    //--------------------------------------------------------//

    Pad_Mapping->SetNPadChannelMap( true, true,  2, 1, QS2P_l1_n_tot_pads, QS1P_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  2, 2, QS2P_l2_n_tot_pads, QS1P_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  2, 3, QS2P_l3_n_tot_pads, QS1P_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  2, 4, QS2P_l4_n_tot_pads, QS1P_l4_n_padPerRow );

    Pad_Mapping->SetNPadChannelMap( true, false, 2, 1, QS2C_l1_n_tot_pads, QS2C_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 2, 2, QS2C_l2_n_tot_pads, QS2C_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 2, 3, QS2C_l3_n_tot_pads, QS2C_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 2, 4, QS2C_l4_n_tot_pads, QS2C_l4_n_padPerRow );

    //-------------------------------------------------------//

    Pad_Mapping->SetNPadChannelMap( true, true,  3, 1, QS3P_l1_n_tot_pads, QS3P_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  3, 2, QS3P_l2_n_tot_pads, QS3P_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  3, 3, QS3P_l3_n_tot_pads, QS3P_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, true,  3, 4, QS3P_l4_n_tot_pads, QS3P_l4_n_padPerRow );

    Pad_Mapping->SetNPadChannelMap( true, false, 3, 1, QS3C_l1_n_tot_pads, QS3C_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 3, 2, QS3C_l2_n_tot_pads, QS3C_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 3, 3, QS3C_l3_n_tot_pads, QS3C_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( true, false, 3, 4, QS3C_l4_n_tot_pads, QS3C_l4_n_padPerRow );

    //-------------------------------------------------------//

    Pad_Mapping->SetNPadChannelMap( false, true,  1, 1, QL1P_l1_n_tot_pads, QL1P_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  1, 2, QL1P_l2_n_tot_pads, QL1P_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  1, 3, QL1P_l3_n_tot_pads, QL1P_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  1, 4, QL1P_l4_n_tot_pads, QL1P_l4_n_padPerRow );

    Pad_Mapping->SetNPadChannelMap( false, false, 1, 1, QL1C_l1_n_tot_pads, QL1C_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 1, 2, QL1C_l2_n_tot_pads, QL1C_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 1, 3, QL1C_l3_n_tot_pads, QL1C_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 1, 4, QL1C_l4_n_tot_pads, QL1C_l4_n_padPerRow );

    //--------------------------------------------------------//

    Pad_Mapping->SetNPadChannelMap( false, true,  2, 1, QL2P_l1_n_tot_pads, QL2P_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  2, 2, QL2P_l2_n_tot_pads, QL2P_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  2, 3, QL2P_l3_n_tot_pads, QL2P_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  2, 4, QL2P_l4_n_tot_pads, QL2P_l4_n_padPerRow );

    Pad_Mapping->SetNPadChannelMap( false, false, 2, 1, QL2C_l1_n_tot_pads, QL2C_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 2, 2, QL2C_l2_n_tot_pads, QL2C_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 2, 3, QL2C_l3_n_tot_pads, QL2C_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 2, 4, QL2C_l4_n_tot_pads, QL2C_l4_n_padPerRow );

    //--------------------------------------------------------//

    Pad_Mapping->SetNPadChannelMap( false, true,  3, 1, QL3P_l1_n_tot_pads, QL3P_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  3, 2, QL3P_l2_n_tot_pads, QL3P_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  3, 3, QL3P_l3_n_tot_pads, QL3P_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, true,  3, 4, QL3P_l4_n_tot_pads, QL3P_l4_n_padPerRow );

    Pad_Mapping->SetNPadChannelMap( false, false, 3, 1, QL3C_l1_n_tot_pads, QL3C_l1_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 3, 2, QL3C_l2_n_tot_pads, QL3C_l2_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 3, 3, QL3C_l3_n_tot_pads, QL3C_l3_n_padPerRow );
    Pad_Mapping->SetNPadChannelMap( false, false, 3, 4, QL3C_l4_n_tot_pads, QL3C_l4_n_padPerRow );

    //-------------------------------------------------------//

    Wire_Mapping->SetNWireChannelMap( true, true,  1, 1, QS1P_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  1, 2, QS1P_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  1, 3, QS1P_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  1, 4, QS1P_l4_n_tot_wires );

    Wire_Mapping->SetNWireChannelMap( true, false, 1, 1, QS1C_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 1, 2, QS1C_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 1, 3, QS1C_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 1, 4, QS1C_l4_n_tot_wires );

    //--------------------------------------------------------//

    Wire_Mapping->SetNWireChannelMap( true, true,  2, 1, QS2P_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  2, 2, QS2P_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  2, 3, QS2P_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  2, 4, QS2P_l4_n_tot_wires );

    Wire_Mapping->SetNWireChannelMap( true, false, 2, 1, QS2C_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 2, 2, QS2C_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 2, 3, QS2C_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 2, 4, QS2C_l4_n_tot_wires );

    //-------------------------------------------------------// 

    Wire_Mapping->SetNWireChannelMap( true, true,  3, 1, QS3P_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  3, 2, QS3P_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  3, 3, QS3P_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, true,  3, 4, QS3P_l4_n_tot_wires );

    Wire_Mapping->SetNWireChannelMap( true, false, 3, 1, QS3C_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 3, 2, QS3C_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 3, 3, QS3C_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( true, false, 3, 4, QS3C_l4_n_tot_wires );

    //-----------------------------------------------------//

    Wire_Mapping->SetNWireChannelMap( false, true,  1, 1, QL1P_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  1, 2, QL1P_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  1, 3, QL1P_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  1, 4, QL1P_l4_n_tot_wires );

    Wire_Mapping->SetNWireChannelMap( false, false, 1, 1, QL1C_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 1, 2, QL1C_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 1, 3, QL1C_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 1, 4, QL1C_l4_n_tot_wires );

    //--------------------------------------------------------//

    Wire_Mapping->SetNWireChannelMap( false, true,  2, 1, QL2P_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  2, 2, QL2P_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  2, 3, QL2P_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  2, 4, QL2P_l4_n_tot_wires );

    Wire_Mapping->SetNWireChannelMap( false, false, 2, 1, QL2C_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 2, 2, QL2C_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 2, 3, QL2C_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 2, 4, QL2C_l4_n_tot_wires );

    //--------------------------------------------------------//

    Wire_Mapping->SetNWireChannelMap( false, true,  3, 1, QL3P_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  3, 2, QL3P_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  3, 3, QL3P_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, true,  3, 4, QL3P_l4_n_tot_wires );

    Wire_Mapping->SetNWireChannelMap( false, false, 3, 1, QL3C_l1_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 3, 2, QL3C_l2_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 3, 3, QL3C_l3_n_tot_wires );
    Wire_Mapping->SetNWireChannelMap( false, false, 3, 4, QL3C_l4_n_tot_wires );

    //-------------------------------------------------------//

  } // end file open

  else
    std::cout<<"NOOOOOOOOOOOOOOO Pad :( :("<<std::endl;

} // end LoadPadMapping method

void ABMapping::LoadStripMapping() {

  int QS1_l1_n_tot_strips, QS1_l2_n_tot_strips, QS1_l3_n_tot_strips, QS1_l4_n_tot_strips;
  int QS2_l1_n_tot_strips, QS2_l2_n_tot_strips, QS2_l3_n_tot_strips, QS2_l4_n_tot_strips;
  int QS3_l1_n_tot_strips, QS3_l2_n_tot_strips, QS3_l3_n_tot_strips, QS3_l4_n_tot_strips;
  int QL1_l1_n_tot_strips, QL1_l2_n_tot_strips, QL1_l3_n_tot_strips, QL1_l4_n_tot_strips;
  int QL2_l1_n_tot_strips, QL2_l2_n_tot_strips, QL2_l3_n_tot_strips, QL2_l4_n_tot_strips;
  int QL3_l1_n_tot_strips, QL3_l2_n_tot_strips, QL3_l3_n_tot_strips, QL3_l4_n_tot_strips;

  if (verbose) std::cout << "Loading Strip Mapping " << input_strip_file_raw << std::endl;

  std::string line;
  
  std::ifstream myfile(input_strip_file_raw.c_str());

  if ( myfile.is_open() ) {

    std::string n_strip_str;

    std::getline ( myfile,line );
    std::getline ( myfile,line );
    std::istringstream iss(line);

    if ( !(iss
	   >> n_strip_str 
	   >> QS1_l1_n_tot_strips >> QS1_l2_n_tot_strips >> QS1_l3_n_tot_strips >> QS1_l4_n_tot_strips
	   >> QS2_l1_n_tot_strips >> QS2_l2_n_tot_strips >> QS2_l3_n_tot_strips >> QS2_l4_n_tot_strips
	   >> QS3_l1_n_tot_strips >> QS3_l2_n_tot_strips >> QS3_l3_n_tot_strips >> QS3_l4_n_tot_strips
	   >> QL1_l1_n_tot_strips >> QL1_l2_n_tot_strips >> QL1_l3_n_tot_strips >> QL1_l4_n_tot_strips
	   >> QL2_l1_n_tot_strips >> QL2_l2_n_tot_strips >> QL2_l3_n_tot_strips >> QL2_l4_n_tot_strips
	   >> QL3_l1_n_tot_strips >> QL3_l2_n_tot_strips >> QL3_l3_n_tot_strips >> QL3_l4_n_tot_strips) ) { return; }

    //-------------------------------------------------------//

    Strip_Mapping->SetNStripChannelMap( true, true,  1, 1, QS1_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  1, 2, QS1_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  1, 3, QS1_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  1, 4, QS1_l4_n_tot_strips );

    Strip_Mapping->SetNStripChannelMap( true, false, 1, 1, QS1_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 1, 2, QS1_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 1, 3, QS1_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 1, 4, QS1_l4_n_tot_strips );

    //--------------------------------------------------------//

    Strip_Mapping->SetNStripChannelMap( true, true,  2, 1, QS2_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  2, 2, QS2_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  2, 3, QS2_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  2, 4, QS2_l4_n_tot_strips );

    Strip_Mapping->SetNStripChannelMap( true, false, 2, 1, QS2_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 2, 2, QS2_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 2, 3, QS2_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 2, 4, QS2_l4_n_tot_strips );

    //-------------------------------------------------------//

    Strip_Mapping->SetNStripChannelMap( true, true,  3, 1, QS3_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  3, 2, QS3_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  3, 3, QS3_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, true,  3, 4, QS3_l4_n_tot_strips );

    Strip_Mapping->SetNStripChannelMap( true, false, 3, 1, QS3_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 3, 2, QS3_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 3, 3, QS3_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( true, false, 3, 4, QS3_l4_n_tot_strips );

    //-------------------------------------------------------//

    Strip_Mapping->SetNStripChannelMap( false, true,  1, 1, QL1_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  1, 2, QL1_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  1, 3, QL1_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  1, 4, QL1_l4_n_tot_strips );

    Strip_Mapping->SetNStripChannelMap( false, false, 1, 1, QL1_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 1, 2, QL1_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 1, 3, QL1_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 1, 4, QL1_l4_n_tot_strips );

    //--------------------------------------------------------//

    Strip_Mapping->SetNStripChannelMap( false, true,  2, 1, QL2_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  2, 2, QL2_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  2, 3, QL2_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  2, 4, QL2_l4_n_tot_strips );

    Strip_Mapping->SetNStripChannelMap( false, false, 2, 1, QL2_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 2, 2, QL2_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 2, 3, QL2_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 2, 4, QL2_l4_n_tot_strips );

    //--------------------------------------------------------//

    Strip_Mapping->SetNStripChannelMap( false, true,  3, 1, QL3_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  3, 2, QL3_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  3, 3, QL3_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, true,  3, 4, QL3_l4_n_tot_strips );

    Strip_Mapping->SetNStripChannelMap( false, false, 3, 1, QL3_l1_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 3, 2, QL3_l2_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 3, 3, QL3_l3_n_tot_strips );
    Strip_Mapping->SetNStripChannelMap( false, false, 3, 4, QL3_l4_n_tot_strips );

  }
  else
    std::cout<<"NOOOOOOOOOOOOOOO Strip :( :("<<std::endl;

}
 
//-------------------------------------------------------------------------//
//                           Return Is Connected
//-------------------------------------------------------------------------//

bool ABMapping::IsConnected_pFEB( int ith_vmm,  int ith_vmm_chan,
                                  bool isSmall, bool isPivot,
                                  int quadNum, int layerNum ) {

  int chanNumConstruction = -1;
  int chanNumAssembly   = -1;

  if ( ith_vmm == 0 ) {
    ReturnWireChannelNumber( ith_vmm, ith_vmm_chan,
			     isSmall, isPivot,
			     quadNum, layerNum,
			     chanNumConstruction,
			     chanNumAssembly);
  }
  else if ( ith_vmm == 1 ||
	    ith_vmm == 2 ) {
    ReturnPadChannelNumber( ith_vmm, ith_vmm_chan,
			    isSmall, isPivot,
			    quadNum, layerNum,
			    chanNumConstruction,
			    chanNumAssembly);
  }
  else {
    chanNumConstruction = -1;
    chanNumAssembly     = -1;
  }
  
  bool isConnected = false;

  if ( chanNumConstruction != -1 &&
       chanNumAssembly     != -1 ) {
    isConnected= true;
  }

  return isConnected;

}

bool ABMapping::IsConnected_sFEB( int ith_vmm,  int ith_vmm_chan,
				  bool isSmall, bool isPivot,
				  int quadNum, int layerNum ) {

  int stripNumConstruction = -1;
  int stripNumAssembly     = -1;

  ReturnStripChannelNumber( ith_vmm, ith_vmm_chan,
			    isSmall, isPivot,
			    quadNum, layerNum,
			    stripNumConstruction,
			    stripNumAssembly);

  bool isConnected = false;

  if ( stripNumConstruction != -1 &&
       stripNumAssembly     != -1 ) {
    isConnected = true;
  }
  
  return isConnected;

}

//------------------------------------------------------------------------//
//                           Return Strip Info
//------------------------------------------------------------------------//

bool ABMapping::ReturnNStripsTotal( bool isSmall, bool isPivot,
				    int quadNum, int layerNum,
				    int &n_tot_strips ) {
  
  int n_tot_strip=-1;

  bool okay = Strip_Mapping->ReturnNStripsTotal( isSmall, isPivot,
						 quadNum, layerNum, n_tot_strip );

  n_tot_strips = n_tot_strip;

  return okay;


} 

bool ABMapping::ReturnStripChannelNumber( int ith_vmm,  int ith_vmm_chan,
					  bool isSmall, bool isPivot,
					  int quadNum, int layerNum, 
					  int &stripNumConstruction,
					  int &stripNumAssembly) {

  int istripConst=-1;
  int istripAssem=-1;

  bool okay = Strip_Mapping->ReturnStripChannelNumber( ith_vmm, ith_vmm_chan,
						       isSmall, isPivot,
						       quadNum, layerNum, 
						       istripConst,
						       istripAssem);
  
  stripNumConstruction = istripConst;
  stripNumAssembly     = istripAssem;

  return okay;

}

bool ABMapping::ReturnStripElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
								  bool isSmall, bool isPivot,
								  int quadNum, int layerNum, 
								  int stripNumConstruction,
								  int &stripNumAssembly) {

  int ivmm=-1;
  int ichan=-1;

  int stripNumAssem=-1;

  bool okay = Strip_Mapping->ReturnElectronicsChannelNumber_Construction( ivmm, ichan,
									  isSmall, isPivot,
									  quadNum, layerNum, 
									  stripNumConstruction,
									  stripNumAssem );

  stripNumAssembly = stripNumAssem;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}

bool ABMapping::ReturnStripElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
							      bool isSmall, bool isPivot,
							      int quadNum, int layerNum,
							      int &stripNumConstruction,
							      int stripNumAssembly) {

  int ivmm=-1;
  int ichan=-1;

  int stripNumConst=-1;

  bool okay = Strip_Mapping->ReturnElectronicsChannelNumber_Assembly( ivmm, ichan,
								      isSmall, isPivot,
								      quadNum, layerNum,
								      stripNumConst,
								      stripNumAssembly );

  stripNumConstruction = stripNumConst;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}

//-------------------------------------------------------------------------//
//                           Return Wire Info                               
//-------------------------------------------------------------------------//

bool ABMapping::ReturnNWiresTotal( bool isSmall, bool isPivot,
				   int quadNum, int layerNum,
				   int &n_tot_wires ) {

  int n_tot_wire=-1;

  bool okay = Wire_Mapping->ReturnNWiresTotal( isSmall, isPivot,
					       quadNum, layerNum, n_tot_wire );

  n_tot_wires = n_tot_wire;

  return okay;


}

bool ABMapping::ReturnWireChannelNumber( int ith_vmm,  int ith_vmm_chan,
					 bool isSmall, bool isPivot,
					 int quadNum, int layerNum, 
					 int &wireNumConstruction,
					 int &wireNumAssembly) {

  int iwireConst=-1;
  int iwireAssem=-1;

  bool okay = Wire_Mapping->ReturnWireChannelNumber( ith_vmm, ith_vmm_chan,
						     isSmall, isPivot,
						     quadNum, layerNum, 
						     iwireConst,
						     iwireAssem );

    //if(verbose) std::cout<<"ith_vmm "<<ith_vmm<<" ith_vmm_chan "<<ith_vmm_chan<<" isSmall "<<isSmall<<" isPivot "<<isPivot<<" quadNum "<<quadNum<<" layerNum "<<layerNum<<" iwireConst "<<iwireConst<<" iwireAssem "<<iwireAssem<<std::endl;

  wireNumConstruction = iwireConst;
  wireNumAssembly     = iwireAssem;
  
  return okay;

}

bool ABMapping::ReturnWireElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
								 bool isSmall, bool isPivot,
								 int quadNum, int layerNum, 
								 int wireNumConstruction,
								 int &wireNumAssembly) {

  int ivmm=-1;
  int ichan=-1;

  int wireNumAssem=-1;

  bool okay = Wire_Mapping->ReturnElectronicsChannelNumber_Construction( ivmm, ichan,
									 isSmall, isPivot,
									 quadNum, layerNum, 
									 wireNumConstruction,
									 wireNumAssem);

  wireNumAssembly = wireNumAssem;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}

bool ABMapping::ReturnWireElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
							     bool isSmall, bool isPivot,
							     int quadNum, int layerNum,
							     int &wireNumConstruction,
							     int wireNumAssembly) {

  int ivmm=-1;
  int ichan=-1;

  int wireNumConst=-1;

  bool okay = Wire_Mapping->ReturnElectronicsChannelNumber_Assembly( ivmm, ichan,
								     isSmall, isPivot,
								     quadNum, layerNum,
								     wireNumConst,
								     wireNumAssembly);

  wireNumConstruction = wireNumConst;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}



//------------------------------------------------------------------------//
//                           Return Pad Info
//------------------------------------------------------------------------//

bool ABMapping::ReturnNPadPerRow( bool isSmall, bool isPivot,
				  int quadNum, int layerNum,
				  int &n_pads_per_row ) {

  
  int n_padsPerRow=-1;

  bool okay = Pad_Mapping->ReturnNPadPerRow( isSmall, isPivot,
					     quadNum, layerNum, n_padsPerRow );
  
  n_pads_per_row = n_padsPerRow;

  return okay;

}

bool ABMapping::ReturnNPadsTotal( bool isSmall, bool isPivot,
				  int quadNum, int layerNum,
				  int &n_tot_pads ) {

  int n_tot_pad=-1;

  bool okay = Pad_Mapping->ReturnNPadsTotal( isSmall, isPivot,
					     quadNum, layerNum, n_tot_pad );
  
  n_tot_pads = n_tot_pad;
  
  return okay;


}

bool ABMapping::ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
					bool isSmall, bool isPivot,
					int quadNum, int layerNum, 
					int &padNumConstruction,
					int &padNumAssembly) {

  int ipadConst = -1;
  int ipadAssem = -1;

  bool okay = Pad_Mapping->ReturnPadChannelNumber( ith_vmm, ith_vmm_chan,
						   isSmall, isPivot,
						   quadNum, layerNum, 
						   ipadConst, ipadAssem );

  //std::cout<<"ith_vmm "<<ith_vmm<<" ith_vmm_chan "<<ith_vmm_chan<<" isSmall "<<isSmall<<" isPivot "<<isPivot<<" quadNum "<<quadNum<<" layerNum "<<layerNum<<" ipadConst "<<ipadConst<<" ipadAssem "<<ipadAssem<<std::endl;

  padNumConstruction = ipadConst;
  padNumAssembly     = ipadAssem;

  return okay;

}

bool ABMapping::ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
					bool isSmall, bool isPivot,
					int quadNum, int layerNum, 
					int &padXConstruction, int &padYConstruction,
					int &padXAssembly,     int &padYAssembly,
					int &n_pads_per_row ) {

  int ipadXConst = -1;
  int ipadYConst = -1;
  int ipadXAssem = -1;
  int ipadYAssem = -1;
  int ipadPerRow = -1;

  bool okay = Pad_Mapping->ReturnPadChannelNumber( ith_vmm, ith_vmm_chan,
						   isSmall, isPivot,
						   quadNum, layerNum, 
						   ipadXConst, ipadYConst, 
						   ipadXAssem, ipadYAssem,
						   ipadPerRow );

  padXConstruction = ipadXConst;
  padYConstruction = ipadYConst;
  padXAssembly = ipadXAssem;
  padYAssembly = ipadYAssem;

  n_pads_per_row = ipadPerRow;

  return okay;

}


bool ABMapping::ReturnPadElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
								bool isSmall, bool isPivot,
								int quadNum, int layerNum, 
								int padXConstruction, int padYConstruction,
								int &padXAssembly,    int &padYAssembly) {

  int ivmm=-1;
  int ichan=-1;

  int ipadXAssem = -1;
  int ipadYAssem = -1;

  bool okay = Pad_Mapping->ReturnElectronicsChannelNumber_Construction( ivmm, ichan,
									isSmall, isPivot,
									quadNum, layerNum, 
									padXConstruction,
									padYConstruction,
									ipadXAssem, ipadYAssem);

  
  padXAssembly = ipadXAssem;
  padYAssembly = ipadYAssem;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}

bool ABMapping::ReturnPadElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
                                                                bool isSmall, bool isPivot,
                                                                int quadNum, int layerNum,
                                                                int &padXConstruction, int &padYConstruction,
                                                                int padXAssembly,      int padYAssembly) {

  int ivmm=-1;
  int ichan=-1;

  int ipadXConst = -1;
  int ipadYConst = -1;

  bool okay = Pad_Mapping->ReturnElectronicsChannelNumber_Assembly( ivmm, ichan,
								    isSmall, isPivot,
								    quadNum, layerNum,
								    ipadXConst,   ipadYConst,
								    padXAssembly, padYAssembly);


  padXConstruction = ipadXConst;
  padYConstruction = ipadYConst;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}



bool ABMapping::ReturnPadElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
								bool isSmall, bool isPivot,
								int quadNum, int layerNum, 
								int padNumConstruction,
								int &padNumAssembly ) {

  int ivmm=-1;
  int ichan=-1;

  int padNumAssem = -1;

  bool okay = Pad_Mapping->ReturnElectronicsChannelNumber_Construction( ivmm, ichan,
									isSmall, isPivot,
									quadNum, layerNum, 
									padNumConstruction,
									padNumAssem );

  padNumAssembly = padNumAssem;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}

bool ABMapping::ReturnPadElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
							    bool isSmall, bool isPivot,
							    int quadNum, int layerNum,
							    int &padNumConstruction,
							    int padNumAssembly ) {

  int ivmm=-1;
  int ichan=-1;

  int padNumConst = -1;

  bool okay = Pad_Mapping->ReturnElectronicsChannelNumber_Assembly( ivmm, ichan,
								    isSmall, isPivot,
								    quadNum, layerNum,
								    padNumConst,
								    padNumAssembly );

  padNumConstruction = padNumConst;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return okay;

}

std::pair< std::vector<int>, std::vector<int> > ABMapping::NumChan_perVmm_PhysConnected(bool isSmall, bool isPivot,
                                              int quadNum, int layerNum, bool ispFEB,
                                              bool isPad, int total_chan_perQuad_perLayer){

    int vmmNum = -1;
    if(ispFEB && isPad) vmmNum = 2;
    else if(!ispFEB && (quadNum == 2 || quadNum == 3) ) vmmNum = 6;
    else if(!ispFEB && (quadNum == 1) ) vmmNum = 8;
    std::vector<int> v_NumChanConnected; // = std::vector<int>(vmmNum,0);
    std::vector<int> v_VMMid; //= std::vector<int>(vmmNum,-1);

    int vmmid, chanid, constr_chan;

    bool found_ElecChan = false;

    int current_id = 0;
    int current_vmmid = -1;

    for(int ii=1; ii <= total_chan_perQuad_perLayer; ii++){

      
        
      if(ispFEB == false) found_ElecChan = ReturnStripElectronicsChannelNumber_Assembly( vmmid,  chanid,
											 isSmall, isPivot,
											 quadNum, layerNum,
											 constr_chan,
											 ii); 
      else if(ispFEB == true) {
	
	if(!isPad) {
	  continue;
	  //found_ElecChan = ReturnWireElectronicsChannelNumber_Assembly( vmmid,  chanid,
	  //isSmall, isPivot,
	  //quadNum, layerNum,
	  //constr_chan,
	  //ii);
	}
	
	else {
	  found_ElecChan = ReturnPadElectronicsChannelNumber_Assembly( vmmid,  chanid,
								       isSmall, isPivot,
								       quadNum, layerNum,
								       constr_chan,
								       ii);
	  if (verbose) std::cout << "ReturnPadElectronicsChannelNumber_Assembly: " << "isSmall: " << isSmall << " isPivot: " << isPivot 
				 << " quadNum: " << quadNum << " layerNum: " << layerNum << " ii: " << ii << " constr_chan: " << constr_chan 
				 << " vmmid: " << vmmid << " chanid: " << chanid << std::endl;
	}
	
      }
      
      if(current_id == 0 && current_vmmid == -1) {
	current_vmmid = vmmid;
      }
      else if(vmmid == current_vmmid){
	
      }
      else if(vmmid != current_vmmid){
	current_id++;
	current_vmmid = vmmid;
      }
      
      /*else if(vmmid != current_vmmid && find(v_VMMid.begin(), v_VMMid.end(), vmmid) == v_VMMid.end() ){
	current_id++;
	current_vmmid = vmmid;
	v_VMMid[current_id] = vmmid;
	}
	else if(vmmid != current_vmmid && find(v_VMMid.begin(), v_VMMid.end(), vmmid) != v_VMMid.end()){
	current_vmmid = vmmid;
	current_id = distance(v_VMMid.begin(), find(v_VMMid.begin(), v_VMMid.end(), vmmid));
	}*/
      
      if(v_NumChanConnected.size()==0 || current_id > v_NumChanConnected.size()-1){
	v_NumChanConnected.push_back(1);
	v_VMMid.push_back(vmmid);
      }
      else if(current_id <= v_NumChanConnected.size()-1){
	v_NumChanConnected[current_id]++;
      }
      
    }
    
    if(isPad){
      for(int ii = 0; ii < v_NumChanConnected.size(); ii++){
	if(verbose) std::cout << "ii: " << ii << " " << v_NumChanConnected[ii] << " " << v_VMMid[ii] << std::endl;
      }
    }

    
    std::pair< std::vector<int>, std::vector<int> > pair_NumChan_VMMid;
    if(v_NumChanConnected.size() == v_VMMid.size()){
      pair_NumChan_VMMid = std::make_pair(v_NumChanConnected, v_VMMid);
    }
    else
      std::cout << "ERROR: v_NumChanConnected and v_VMMid do not have same size." << std::endl;
    
    return pair_NumChan_VMMid;

}
