#define AnalyzeNoiseThres_cxx
#include "NSWNoiseAnalysis/AnalyzeNoiseThres.h"
#include "NSWNoiseAnalysis/Variables.h"

AnalyzeNoiseThres::AnalyzeNoiseThres(const char *inputRunName, const char *decodedRunName, const char* outputRunName, const char *inputThres, bool isSmall, bool isPivot, bool isELinkID, bool isswROD){

    isSmall_ = isSmall;
    isPivot_ = isPivot;

    a_ABMap = new ABMapping();
    a_ABMap->SetVerbose(false);
    a_ABMap->LoadBenoitMapping();
 
    Init();
   
    isELinkID_ = isELinkID;
    isswROD_ = isswROD;

    std::string input_thres_str;

    input_thres_str = inputThres;

    std::stringstream input_thres_ss(input_thres_str);

    std::vector<std::string> v_ThresAboveBaseline_str;

    std::string delimiter = ",";

    size_t pos = 0;
    std::string token;
    while ((pos = input_thres_str.find(delimiter)) != std::string::npos) {
      token = input_thres_str.substr(0, pos);
      v_ThresAboveBaseline_str.push_back(token);
      std::cout << token << std::endl;
      input_thres_str.erase(0, pos + delimiter.length());
    }
    v_ThresAboveBaseline_str.push_back(input_thres_str);

    int Thres;

    for(unsigned int ii = 0; ii < v_ThresAboveBaseline_str.size(); ii++){

      if(v_ThresAboveBaseline_str[ii].find("-") != std::string::npos){
        v_ThresAboveBaseline_str[ii].erase(v_ThresAboveBaseline_str[ii].begin());
	std::stringstream ss(v_ThresAboveBaseline_str[ii]);

        ss >> Thres;
	std::cout << v_ThresAboveBaseline_str[ii] << " " << Thres << std::endl;
        Thres *= -1;
	std::cout << Thres << std::endl;
        v_ThresAboveBaseline.push_back(Thres);
      }
      else{
	std::stringstream ss(v_ThresAboveBaseline_str[ii]);
        ss >> Thres;
	std::cout << Thres << std::endl;
        v_ThresAboveBaseline.push_back(Thres);
      }

    }

    testThresSize = v_ThresAboveBaseline.size();

    v_ThresholdRuns.resize(testThresSize);

    inputRunName_ = inputRunName;
    decodedRunName_ = decodedRunName;
    outputRunName_ = outputRunName;

    for(int iThres = 0; iThres<testThresSize; iThres++){
        
        std::string inputHistFile_, inputTree_, outFileName_;
	if(v_ThresAboveBaseline[iThres] >= 0){
          inputHistFile_ = decodedRunName_+"+"+std::to_string(v_ThresAboveBaseline[iThres])+"_decoded.root";
          inputTree_ = inputRunName_+"+"+std::to_string(v_ThresAboveBaseline[iThres])+".root";
          outFileName_ = outputRunName_+"+"+std::to_string(v_ThresAboveBaseline[iThres])+"_NoiseRate.root";
        }
        else{
          inputHistFile_ = decodedRunName_+"-"+std::to_string(std::abs(int(v_ThresAboveBaseline[iThres])))+"_decoded.root";
          inputTree_ = inputRunName_+"-"+std::to_string(std::abs(int(v_ThresAboveBaseline[iThres])))+".root";
          outFileName_ = outputRunName_+"-"+std::to_string(std::abs(int(v_ThresAboveBaseline[iThres])))+"_NoiseRate.root";
        }

        v_ThresholdRuns[iThres] = new ThresholdRunInfo(inputHistFile_.c_str(), inputTree_.c_str(), outFileName_.c_str(), isSmall_, isPivot_, isELinkID_, isswROD_);

        v_ThresholdRuns[iThres]->ThresholdAboveBaseline = v_ThresAboveBaseline[iThres];

        v_ThresholdRuns[iThres]->InitNoiseRateValues();
	std::cout << v_ThresholdRuns[iThres] << std::endl;
        std::cout<<  iThres << " "<< v_ThresholdRuns[iThres]->v_NoiseRate_ElectronicChan[0][0][0][0].size()<<std::endl;

        v_ThresholdRuns[iThres]->InitHistograms();

        v_ThresholdRuns[iThres]->CreateHistDir();

        v_ThresholdRuns[iThres]->inputFileName = inputTree_;
        v_ThresholdRuns[iThres]->inputDecodedFileName = inputHistFile_;
        v_ThresholdRuns[iThres]->outputFileName = outFileName_;
        
    }

    std::cout<<"AnalyzeNoiseThres: End of construction "<<std::endl;

}

AnalyzeNoiseThres::~AnalyzeNoiseThres(){
    
    for(int iThres = 0; iThres<testThresSize; iThres++){

        delete v_ThresholdRuns[iThres];

    }

    std::cout<<"AnalyzeNoiseThres: Destructor has been called!! "<<std::endl;

}

void AnalyzeNoiseThres::Init(){

    total_quads = 1;
    total_layers = 4;
    total_types_FEBs = 2;
    total_vmms_per_sFEB = 8;
    total_vmms_per_pFEB = 3;
    total_chans_pervmm = 64;

    total_PhysicalChannels_Strip.resize(total_quads);
    total_PhysicalChannels_Wire.resize(total_quads);
    total_PhysicalChannels_Pad.resize(total_quads);
     
    for(int iQ=0; iQ<total_quads; iQ++){
     
        total_PhysicalChannels_Wire[iQ].resize(total_layers);
        total_PhysicalChannels_Pad[iQ].resize(total_layers);
   
    }

    for(int iQ=0; iQ<total_quads; iQ++){

      for(int iL=0; iL<total_layers; iL++){

	int num_Strips, num_Wires, num_Pads;
	
	if( !(a_ABMap->ReturnNStripsTotal(isSmall_, isPivot_, iQ+1, iL+1, num_Strips)) ) std::cerr<<"Cannot determine no of strips for given Quad "<< iQ+1 << " and given layer " << iL+1 << std::endl;
	if( !(a_ABMap->ReturnNWiresTotal(isSmall_, isPivot_, iQ+1, iL+1, num_Wires)) ) std::cerr<<"Cannot determine no of wires for given Quad "<< iQ+1 << " and given layer " <<iL+1 <<std::endl;
	if( !(a_ABMap->ReturnNPadsTotal(isSmall_, isPivot_, iQ+1, iL+1, num_Pads)) ) std::cerr<<"Cannot determine no of pads for given Quad "<< iQ+1 << " and given layer " <<iL+1 <<std::endl;
	
	total_PhysicalChannels_Strip[iQ] = num_Strips;
	
	total_PhysicalChannels_Wire[iQ][iL] = num_Wires;
	
	total_PhysicalChannels_Pad[iQ][iL] = num_Pads;
      }
    }

    /*total_PhysicalChannels_Strip[0]=406;
    total_PhysicalChannels_Strip[1]=365;
    total_PhysicalChannels_Strip[2]=307;

    total_PhysicalChannels_Wire[0][0]=19;
    total_PhysicalChannels_Wire[1][0]=29;
    total_PhysicalChannels_Wire[2][0]=38;
    total_PhysicalChannels_Wire[0][1]=19;
    total_PhysicalChannels_Wire[1][1]=29;
    total_PhysicalChannels_Wire[2][1]=38;
    total_PhysicalChannels_Wire[0][2]=19;
    total_PhysicalChannels_Wire[1][2]=29;
    total_PhysicalChannels_Wire[2][2]=37;
    total_PhysicalChannels_Wire[0][3]=20;
    total_PhysicalChannels_Wire[1][3]=30;
    total_PhysicalChannels_Wire[2][3]=38;

    //========= P ==========                                                                   

    if(isPivot_){
        
        std::cout << "out Pivot: " << std::endl;

        total_PhysicalChannels_Pad[0][0]=68;
        total_PhysicalChannels_Pad[1][0]=30;
        total_PhysicalChannels_Pad[2][0]=24;
        total_PhysicalChannels_Pad[0][1]=68;
        total_PhysicalChannels_Pad[1][1]=30;
        total_PhysicalChannels_Pad[2][1]=24;
        total_PhysicalChannels_Pad[0][2]=51;
        total_PhysicalChannels_Pad[1][2]=45;
        total_PhysicalChannels_Pad[2][2]=39;
        total_PhysicalChannels_Pad[0][3]=51;
        total_PhysicalChannels_Pad[1][3]=45;
        total_PhysicalChannels_Pad[2][3]=39;

    }

    //======== C =========                                                                            

    else if(!isPivot_){

        std::cout << "out Confirm: " << std::endl;
        
        total_PhysicalChannels_Pad[0][0]=72;
        total_PhysicalChannels_Pad[1][0]=45;
        total_PhysicalChannels_Pad[2][0]=42;
        total_PhysicalChannels_Pad[0][1]=72;
        total_PhysicalChannels_Pad[1][1]=45;
        total_PhysicalChannels_Pad[2][1]=42;
        total_PhysicalChannels_Pad[0][2]=68;
        total_PhysicalChannels_Pad[1][2]=48;
        total_PhysicalChannels_Pad[2][2]=39;
        total_PhysicalChannels_Pad[0][3]=68;
        total_PhysicalChannels_Pad[1][3]=48;
        total_PhysicalChannels_Pad[2][3]=39;
    */
}

void AnalyzeNoiseThres::EventLoop()
{

    for(int iThres = 0; iThres<testThresSize; iThres++){
        std::cout<<  iThres << " "<< v_ThresholdRuns[iThres]->v_NoiseRate_ElectronicChan[0][0][0][0].size()<<std::endl;

        if (v_ThresholdRuns[iThres]->fChain == 0) {std::cout << "empty tree" << std::endl; return;}
         
        Long64_t nentries = v_ThresholdRuns[iThres]->fChain->GetEntriesFast();
        std::cout << "nentries " << nentries << std::endl;
        
        Long64_t nbytes = 0, nb = 0;
        int decade = 0;
        
        //========================== Defining variables ============================//
        int event_number=0;
        
        // loop over entries of the tree
	if(!isswROD_){
	  if(!isELinkID_) v_ThresholdRuns[iThres]->NL1Packets = nentries;
	  else{
            for (Long64_t jentry=0; jentry<nentries; jentry++) {
	      
	        //=== print fraction of total events processed ====                                                                                                                                                
	        double progress = 10.0 * jentry / (1.0 * nentries);
		int k = int (progress);
		if (k > decade) std::cout << 10 * k << " %" << std::endl;
		decade = k;
              
                //===== read this entry =====                                                                                                                       
                Long64_t ientry = v_ThresholdRuns[iThres]->LoadTree(jentry);
                if (ientry < 0) break;
                nb = v_ThresholdRuns[iThres]->fChain->GetEntry(jentry);   nbytes += nb;
                
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("*",0); //disable all branches                                                                                                                                                  
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("run_number",1);
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("linkId",1);
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("level1Id",1);
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("vmmid",1);
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("channel",1);
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("nhits",1);
                //v_ThresholdRuns[iThres]->fChain->SetBranchStatus("ELinkID",1);
                
                int size_linkId = v_ThresholdRuns[iThres]->linkId->size();
                int size_vmmid = v_ThresholdRuns[iThres]->vmmid->size();
                int size_channel = v_ThresholdRuns[iThres]->channel->size();
                int size_nhits = v_ThresholdRuns[iThres]->nhits->size();
                //int size_ELinkID = ELinkID->size();
                
                //std::cout<<"Size of linkId "<< size_linkId << " vmmid " << size_vmmid << " channel " << size_channel << " nhits " << size_nhits <<std::endl;
                
                //std::cout<<"ELinkID "<<ELinkID<<std::endl;
                
                //for(int ii=0; ii<v_ThresholdRuns[iThres]->linkId->size(); ii++){
                
                std::pair<int, int> currentElinkPacketsPair = std::make_pair(v_ThresholdRuns[iThres]->ELinkID,0);
                
                if ( v_ThresholdRuns[iThres]->m_ElinkToNPackets.find(v_ThresholdRuns[iThres]->ELinkID) == v_ThresholdRuns[iThres]->m_ElinkToNPackets.end() ) {
                    v_ThresholdRuns[iThres]->m_ElinkToNPackets.insert(currentElinkPacketsPair);
                    v_ThresholdRuns[iThres]->m_ElinkToNPackets[v_ThresholdRuns[iThres]->ELinkID]++;
                }
                else
                    v_ThresholdRuns[iThres]->m_ElinkToNPackets[v_ThresholdRuns[iThres]->ELinkID]++;
              
                //}
                
            }
            
            std::cout<<"Map size "<<v_ThresholdRuns[iThres]->m_ElinkToNPackets.size()<<std::endl;
            
            for(std::map<int,int>::iterator iter = v_ThresholdRuns[iThres]->m_ElinkToNPackets.begin(); iter != v_ThresholdRuns[iThres]->m_ElinkToNPackets.end(); ++iter){
	        std::cout<<"iter: "<<iter->first<<" "<<iter->second<<std::endl;
                
                v_ThresholdRuns[iThres]->NL1Packets = iter->second;
	    }
            
	  }
        }
    }

}
    
void AnalyzeNoiseThres::PlotHists(){

    //NL1Packets = ;
    
    for(int iThres = 0; iThres<testThresSize; iThres++){

        for(int iL=0; iL<total_layers; iL++){
            
            for(int iQ=0; iQ<total_quads; iQ++){

                std::string DirectoryName_Strip = "Quad_"+std::to_string(iQ+1)+"/Layer_"+std::to_string(iL+1)+"/Strip/Strip_Hits_Vs_PhysChannel/";
                std::string HistName_Strip = "Strip_Hits_Vs_PhysChanNo_Q"+std::to_string(iQ+1)+"L"+std::to_string(iL+1);
                
                std::string DirectoryName_Pad = "Quad_"+std::to_string(iQ+1)+"/Layer_"+std::to_string(iL+1)+"/Pad/Pad_Hits_Vs_PhysChannel/";
                std::string HistName_Pad = "Pad_Hits_Vs_PhysChanNo_Q"+std::to_string(iQ+1)+"L"+std::to_string(iL+1);
         
                v_ThresholdRuns[iThres]->inFile->cd(DirectoryName_Strip.c_str());
                TH1F *h_strip_hits_vs_physchan = (TH1F*)v_ThresholdRuns[iThres]->inFile->FindObjectAny(HistName_Strip.c_str());
                
                v_ThresholdRuns[iThres]->inFile->cd(DirectoryName_Pad.c_str());
                TH1F *h_pad_hits_vs_physchan = (TH1F*)v_ThresholdRuns[iThres]->inFile->FindObjectAny(HistName_Pad.c_str());
                
                for(int iBin=1; iBin<=512; iBin++){
		  //std::cout<<"iBin "<<iBin<<std::endl;                                                                                                                                                  

		    double noiseRate_Strip = h_strip_hits_vs_physchan->GetBinContent(iBin)*pow(10,9)/(v_ThresholdRuns[iThres]->NL1Packets*200);
                    if(noiseRate_Strip==0.0) noiseRate_Strip = 0.0001;
                    double noiseRate_Pad = h_pad_hits_vs_physchan->GetBinContent(iBin)*pow(10,9)/(v_ThresholdRuns[iThres]->NL1Packets*200);
                    if(noiseRate_Pad==0.0) noiseRate_Pad = 0.0001;

		    if(noiseRate_Pad!=0.0001) std::cout << "Noise rate pad " <<  noiseRate_Pad << std::endl;

                    if(iBin<=total_PhysicalChannels_Strip[iQ]) {
                        if(iQ==0)
                            v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Fill(iBin, noiseRate_Strip);
                        else if(iQ==1)
                            v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Fill(total_PhysicalChannels_Strip[0]+iBin, noiseRate_Strip);
                        else if(iQ==2)
                            v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Fill(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+iBin, noiseRate_Strip);
                    }
                    if(iBin<=total_PhysicalChannels_Pad[iQ][iL]){
                        if(iQ==0)
                            v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Fill(iBin, noiseRate_Pad);
                        else if(iQ==1)
                            v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Fill(total_PhysicalChannels_Pad[0][iL]+iBin, noiseRate_Pad);
                        else if(iQ==2)
                            v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Fill(total_PhysicalChannels_Pad[0][iL]+total_PhysicalChannels_Pad[1][iL]+iBin, noiseRate_Pad);
                    }

                }
       
            }
            
        }


        for(int iQ=0; iQ<total_quads; iQ++){
            
            for(int iL=0; iL<total_layers; iL++){
                
                std::string DirectoryName_Strip = "Quad_"+std::to_string(iQ+1)+"/Layer_"+std::to_string(iL+1)+"/Strip/Strip_Hits_Vs_PhysChannel/";
                std::string HistName_Strip = "Strip_Hits_Vs_PhysChanNo_Q"+std::to_string(iQ+1)+"L"+std::to_string(iL+1);
                std::string DirectoryName_Wire = "Quad_"+std::to_string(iQ+1)+"/Layer_"+std::to_string(iL+1)+"/Wire/Wire_Hits_Vs_PhysChannel/";
                std::string HistName_Wire = "Wire_Hits_Vs_PhysChanNo_Q"+std::to_string(iQ+1)+"L"+std::to_string(iL+1);
                std::string DirectoryName_Pad = "Quad_"+std::to_string(iQ+1)+"/Layer_"+std::to_string(iL+1)+"/Pad/Pad_Hits_Vs_PhysChannel/";
                std::string HistName_Pad = "Pad_Hits_Vs_PhysChanNo_Q"+std::to_string(iQ+1)+"L"+std::to_string(iL+1);
                
                //v_ThresholdRuns[iThres]->inFile->cd(DirectoryName_Strip.c_str());
                //TH1F *h_strip_hits_vs_physchan = (TH1F*)v_ThresholdRuns[iThres]->inFile->FindObjectAny(HistName_Strip.c_str());
                TH1F *h_strip_hits_vs_physchan = (TH1F*)v_ThresholdRuns[iThres]->inFile->Get((DirectoryName_Strip + "/" + HistName_Strip).c_str());
                //v_ThresholdRuns[iThres]->inFile->cd(DirectoryName_Wire.c_str());
                TH1F *h_wire_hits_vs_physchan = (TH1F*)v_ThresholdRuns[iThres]->inFile->Get((DirectoryName_Wire + "/" + HistName_Wire).c_str());
                //v_ThresholdRuns[iThres]->inFile->cd(DirectoryName_Pad.c_str());
                TH1F *h_pad_hits_vs_physchan = (TH1F*)v_ThresholdRuns[iThres]->inFile->Get((DirectoryName_Pad + "/" + HistName_Pad).c_str());

		//std::cout<< "QL" << iQ << " " << iL <<  iThres << " "<< v_ThresholdRuns[iThres]->v_NoiseRate_ElectronicChan[0][0][0][0].size()<<std::endl;
                
                for(int iBin=1; iBin<=512; iBin++){
                    //std::cout<<"iBin "<<iBin<<std::endl;
		    //std::cout<< iBin << " " << iThres << " "<< v_ThresholdRuns[iThres]->v_NoiseRate_ElectronicChan[0][0][0][0].size()<<std::endl;
                    
          	    double noiseRate_Strip = h_strip_hits_vs_physchan->GetBinContent(iBin)*pow(10,9)/(v_ThresholdRuns[iThres]->NL1Packets*200);
                    if(noiseRate_Strip==0.0) noiseRate_Strip = 0.0001;
                    double noiseRate_Wire = h_wire_hits_vs_physchan->GetBinContent(iBin)*pow(10,9)/(v_ThresholdRuns[iThres]->NL1Packets*200);
                    if(noiseRate_Wire==0.0) noiseRate_Wire = 0.0001;
                    double noiseRate_Pad = h_pad_hits_vs_physchan->GetBinContent(iBin)*pow(10,9)/(v_ThresholdRuns[iThres]->NL1Packets*200);
                    if(noiseRate_Pad==0.0) noiseRate_Pad = 0.0001;
                    

                    if(iBin<=total_PhysicalChannels_Strip[iQ]) {
                        v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Fill(iBin, noiseRate_Strip);
                        v_ThresholdRuns[iThres]->v_NoiseRate_Strip_PhysChan[iQ][iL][iBin-1] = noiseRate_Strip;
                    }

                    if(iBin<=total_PhysicalChannels_Wire[iQ][iL]){
                        v_ThresholdRuns[iThres]->h_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Fill(iBin, noiseRate_Wire);
                        v_ThresholdRuns[iThres]->v_NoiseRate_Wire_PhysChan[iQ][iL][iBin-1] = noiseRate_Wire;
                    }


                    if(iBin<=total_PhysicalChannels_Pad[iQ][iL]){
                        v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Fill(iBin, noiseRate_Pad);
                        v_ThresholdRuns[iThres]->v_NoiseRate_Pad_PhysChan[iQ][iL][iBin-1] = noiseRate_Pad;

                    }
		    //std::cout << " size " << v_ThresholdRuns[iThres]->v_NoiseRate_Pad_PhysChan[iQ][iL].size() << " ";
                    //std::cout << total_PhysicalChannels_Pad[iQ][iL] << std::endl;
                    
	            //std::cout<< iBin << " " << iThres << " "<< v_ThresholdRuns[iThres]->v_NoiseRate_ElectronicChan[0][0][0][0].size()<<std::endl;

                }
                
                for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
                    
                    std::string FEBType;
                    if(iispFEB==0) FEBType = "sFEB";
                    else if(iispFEB==1) FEBType = "pFEB";
                    
                    
                    std::string DirectoryName = "Quad_"+std::to_string(iQ+1)+"/Layer_"+std::to_string(iL+1)+"/"+FEBType+"/Hits_Vs_Channel/";
                    //std::string HistName = "Quad_"+std::to_string(iQ+1)+"/Layer_"+std::to_string(iL+1)+"/"+FEBType+"/Hits_Vs_Channel/Hits_Vs_TotalChanNo_Q"+std::to_string(iQ+1)+"L"+std::to_string(iL+1)+"_"+FEBType;
                    
                    std::string HistName = "Hits_Vs_TotalChanNo_Q"+std::to_string(iQ+1)+"L"+std::to_string(iL+1)+"_"+FEBType;
                    
                    v_ThresholdRuns[iThres]->inFile->cd(DirectoryName.c_str());
                    TH1F* h_hits_vs_chan = (TH1F*)(v_ThresholdRuns[iThres]->inFile)->FindObjectAny(HistName.c_str());
                    
                    int NBins = h_hits_vs_chan->GetNbinsX();
                    
                    //std::cout<<"no of entries "<<h_hits_vs_chan->GetEntries()<<std::endl;
                    
                    for(int iBin=1; iBin<=h_hits_vs_chan->GetNbinsX(); iBin++){
		      //std::cout<<"iBin "<<iBin<<std::endl;
                        
                        double noiseRate = h_hits_vs_chan->GetBinContent(iBin)*pow(10,9)/(v_ThresholdRuns[iThres]->NL1Packets*200);
                        if(noiseRate==0.0) noiseRate = 0.0001;

                        int VMMid, Chanid;
                        
                        VMMid = (iBin-1) / 64;
                        Chanid = (iBin-1) % 64;

                        if(iispFEB==0){

			    v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB]->Fill(iBin-1, noiseRate); 
                            
                            if(VMMid<8) v_ThresholdRuns[iThres]->v_NoiseRate_ElectronicChan[iQ][iL][iispFEB][VMMid][Chanid]= noiseRate;

                        }
                        
                        else if(iispFEB==1) {
                            
                            if(VMMid<3) v_ThresholdRuns[iThres]->v_NoiseRate_ElectronicChan[iQ][iL][iispFEB][VMMid][Chanid]= noiseRate;

                            if(iBin<=64) {
                                //std::cout<<"iBin pFEB "<<iBin<<std::endl;
                                v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB]->Fill(iBin-1, noiseRate);
                                v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB]->Fill(iBin-1, noiseRate);
                            }
                            else if(iBin>64 && iBin<=192) {
                                //std::cout<<"iBin-64 pFEB "<<iBin-64<<std::endl;
                                v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB]->Fill(iBin-1, noiseRate);
                                v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB]->Fill(iBin-1, noiseRate);
                            }
                        }
                        
                       
                    }
                    
                }
                
                
            }
            
        }
        
    }
}
    
void AnalyzeNoiseThres::SaveHistograms(){
    
    Variables *VariablesObj = new Variables();

    for(int iThres = 0; iThres<testThresSize; iThres++){
        
        std::cout<<"Blahh Save Histograms "<<std::endl;
        
        //gStyle->SetOptStat(0);
        //gStyle->SetOptTitle(1);
        
        for(int iL=0; iL<total_layers; iL++){
         
            double max_Strip_NoiseRate_Vs_PhysChannelNo_perLayer = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]));
            
            v_ThresholdRuns[iThres]->c_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->cd();
            v_ThresholdRuns[iThres]->c_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->SetLogy();

            v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Draw();

            VariablesObj->StyleTLine(total_PhysicalChannels_Strip[0]+0.5, 0.0001, total_PhysicalChannels_Strip[0]+0.5, max_Strip_NoiseRate_Vs_PhysChannelNo_perLayer*1.5)->Draw("same");
            VariablesObj->StyleTLine(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+0.5, 0.0001, total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+0.5, max_Strip_NoiseRate_Vs_PhysChannelNo_perLayer*1.5)->Draw("same");
            VariablesObj->StyleTLine(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2]+0.5, 0.0001, total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2]+0.5, max_Strip_NoiseRate_Vs_PhysChannelNo_perLayer*1.5)->Draw("same");

            gPad->Update();
            TLegend *leg_strip = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
            leg_strip->AddEntry(v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo_perLayer[iL],"Strips - Noise Rate ","p");


            leg_strip->Draw("same");

            gPad->Modified();

            //======================================================================================================================================//

            double max_Pad_NoiseRate_Vs_PhysChannelNo_perLayer = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]));

            v_ThresholdRuns[iThres]->c_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->cd();
            v_ThresholdRuns[iThres]->c_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->SetLogy();

            v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL]->Draw();

	    VariablesObj->StyleTLine(total_PhysicalChannels_Pad[0][iL]+0.5, 0.0001, total_PhysicalChannels_Pad[0][iL]+0.5, max_Pad_NoiseRate_Vs_PhysChannelNo_perLayer*1.5)->Draw("same");
            //VariablesObj->StyleTLine(total_PhysicalChannels_Pad[0][iL]+total_PhysicalChannels_Pad[1][iL]+0.5, 0.0001, total_PhysicalChannels_Pad[0][iL]+total_PhysicalChannels_Pad[1][iL]+0.5, max_Pad_NoiseRate_Vs_PhysChannelNo_perLayer*1.5)->Draw("same");
            //VariablesObj->StyleTLine(total_PhysicalChannels_Pad[0][iL]+total_PhysicalChannels_Pad[1][iL]+total_PhysicalChannels_Pad[2][iL]+0.5, 0.0001, total_PhysicalChannels_Pad[0][iL]+total_PhysicalChannels_Pad[1][iL]+total_PhysicalChannels_Pad[2][iL]+0.5, max_Pad_NoiseRate_Vs_PhysChannelNo_perLayer*1.5)->Draw("same");

            gPad->Update();
            TLegend *leg_pad = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
            leg_pad->AddEntry(v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo_perLayer[iL],"Pads - Noise Rate ","p");


            leg_pad->Draw("same");

            gPad->Modified();

        }

        for(int iQ=0; iQ<total_quads; iQ++){
            
            for(int iL=0; iL<total_layers; iL++){
                
                //std::cout<<" Layer "<<iL<<std::endl;
                
                //======================================================================================================================================//

                double max_Strip_NoiseRate_Vs_PhysChannelNo = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]));

                v_ThresholdRuns[iThres]->c_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]->cd();
                v_ThresholdRuns[iThres]->c_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]->SetLogy();

                v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Draw();

                gPad->Update();
                TLegend *leg_strip = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
                leg_strip->AddEntry(v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL],"Strips - Noise Rate ","p");


                leg_strip->Draw("same");

                gPad->Modified();
                
                //======================================================================================================================================//
                
                double max_Wire_NoiseRate_Vs_PhysChannelNo = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]));

                v_ThresholdRuns[iThres]->c_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]->cd();
                v_ThresholdRuns[iThres]->c_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]->SetLogy();

                v_ThresholdRuns[iThres]->h_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Draw();

                gPad->Update();
                TLegend *leg_wire = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");

                leg_wire->AddEntry(v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL],"Wires - Noise Rate ","p");

                leg_wire->Draw("same");

                gPad->Modified();

                //======================================================================================================================================//
                
                double max_Pad_NoiseRate_Vs_PhysChannelNo = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]));

                v_ThresholdRuns[iThres]->c_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]->cd();
                v_ThresholdRuns[iThres]->c_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]->SetLogy();

                v_ThresholdRuns[iThres]->h_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Draw();

                gPad->Update();
                TLegend *leg_pad = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
                leg_pad->AddEntry(v_ThresholdRuns[iThres]->h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL],"Pads - Noise Rate ","p");

                leg_pad->Draw("same");

                gPad->Modified();

                //======================================================================================================================================//

                for(int ii=0; ii<2; ii++){
                    
                    //std::cout<<" ispFEB "<<ii<<std::endl;
                    
                    std::string FEBType;
                    if(ii==0) FEBType = "sFEB";
                    else if(ii==1) FEBType = "pFEB";
                    
                    //std::cout<<"FEBType "<<FEBType<<" ispFEB "<<ii<<std::endl;
                    
                    double max_NoiseRate_Vs_ChannelNo = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][ii]));
                    
                    v_ThresholdRuns[iThres]->c_NoiseRate_Vs_ChannelNo[iQ][iL][ii]->cd();
                    v_ThresholdRuns[iThres]->c_NoiseRate_Vs_ChannelNo[iQ][iL][ii]->SetLogy();

                    v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][ii]->Draw();
                    
                    for(int iB=1; iB<v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][ii]->GetNbinsX()/64; iB++){
                        VariablesObj->StyleTLine(iB*64-0.5, 0, iB*64-0.5, max_NoiseRate_Vs_ChannelNo*1.5)->Draw("same");
                    }
                    
                    gPad->Update();
                    TLegend *leg = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
                    if(ii==0) leg->AddEntry(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][ii],"sFEB - Noise Rate ","p");
                    else if(ii==1){
                        leg->AddEntry(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][ii],"pFEB - Noise Rate ","p");
                    }
                    
                    leg->Draw("same");
                    
                    gPad->Modified();
                    
                    //======================================================================================================================================//  

                    if(ii==1){
                        
                        double max_NoiseRate_Vs_ChannelNo_pFEB_vmm0 = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][ii]));
                        
                        v_ThresholdRuns[iThres]->c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][ii]->cd();
                        v_ThresholdRuns[iThres]->c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][ii]->SetLogy();

                        v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][ii]->Draw();
                        
                        for(int iB=1; iB<v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][ii]->GetNbinsX()/64; iB++){

                            VariablesObj->StyleTLine(iB*64-0.5, 0, iB*64-0.5, max_NoiseRate_Vs_ChannelNo_pFEB_vmm0*1.5)->Draw("same");

                        }
                        
                        gPad->Update();
                        TLegend *leg = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
                        if(ii==0) leg->AddEntry(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][ii],"sFEB - Noise Rate ","p");
                        else if(ii==1){
                            leg->AddEntry(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][ii],"pFEB - Noise Rate ","p");
                        }
                        
                        leg->Draw("same");
                        
                        gPad->Modified();

                        //======================================================================================================================================//
                        
                        double max_NoiseRate_Vs_ChannelNo_pFEB_vmm12 = VariablesObj->StyleHistogramGetMaxYTH2(*(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][ii]));
                        
                        v_ThresholdRuns[iThres]->c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][ii]->cd();
                        v_ThresholdRuns[iThres]->c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][ii]->SetLogy();

                        v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][ii]->Draw();
                        
                        for(int iB=1; iB<v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][ii]->GetNbinsX()/64; iB++){

                            VariablesObj->StyleTLine((iB+1)*64-0.5, 0, (iB+1)*64-0.5, max_NoiseRate_Vs_ChannelNo_pFEB_vmm12*1.5)->Draw("same");

                        }
                        
                        gPad->Update();
                        TLegend *leg1 = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
                        if(ii==0) leg1->AddEntry(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][ii],"sFEB - Noise Rate ","p");
                        else if(ii==1){
                            leg1->AddEntry(v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][ii],"pFEB - Noise Rate ","p");
                        }
                        
                        leg1->Draw("same");
                        
                        gPad->Modified();
                        
                        //======================================================================================================================================//  
                        
                    }
                    
                }
            }
	}
    }
    
    delete VariablesObj;
}

/*void AnalyzeNoiseThres::End() {
    
    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){
     
            oFile->cd(Strip_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());
            c_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Write("",TObject::kOverwrite);
            delete c_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            delete h_Strip_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            
            oFile->cd(Wire_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());
            c_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Write("",TObject::kOverwrite);
            delete c_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            delete h_Wire_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            
            oFile->cd(Pad_NoiseRate_Vs_PhysChannelNo_DirName[iQ][iL].c_str());
            c_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL]->Write("",TObject::kOverwrite);
            delete c_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            delete h_Pad_NoiseRate_Vs_PhysChannelNo[iQ][iL];
            
            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
                
                oFile->cd(NoiseRate_Vs_ChannelNo_DirName[iQ][iL][iispFEB].c_str());
                c_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
                delete c_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB];
                delete v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo[iQ][iL][iispFEB];
                
                if(iispFEB==1){    
                    oFile->cd(NoiseRate_Vs_ChannelNo_pFEB_vmm0_DirName[iQ][iL][iispFEB].c_str());
                    c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
                    delete c_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB];
                    delete v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm0[iQ][iL][iispFEB];
                    
                    oFile->cd(NoiseRate_Vs_ChannelNo_pFEB_vmm12_DirName[iQ][iL][iispFEB].c_str());
                    c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
                    delete c_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB];
                    delete v_ThresholdRuns[iThres]->h_NoiseRate_Vs_ChannelNo_pFEB_vmm12[iQ][iL][iispFEB];
                }
   
            }
            
        }
    }

    inFile->Close();
    oFile->Close();
    
    }*/
