// @Siyuan.Sun@cern.ch

#include "NSWNoiseAnalysis/WireChannelMap.h"

using namespace std;

WireChannelMap::WireChannelMap(){
  Init();
  verbose = false;
}

void WireChannelMap::SetVerbose(bool v) {
  verbose = v;
}

void WireChannelMap::Init() {

  std::map<std::pair<int, int>, std::pair<int,int>> ichan_map;

  for (uint ivmm=0; ivmm < 1; ivmm++ ){
    for( uint ichan = 0; ichan< 64; ichan++ ) {

      ichan_map.insert( std::make_pair(std::make_pair(ivmm, ichan), 
				       std::make_pair(-1,-1)) );

    }
  }
   
  DetectorWireMapping.insert(std::make_pair("QS1P1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS1P2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS1P3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS1P4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2P1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2P2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2P3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2P4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3P1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3P2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3P3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3P4", ichan_map));
  
  DetectorWireMapping.insert(std::make_pair("QL1P1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL1P2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL1P3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL1P4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2P1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2P2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2P3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2P4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3P1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3P2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3P3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3P4", ichan_map));
  
  DetectorWireMapping.insert(std::make_pair("QS1C1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS1C2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS1C3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS1C4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2C1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2C2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2C3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS2C4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3C1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3C2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3C3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QS3C4", ichan_map));
  
  DetectorWireMapping.insert(std::make_pair("QL1C1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL1C2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL1C3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL1C4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2C1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2C2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2C3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL2C4", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3C1", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3C2", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3C3", ichan_map));
  DetectorWireMapping.insert(std::make_pair("QL3C4", ichan_map));

  //------------------------------------------------------//

  DetectorNWiresTotal.insert( std::make_pair("QS1P1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS1P2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS1P3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS1P4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2P1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2P2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2P3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2P4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3P1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3P2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3P3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3P4", -1) );

  DetectorNWiresTotal.insert( std::make_pair("QL1P1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL1P2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL1P3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL1P4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2P1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2P2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2P3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2P4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3P1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3P2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3P3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3P4", -1) );

  //------------------------------------------------------//

  DetectorNWiresTotal.insert( std::make_pair("QS1C1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS1C2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS1C3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS1C4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2C1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2C2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2C3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS2C4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3C1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3C2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3C3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QS3C4", -1) );

  DetectorNWiresTotal.insert( std::make_pair("QL1C1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL1C2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL1C3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL1C4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2C1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2C2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2C3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL2C4", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3C1", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3C2", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3C3", -1) );
  DetectorNWiresTotal.insert( std::make_pair("QL3C4", -1) );

}

void WireChannelMap::Clear() {



  for (uint ivmm=0; ivmm < 1; ivmm++ ){
    for( uint ichan = 0; ichan< 64; ichan++ ) {

      DetectorWireMapping["QS1P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS1P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS1P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS1P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

      DetectorWireMapping["QL1P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL1P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL1P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL1P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

      DetectorWireMapping["QS1C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS1C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS1C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS1C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS2C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QS3C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

      DetectorWireMapping["QL1C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL1C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL1C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL1C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL2C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);
      DetectorWireMapping["QL3C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1,-1);

    }
  }

  //------------------------------//

  DetectorNWiresTotal["QS1P1"] = -1;
  DetectorNWiresTotal["QS1P2"] = -1;
  DetectorNWiresTotal["QS1P3"] = -1;
  DetectorNWiresTotal["QS1P4"] = -1;
  DetectorNWiresTotal["QS2P1"] = -1;
  DetectorNWiresTotal["QS2P2"] = -1;
  DetectorNWiresTotal["QS2P3"] = -1;
  DetectorNWiresTotal["QS2P4"] = -1;
  DetectorNWiresTotal["QS3P1"] = -1;
  DetectorNWiresTotal["QS3P2"] = -1;
  DetectorNWiresTotal["QS3P3"] = -1;
  DetectorNWiresTotal["QS3P4"] = -1;

  DetectorNWiresTotal["QL1P1"] = -1;
  DetectorNWiresTotal["QL1P2"] = -1;
  DetectorNWiresTotal["QL1P3"] = -1;
  DetectorNWiresTotal["QL1P4"] = -1;
  DetectorNWiresTotal["QL2P1"] = -1;
  DetectorNWiresTotal["QL2P2"] = -1;
  DetectorNWiresTotal["QL2P3"] = -1;
  DetectorNWiresTotal["QL2P4"] = -1;
  DetectorNWiresTotal["QL3P1"] = -1;
  DetectorNWiresTotal["QL3P2"] = -1;
  DetectorNWiresTotal["QL3P3"] = -1;
  DetectorNWiresTotal["QL3P4"] = -1;

  DetectorNWiresTotal["QS1C1"] = -1;
  DetectorNWiresTotal["QS1C2"] = -1;
  DetectorNWiresTotal["QS1C3"] = -1;
  DetectorNWiresTotal["QS1C4"] = -1;
  DetectorNWiresTotal["QS2C1"] = -1;
  DetectorNWiresTotal["QS2C2"] = -1;
  DetectorNWiresTotal["QS2C3"] = -1;
  DetectorNWiresTotal["QS2C4"] = -1;
  DetectorNWiresTotal["QS3C1"] = -1;
  DetectorNWiresTotal["QS3C2"] = -1;
  DetectorNWiresTotal["QS3C3"] = -1;
  DetectorNWiresTotal["QS3C4"] = -1;

  DetectorNWiresTotal["QL1C1"] = -1;
  DetectorNWiresTotal["QL1C2"] = -1;
  DetectorNWiresTotal["QL1C3"] = -1;
  DetectorNWiresTotal["QL1C4"] = -1;
  DetectorNWiresTotal["QL2C1"] = -1;
  DetectorNWiresTotal["QL2C2"] = -1;
  DetectorNWiresTotal["QL2C3"] = -1;
  DetectorNWiresTotal["QL2C4"] = -1;
  DetectorNWiresTotal["QL3C1"] = -1;
  DetectorNWiresTotal["QL3C2"] = -1;
  DetectorNWiresTotal["QL3C3"] = -1;
  DetectorNWiresTotal["QL3C4"] = -1;

}

bool WireChannelMap::SetNWireChannelMap( bool isSmall, bool isPivot,
					 int quadNum, int layerNum,
					 int n_tot_wires ) {

  bool is_okay = true;

  //-----------------------------------------------//
  //-----------------------------------------------//

  if ( !( quadNum >= 1 && quadNum <= 3 ) ) {
    is_okay = false;
  }

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
  }

  //---------------------------------------------//

  if ( !( n_tot_wires >= 1 ) ) {
    is_okay = false;
  }

  //--------------------------------------------//
  
  //-------------------------------------------------------------//                                                                                                                                     
  //      Swap 1 & 4, 2 & 3 layer number for confirm wedge                                                                                                                                              
  //   Because Benoit saves gas gap number and not layer number                                                                                                                                         
  //-------------------------------------------------------------//                                                                                                                                     
  if(isPivot == false){
    if      ( layerNum == 1 ) layerNum = 4;
    else if ( layerNum == 2 ) layerNum = 3;
    else if ( layerNum == 3 ) layerNum = 2;
    else if ( layerNum == 4 ) layerNum = 1;
    else {
      std::cout << "FATAL: No such layer number : " << layerNum << std::endl;
    }
  }

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorNWiresTotal.find(m_sx.str()) != DetectorNWiresTotal.end() ) {
    DetectorNWiresTotal [m_sx.str()] = n_tot_wires;
  }

  return is_okay;

}

bool WireChannelMap::SetWireChannelMap( int ith_vmm,  int ith_vmm_chan,
					bool isSmall, bool isPivot,
					int quadNum, int layerNum, 
					int wireNumConstruction, int wireNumAssembly ) {
  
  bool is_okay = true;

  int ichan = -1;

  int n_tot_wires;
  is_okay = ReturnNWiresTotal( isSmall, isPivot,
			       quadNum, layerNum,
			       n_tot_wires );

  //-----------------------------------------------//

  if ( !( ith_vmm == 0 ) ) {
    is_okay = false;
  }

  if      ( ith_vmm_chan >=  0 ) ichan = ith_vmm_chan % 64;

  if ( !( ichan >= 0 && ichan <= 63 ) ) {
    is_okay = false;
  }

  //-----------------------------------------------//
  
  if ( !( quadNum >= 1 && quadNum <= 3 ) ) {
    is_okay = false;
  }

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
  }

  //---------------------------------------------//

  if ( !( n_tot_wires >= 1 ) ) {
    is_okay = false;
  }

  if ( !( wireNumConstruction >= 1 && 
	  wireNumConstruction <= n_tot_wires ) && 
       wireNumConstruction != -1 ) {
    is_okay = false;
  }
  if ( !( wireNumAssembly >= 1 && 
	  wireNumAssembly <= n_tot_wires ) && 
       wireNumAssembly != -1 ) {
    is_okay = false;
  }

  //--------------------------------------------//

  if ( !is_okay ) {
    std::cout << "Mapping Failed" << std::endl;
    std::cout << "ivmm " << ith_vmm << " ichan " << ith_vmm_chan 
	      << " Quad" << quadNum << " Layer " << layerNum 
	      << " wire (construction conv.) " << wireNumConstruction
	      << " wire (assembly conv.) " << wireNumAssembly
	      << " n_tot_wires " << n_tot_wires << std::endl;
  }
  else {

    if ( verbose ) {
      std::cout << "Setting Wire Channel: ivmm " << ith_vmm << " ith_vmm_chan " << ichan;
      if ( isSmall ) std::cout << " QS ";
      else           std::cout << " QL ";
      std::cout << quadNum;
      if ( isPivot ) std::cout << " P ";
      else           std::cout << " C ";
      std::cout << layerNum;
      std::cout << " wire num (construction convention)" << wireNumConstruction << std::endl;
      std::cout << " wire num (assembly convention)" << wireNumAssembly << std::endl;
    }

    m_sx.str("");
    if ( isSmall ) m_sx << "QS" << quadNum;
    else           m_sx << "QL" << quadNum;
    if ( isPivot ) m_sx << "P"  << layerNum;
    else           m_sx << "C"  << layerNum;

    if ( DetectorWireMapping.find(m_sx.str()) != DetectorWireMapping.end() ) {
      //if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;
      DetectorWireMapping [m_sx.str()][std::make_pair(ith_vmm, ichan)] = 
	std::make_pair(wireNumConstruction, wireNumAssembly);
    }
    if ( DetectorNWiresTotal.find(m_sx.str()) != DetectorNWiresTotal.end() ) {
      //if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;
      DetectorNWiresTotal [m_sx.str()] = n_tot_wires;
    }
    else {
      std::cout << m_sx.str().c_str() << " not found " << std::endl;
    }
  }

  //------------------------------------------//

  return is_okay;

}

bool WireChannelMap::ReturnNWiresTotal( bool isSmall, bool isPivot,
					int quadNum, int layerNum,
					int &n_tot_wires ) {

  bool found = false;

  n_tot_wires = -1;

  m_sx.str("");
  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorNWiresTotal.find(m_sx.str()) != DetectorNWiresTotal.end() ) {
    if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;
    n_tot_wires = DetectorNWiresTotal[m_sx.str()];
    found = true;
  }

  return found;

}

bool WireChannelMap::ReturnWireChannelNumber( int ith_vmm,  int ith_vmm_chan,
					      bool isSmall, bool isPivot,
					      int quadNum, int layerNum, 
					      int &wireNumConstruction, int &wireNumAssembly ) {
  
  bool foundNum = false;

  std::map<std::pair<int,int>, std::pair<int,int>> ichan_map;

  m_sx.str("");
  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorWireMapping.find(m_sx.str()) != DetectorWireMapping.end() ) {

    if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;

    ichan_map = DetectorWireMapping[m_sx.str()] ;

    wireNumConstruction = ichan_map[ std::make_pair(ith_vmm, ith_vmm_chan) ].first;
    wireNumAssembly     = ichan_map[ std::make_pair(ith_vmm, ith_vmm_chan) ].second;

    foundNum = true;

  }

  return ( foundNum );

}

bool WireChannelMap::ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
								  bool isSmall, bool isPivot,
								  int quadNum, int layerNum, 
								  int wireNumConstruction, int &wireNumAssembly ) {
  
  bool found = false;

  std::map<std::pair<int,int>, std::pair<int,int>> ichan_map;

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  for ( int ivmm=0; ivmm < 8; ivmm++ ) {
    for ( int ich=0; ich < 64; ich++ ) {
      
      int iwireConstruction = -1;
      int iwireAssembly = -1;

      bool ifound = ReturnWireChannelNumber( ivmm, ich,
					     isSmall, isPivot,
					     quadNum, layerNum,
					     iwireConstruction, iwireAssembly );

      if ( ifound && iwireConstruction == wireNumConstruction ) {
	ith_vmm = ivmm;
	ith_vmm_chan = ich;
	wireNumAssembly = iwireAssembly;
	found = true;
	break;
      }
    } // loop over chan

    if ( found ) break;

  }

  if ( !found ) {
    ith_vmm = -1;
    ith_vmm_chan = -1;
    wireNumAssembly = -1;
  }

  return found;

}

bool WireChannelMap::ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
							      bool isSmall, bool isPivot,
							      int quadNum, int layerNum,
							      int &wireNumConstruction, int wireNumAssembly ) {

  bool found = false;

  std::map<std::pair<int,int>, std::pair<int,int>> ichan_map;

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  for ( int ivmm=0; ivmm < 8; ivmm++ ) {
    for ( int ich=0; ich < 64; ich++ ) {

      int iwireConstruction = -1;
      int iwireAssembly = -1;

      bool ifound = ReturnWireChannelNumber( ivmm, ich,
                                             isSmall, isPivot,
                                             quadNum, layerNum,
                                             iwireConstruction, iwireAssembly );

      if ( ifound && iwireAssembly == wireNumAssembly ) {
        ith_vmm = ivmm;
        ith_vmm_chan = ich;
        wireNumConstruction = iwireConstruction;
        found = true;
        break;
      }
    } // loop over chan                                                                                                                                                                           

    if ( found ) break;

  }

  if ( !found ) {
    ith_vmm = -1;
    ith_vmm_chan = -1;
    wireNumConstruction = -1;
  }

  return found;

}
