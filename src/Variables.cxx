#define Variables_cxx

#include "NSWNoiseAnalysis/Variables.h"
#include <TH2.h>
#include <TStyle.h>
#include <math.h>

//#include "recipeAUX/OxbridgeMT2/interface/Basic_Mt2_332_Calculator.h"
//#include "recipeAUX/OxbridgeMT2/interface/ChengHanBisect_Mt2_332_Calculator.h"

//#include "SusyAnaTools/TopTagger/interface/Type3TopTagger.h"

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
//#include "Math/VectorUtil.h"
#include "TRandom.h"

#include <iostream>
#include <cstdio>
#include <string>
#include <ctime>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TApplication.h"

#include <fstream>
#include <cstdint>
#include <sys/stat.h>

#include <tuple>

using namespace std;

double Variables::DeltaPhi(double phi1, double phi2) {
  double result = phi1 - phi2;
  while (result > M_PI)    result -= 2 * M_PI;
  while (result <= -M_PI)  result += 2 * M_PI;
  return result;
}

double Variables::DeltaR(double eta1, double phi1, double eta2, double phi2) {
  double deta = eta1 - eta2;
  double dphi = DeltaPhi(phi1, phi2);
  return std::sqrt(deta*deta + dphi*dphi);
}

double Variables::HT (std::vector<TLorentzVector> vjets) {
  double ht = 0.0;
  for( unsigned int ijet=0; ijet<vjets.size(); ijet++) {    
    if( vjets[ijet].Pt()>50.0 && std::abs(vjets[ijet].Eta())<2.5 ) 
      ht += vjets[ijet].Pt();
  }
  return ht;
}

TLorentzVector Variables::MHT(std::vector<TLorentzVector> vjets) {;
  TLorentzVector mht(0.0, 0.0, 0.0, 0.0);
  for( unsigned int ijet=0; ijet<vjets.size(); ijet++) {    
    if( vjets[ijet].Pt()>30.0 && std::abs(vjets[ijet].Eta())<5.0 ) 
      mht -= vjets[ijet];
  }

  return mht;
}

double Variables::CosAngle(TLorentzVector Lep, TLorentzVector V) {
  TLorentzVector Lep_VFrame = Lep;
  Double_t Angle_LepVrest_V, CosAngle_LepVrest_V;
  Lep_VFrame.Boost((-1)*V.BoostVector());
  Angle_LepVrest_V=(Lep_VFrame.Vect()).Angle(V.Vect());
  CosAngle_LepVrest_V=cos(Angle_LepVrest_V);
  return CosAngle_LepVrest_V;
}

TLorentzVector Variables::E_Smear(TLorentzVector Par, Double_t perc_sm){
  TLorentzVector Par_ESm;
  Par_ESm=Par;
  Par_ESm.SetE(Par.E()*(1+(perc_sm*gRandom->Gaus(0,1)/200)));
  Double_t Ratio_E_NewToOld=Par_ESm.E()/Par.E();
  //============ Assuming higher energy regime where masses of leptons do not matter ==========//
  Par_ESm.SetPx(Par.Px()*Ratio_E_NewToOld);
  Par_ESm.SetPy(Par.Py()*Ratio_E_NewToOld);
  Par_ESm.SetPz(Par.Pz()*Ratio_E_NewToOld);
  return Par_ESm;    
}

double Variables::StyleHistogramGetMaxYTH2(TH2F & Hist){
    
    Hist.GetXaxis()->SetTitleSize(0.045);
    Hist.GetXaxis()->SetLabelSize(0.045);
    Hist.GetYaxis()->SetTitleSize(0.045);
    Hist.GetYaxis()->SetLabelSize(0.045);
    Hist.GetXaxis()->SetTitleOffset(1);
    Hist.GetYaxis()->SetTitleOffset(1);
    Hist.SetMarkerSize(1);
    Hist.GetXaxis()->SetTitle("VMM*64 + ChannelNo");
    Hist.GetYaxis()->SetTitle("Noise Rate (kHz)");
    Hist.SetMarkerColor(kBlue);
    Hist.SetLineColor(kBlue);
    Hist.SetMarkerStyle(kFullSquare);
    
    TProfile *ProjectToX_Hist = Hist.ProfileX("ProjectToX_Hist",      1, -1, "");
    
    Hist.GetYaxis()->SetRangeUser(0.0001,ProjectToX_Hist->GetBinContent(ProjectToX_Hist->GetMaximumBin())*1.5);

    return ProjectToX_Hist->GetMaximum();
    
}

TLine * Variables::StyleTLine(double x1, double y1, double x2, double y2){
    
    TLine *myline1 = new TLine(x1, y1, x2, y2);
    myline1->SetLineColor(kRed);
    myline1->SetLineWidth(1.5);
    myline1->SetLineStyle(1);

    return myline1;
    delete myline1;

}
