#define Combine_AllThresRuns_cxx
#include "NSWNoiseAnalysis/Combine_AllThresRuns.h"
#include "NSWNoiseAnalysis/AnalyzeNoiseThres.h"
#include "NSWNoiseAnalysis/Variables.h"
#include "NSWNoiseAnalysis/ABMapping.h"
#include "NSWNoiseAnalysis/PadChannelMap.h"
#include "NSWNoiseAnalysis/WireChannelMap.h"
#include "NSWNoiseAnalysis/StripChannelMap.h"

Combine_AllThresRuns::Combine_AllThresRuns(const char *commonName, const char *inputRunName, const char *decodedRunName, const char* outputRunName, const char* outputSCurve, const char *inputThres, const char *SorL, const char *PorC, const char *sisELinkID, const char *sisswROD) {

    m_ABMap = new ABMapping();
    m_ABMap->SetVerbose(false);
    m_ABMap->LoadBenoitMapping();
    
    s_commonName = commonName;
    s_inputRunName = inputRunName;
    s_decodedRunName = decodedRunName;
    s_outputRunName = outputRunName;
    s_outputSCurve = outputSCurve;
    s_SCurveDir = s_outputSCurve;
    size_t pos = s_SCurveDir.find(s_commonName);
    if (pos != std::string::npos)
      {
        s_SCurveDir.erase(pos, s_commonName.length());
      }

    s_SorL = SorL;
    s_PorC = PorC;

    if(s_SorL=="S") isSmall=true;
    else if(s_SorL=="L") isSmall=false;
    else std::cout<<"Please provide S or L as third argument"<<std::endl;

    std::cout << SorL << " " << s_SorL << " " << isSmall << std::endl;

    if(s_PorC=="P") isPivot=true;
    else if(s_PorC=="C") isPivot=false;
    else std::cout<<"Please provide P or C as fourth argument"<<std::endl;

    std::cout << PorC << " " << s_PorC << " " << isPivot << std::endl;

    s_isELinkID = sisELinkID;
    if(s_isELinkID == "1") isELinkID = true;
    else if(s_isELinkID == "0") isELinkID = false;
    else std::cerr << "Please provide the 9th argument as 'ELinkID 1 or no-ELinkID 0.'" << std::endl;

    std::cout << "ELinkID : " << s_isELinkID << " bool " << isELinkID <<std::endl;

    VMM_Colors.resize(8);
    VMM_Colors[0] = kBlue;
    VMM_Colors[1] = kRed;
    VMM_Colors[2] = kCyan;
    VMM_Colors[3] = kOrange-3;
    VMM_Colors[4] = kMagenta;
    VMM_Colors[5] = kGreen+2;
    VMM_Colors[6] = kBlack;
    VMM_Colors[7] = kGreen;
    
    AnalyzeNoiseThres_Obj_ = new AnalyzeNoiseThres(inputRunName,decodedRunName,outputRunName,inputThres, isSmall, isPivot, isELinkID, isswROD);
    
    AnalyzeNoiseThres_Obj_->EventLoop();
    AnalyzeNoiseThres_Obj_->PlotHists();
    AnalyzeNoiseThres_Obj_->SaveHistograms();

    v_ThresholdRunInfo_Obj_ = AnalyzeNoiseThres_Obj_->v_ThresholdRuns;

    numThresAboveBaseline = v_ThresholdRunInfo_Obj_.size();

    v_ThresAboveBaseline_ = AnalyzeNoiseThres_Obj_->v_ThresAboveBaseline;

    total_quads = AnalyzeNoiseThres_Obj_->total_quads;
    total_layers = AnalyzeNoiseThres_Obj_->total_layers;
    total_types_FEBs = AnalyzeNoiseThres_Obj_->total_types_FEBs;
    total_vmms_per_sFEB = AnalyzeNoiseThres_Obj_->total_vmms_per_sFEB;
    total_vmms_per_pFEB = AnalyzeNoiseThres_Obj_->total_vmms_per_pFEB;
    total_chans_pervmm = AnalyzeNoiseThres_Obj_->total_chans_pervmm;

    total_PhysicalChannels_Strip = AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Strip;
    total_PhysicalChannels_Wire = AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Wire;
    total_PhysicalChannels_Pad = AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Pad;

    std::string outFileName = outputSCurve;
    outFileName = outFileName+"_SCurve.root";
    std::cout<<"SCurve output file: "<<outFileName<<std::endl;
    outputFile = new TFile(outFileName.c_str(), "recreate");
    
}

Combine_AllThresRuns::~Combine_AllThresRuns(){

  delete outputFile;

  delete AnalyzeNoiseThres_Obj_;

  std::cout<<"Combine_AllThresRuns: Destructor has been called!"<<std::endl;

}


void Combine_AllThresRuns::InitPlots(){

    g_Strip_SCurves_perPhysChannel.resize(total_quads);
    c_Strip_SCurves_perPhysChannel.resize(total_quads);
    g_Wire_SCurves_perPhysChannel.resize(total_quads);
    c_Wire_SCurves_perPhysChannel.resize(total_quads);
    g_Pad_SCurves_perPhysChannel.resize(total_quads);
    c_Pad_SCurves_perPhysChannel.resize(total_quads);
    g_SCurves_perElecChannel.resize(total_quads);
    c_SCurves_perElecChannel.resize(total_quads);
    
    c_Overlay_SCurves_perVmm.resize(total_quads);
    c_Overlay_SCurves_Strips.resize(total_quads);
    c_Overlay_SCurves_Pads.resize(total_quads);
    c_Overlay_SCurves_perFEB_unconnected.resize(total_quads);

    quadDirName.resize(total_quads);
    LayerDirName.resize(total_quads);
    febTypeDirName.resize(total_quads);
    vmmDirName.resize(total_quads);
    SCurves_ElecChanDirName.resize(total_quads);
    OverlaySCurevs_DirName.resize(total_quads);
    
    StripDirName.resize(total_quads);
    WireDirName.resize(total_quads);
    PadDirName.resize(total_quads);
    StripSCurves_PhysChanDirName.resize(total_quads);
    WireSCurves_PhysChanDirName.resize(total_quads);
    PadSCurves_PhysChanDirName.resize(total_quads);

    for(int iQ=0; iQ<total_quads; iQ++){
     
        g_Strip_SCurves_perPhysChannel[iQ].resize(total_layers);
        c_Strip_SCurves_perPhysChannel[iQ].resize(total_layers);
        g_Wire_SCurves_perPhysChannel[iQ].resize(total_layers);
        c_Wire_SCurves_perPhysChannel[iQ].resize(total_layers);
        g_Pad_SCurves_perPhysChannel[iQ].resize(total_layers);
        c_Pad_SCurves_perPhysChannel[iQ].resize(total_layers);
        g_SCurves_perElecChannel[iQ].resize(total_layers);
        c_SCurves_perElecChannel[iQ].resize(total_layers);
        
        c_Overlay_SCurves_perVmm[iQ].resize(total_layers);
        c_Overlay_SCurves_Strips[iQ].resize(total_layers);
        c_Overlay_SCurves_Pads[iQ].resize(total_layers);
        c_Overlay_SCurves_perFEB_unconnected[iQ].resize(total_layers);

        LayerDirName[iQ].resize(total_layers);
        febTypeDirName[iQ].resize(total_layers);
        vmmDirName[iQ].resize(total_layers);
        SCurves_ElecChanDirName[iQ].resize(total_layers);
        OverlaySCurevs_DirName[iQ].resize(total_layers);

        StripDirName[iQ].resize(total_layers);
        WireDirName[iQ].resize(total_layers);
        PadDirName[iQ].resize(total_layers);
        StripSCurves_PhysChanDirName[iQ].resize(total_layers);
        WireSCurves_PhysChanDirName[iQ].resize(total_layers);
        PadSCurves_PhysChanDirName[iQ].resize(total_layers);

        for(int iL=0; iL<total_layers; iL++){
            
            g_Strip_SCurves_perPhysChannel[iQ][iL].resize(AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Strip[iQ]);
            c_Strip_SCurves_perPhysChannel[iQ][iL].resize(AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Strip[iQ]);
            g_Wire_SCurves_perPhysChannel[iQ][iL].resize(AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Wire[iQ][iL]);
            c_Wire_SCurves_perPhysChannel[iQ][iL].resize(AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Wire[iQ][iL]);
            g_Pad_SCurves_perPhysChannel[iQ][iL].resize(AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Pad[iQ][iL]);
            c_Pad_SCurves_perPhysChannel[iQ][iL].resize(AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Pad[iQ][iL]);
            
            c_Overlay_SCurves_perVmm[iQ][iL].resize(total_types_FEBs);
            c_Overlay_SCurves_perFEB_unconnected[iQ][iL].resize(total_types_FEBs);

            g_SCurves_perElecChannel[iQ][iL].resize(total_types_FEBs);
            c_SCurves_perElecChannel[iQ][iL].resize(total_types_FEBs);
            
            febTypeDirName[iQ][iL].resize(total_types_FEBs);
            vmmDirName[iQ][iL].resize(total_types_FEBs);
            SCurves_ElecChanDirName[iQ][iL].resize(total_types_FEBs);
            OverlaySCurevs_DirName[iQ][iL].resize(total_types_FEBs);

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
             
                int numVMMs;
                if(iispFEB==0) numVMMs = total_vmms_per_sFEB;
                else if(iispFEB==1) numVMMs = total_vmms_per_pFEB;
                
                g_SCurves_perElecChannel[iQ][iL][iispFEB].resize(numVMMs);
                c_SCurves_perElecChannel[iQ][iL][iispFEB].resize(numVMMs);

                c_Overlay_SCurves_perVmm[iQ][iL][iispFEB].resize(numVMMs);

                vmmDirName[iQ][iL][iispFEB].resize(numVMMs);
                SCurves_ElecChanDirName[iQ][iL][iispFEB].resize(numVMMs);
                OverlaySCurevs_DirName[iQ][iL][iispFEB].resize(numVMMs);

                for(int iVMM=0; iVMM<numVMMs; iVMM++){

                    g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
                    c_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
                    
                }
   
            }
            
        }
   
    }

    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){
            
            std::string name_Strip_SCurve, name_Wire_SCurve, name_Pad_SCurve;

            //std::cout<<"Blahhh InitPlots"<<std::endl;
            
            std::string c_name_Overlay_SCurves_Strips, c_name_Overlay_SCurves_Pads;
            
            c_name_Overlay_SCurves_Strips = "Strips_Overlay_SCurves_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_SFEB";
            c_Overlay_SCurves_Strips[iQ][iL] = new TCanvas(c_name_Overlay_SCurves_Strips.c_str(),c_name_Overlay_SCurves_Strips.c_str(),800,800);
            
            c_name_Overlay_SCurves_Pads = "Pads_Overlay_SCurves_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_PFEB";
            c_Overlay_SCurves_Pads[iQ][iL] = new TCanvas(c_name_Overlay_SCurves_Pads.c_str(),c_name_Overlay_SCurves_Pads.c_str(),800,800);

            for(int iPhysChan = 0; iPhysChan<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Strip[iQ]; iPhysChan++){
                
                //std::cout<<iPhysChan<<" Blahh "<<std::endl;
                
                name_Strip_SCurve = "Strip_SCurve_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_PhysChan"+std::to_string(iPhysChan+1);
                gPad->SetLogy();
                g_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan] = new TGraph(numThresAboveBaseline);
                g_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetName(name_Strip_SCurve.c_str());
                //g_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetDirectory(0);
                
                c_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan] = new TCanvas(name_Strip_SCurve.c_str(), name_Strip_SCurve.c_str(), 800, 800);
                //c_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetDirectory(0);
                
            }
 
            for(int iPhysChan = 0; iPhysChan<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Wire[iQ][iL]; iPhysChan++){
                
                name_Wire_SCurve = "Wire_SCurve_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_PhysChan"+std::to_string(iPhysChan+1);
                gPad->SetLogy();
                g_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan] = new TGraph(numThresAboveBaseline);
                g_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetName(name_Wire_SCurve.c_str());
                //g_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetDirectory(0);
                
                c_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan] = new TCanvas(name_Wire_SCurve.c_str(), name_Wire_SCurve.c_str(), 800, 800);
                //c_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetDirectory(0);

            }

            for(int iPhysChan = 0; iPhysChan<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Pad[iQ][iL]; iPhysChan++){
                
                name_Pad_SCurve = "Pad_SCurve_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_PhysChan"+std::to_string(iPhysChan+1);
                gPad->SetLogy();
                g_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan] = new TGraph(numThresAboveBaseline);
                g_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetName(name_Pad_SCurve.c_str());
                //g_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetDirectory(0);
                
                c_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan] = new TCanvas(name_Pad_SCurve.c_str(), name_Pad_SCurve.c_str(), 800, 800);
                //c_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetDirectory(0);
                
            }
            
            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                std::string FEBType;
                int numVMMs;
                if(iispFEB==0) {numVMMs = total_vmms_per_sFEB; FEBType="sFEB";}
                else if(iispFEB==1) {numVMMs = total_vmms_per_pFEB; FEBType="pFEB";}

                std::string c_name_Overlay_SCurves_perFEB_unconnected;
                
                c_name_Overlay_SCurves_perFEB_unconnected = "Overlay_SCurves_UnconnectedChans_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType;
                c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB] = new TCanvas(c_name_Overlay_SCurves_perFEB_unconnected.c_str(),c_name_Overlay_SCurves_perFEB_unconnected.c_str(),800,800);

                for(int iVMM=0; iVMM<numVMMs; iVMM++){
                    
                    std::string name_Overlay_SCurves_ElecChan;
                    
                    name_Overlay_SCurves_ElecChan = "SCurves_Overlay_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_vmm"+std::to_string(iVMM);
                    
                    c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM] = new TCanvas(name_Overlay_SCurves_ElecChan.c_str(), name_Overlay_SCurves_ElecChan.c_str(), 800, 800);

                    for(int iChan=0; iChan<total_chans_pervmm; iChan++){
                        
                        std::string name_ElecChan_SCurve;
                        
                        name_ElecChan_SCurve = "SCurve_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_vmm"+std::to_string(iVMM)+"_Chan"+std::to_string(iChan);
                        gPad->SetLogy();
                        g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan] = new TGraph(numThresAboveBaseline);
                        g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan]->SetName(name_ElecChan_SCurve.c_str());
                        //g_SCurves_perElecChannel[iQ][iL][iChan]->SetDirectory(0);

                        c_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan] = new TCanvas(name_ElecChan_SCurve.c_str(), name_ElecChan_SCurve.c_str(), 800, 800);
                        //c_SCurves_perElecChannel[iQ][iL][iChan]->SetDirectory(0);
                        
                    }
                }
            }
       
        }
        
    }

}

void Combine_AllThresRuns::InitDirectories(){

    for(int iQ=0; iQ<total_quads; iQ++){

        quadDirName[iQ] = "Quad_"+std::to_string(iQ+1)+"/";
        outputFile->mkdir(quadDirName[iQ].c_str());

        for(int iL=0; iL<total_layers; iL++){
            
            LayerDirName[iQ][iL] = quadDirName[iQ]+"Layer_"+std::to_string(iL+1)+"/";
            outputFile->mkdir(LayerDirName[iQ][iL].c_str());

            StripDirName[iQ][iL] = LayerDirName[iQ][iL]+"Strip/";
            outputFile->mkdir(StripDirName[iQ][iL].c_str());
            StripSCurves_PhysChanDirName[iQ][iL] = StripDirName[iQ][iL]+"SCurve_PhysChannels/";
            outputFile->mkdir(StripSCurves_PhysChanDirName[iQ][iL].c_str());
            
            WireDirName[iQ][iL] = LayerDirName[iQ][iL]+"Wire/";
            outputFile->mkdir(WireDirName[iQ][iL].c_str());
            WireSCurves_PhysChanDirName[iQ][iL] = WireDirName[iQ][iL]+"SCurve_PhysChannels/";
            outputFile->mkdir(WireSCurves_PhysChanDirName[iQ][iL].c_str());
            
            PadDirName[iQ][iL] = LayerDirName[iQ][iL]+"Pad/";
            outputFile->mkdir(PadDirName[iQ][iL].c_str());
            PadSCurves_PhysChanDirName[iQ][iL] = PadDirName[iQ][iL]+"SCurve_PhysChannels/";
            outputFile->mkdir(PadSCurves_PhysChanDirName[iQ][iL].c_str());

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                std::string FEBType;
                if(iispFEB==0) FEBType = "sFEB";
                else if(iispFEB==1) FEBType = "pFEB";

                std::cout<<"FEB "<<FEBType<<std::endl;
                
                int numVMMs;
                if(iispFEB==0) numVMMs = total_vmms_per_sFEB;
                else if(iispFEB==1) numVMMs = total_vmms_per_pFEB;
                
                febTypeDirName[iQ][iL][iispFEB] = LayerDirName[iQ][iL]+FEBType+"/";
                outputFile->mkdir(febTypeDirName[iQ][iL][iispFEB].c_str());

                for(int iVMM=0; iVMM<numVMMs; iVMM++){
                 
                    

                    vmmDirName[iQ][iL][iispFEB][iVMM] = febTypeDirName[iQ][iL][iispFEB]+"VMM_"+std::to_string(iVMM)+"/";
                    outputFile->mkdir(vmmDirName[iQ][iL][iispFEB][iVMM].c_str());
                    
                    SCurves_ElecChanDirName[iQ][iL][iispFEB][iVMM] = vmmDirName[iQ][iL][iispFEB][iVMM]+"SCurve_ElecChannels/";
                    outputFile->mkdir(SCurves_ElecChanDirName[iQ][iL][iispFEB][iVMM].c_str());

                    OverlaySCurevs_DirName[iQ][iL][iispFEB][iVMM] = vmmDirName[iQ][iL][iispFEB][iVMM]+"SCurves_Overlay_ElecChannels/";
                    outputFile->mkdir(OverlaySCurevs_DirName[iQ][iL][iispFEB][iVMM].c_str());
                    

                }

            }
            
        }
        
    }

    std::cout<<"End of InitDirectories()"<<std::endl;

}

void Combine_AllThresRuns::PlotSCurves(){
    
    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){
            
            for(int iPhysChan = 0; iPhysChan<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Strip[iQ]; iPhysChan++){
                
                for(int iThres=0; iThres<numThresAboveBaseline; iThres++){

                    g_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetPoint(iThres, v_ThresAboveBaseline_[iThres], v_ThresholdRunInfo_Obj_[iThres]->v_NoiseRate_Strip_PhysChan[iQ][iL][iPhysChan]);
                    
                }
                
            }
            
            for(int iPhysChan = 0; iPhysChan<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Wire[iQ][iL]; iPhysChan++){

                for(int iThres=0; iThres<numThresAboveBaseline; iThres++){

                    g_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetPoint(iThres, v_ThresAboveBaseline_[iThres], v_ThresholdRunInfo_Obj_[iThres]->v_NoiseRate_Wire_PhysChan[iQ][iL][iPhysChan]);

                }

            }
            
            for(int iPhysChan = 0; iPhysChan<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Pad[iQ][iL]; iPhysChan++){

                for(int iThres=0; iThres<numThresAboveBaseline; iThres++){

                    g_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan]->SetPoint(iThres, v_ThresAboveBaseline_[iThres], v_ThresholdRunInfo_Obj_[iThres]->v_NoiseRate_Pad_PhysChan[iQ][iL][iPhysChan]);
                    
                }

            }
            
            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                int numVMMs;
                if(iispFEB==0) numVMMs = total_vmms_per_sFEB;
                else if(iispFEB==1) numVMMs = total_vmms_per_pFEB;

                for(int iVMM=0; iVMM<numVMMs; iVMM++){

                    for(int iChan=0; iChan<total_chans_pervmm; iChan++){
             
                        for(int iThres=0; iThres<numThresAboveBaseline; iThres++){

			  //std::cout << "Thre above baseline::" << v_ThresAboveBaseline_[iThres] << std::endl;

                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan]->SetPoint(iThres, v_ThresAboveBaseline_[iThres], v_ThresholdRunInfo_Obj_[iThres]->v_NoiseRate_ElectronicChan[iQ][iL][iispFEB][iVMM][iChan]);

                        }
                        
                    }
                }
            }
            
        }
    }


    
}

void Combine_AllThresRuns::SavePlots(){
    
    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){

            for(int iPhysChan = 0; iPhysChan<g_Strip_SCurves_perPhysChannel[iQ][iL].size(); iPhysChan++){
                
                SetAttGraph( (*g_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan]), 0.75, (*c_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChan]) );

            }
            
            for(int iPhysChan = 0; iPhysChan<g_Wire_SCurves_perPhysChannel[iQ][iL].size(); iPhysChan++){

                SetAttGraph( (*g_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan]), 0.75, (*c_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChan]) );

            }
            
            for(int iPhysChan = 0; iPhysChan<g_Pad_SCurves_perPhysChannel[iQ][iL].size(); iPhysChan++){

                SetAttGraph( (*g_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan]), 0.75, (*c_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChan]) );

            }
            
            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
                
                int numVMMs;
                std::string FEBType;
                if(iispFEB==0) { numVMMs = total_vmms_per_sFEB; FEBType = "sFEB"; }
                else if(iispFEB==1) { numVMMs = total_vmms_per_pFEB; FEBType = "pFEB"; }

                for(int iVMM=0; iVMM<numVMMs; iVMM++){
                     
                    for(int iChan=0; iChan<total_chans_pervmm; iChan++){

                        SetAttGraph( (*g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan]), 0.75, (*c_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan]) );

                    }
   
                }
                
            }

        }

    }

    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){
            
            c_Overlay_SCurves_Strips[iQ][iL]->cd();
            c_Overlay_SCurves_Strips[iQ][iL]->SetLogy(1);
            TH1F *h_strip = c_Overlay_SCurves_Strips[iQ][iL]->DrawFrame(-55, 0.00001, 500, 10000000, "");
            h_strip->GetXaxis()->SetTitle("Threshold Above Baseline+30mV (DAC Count)");
	    h_strip->GetXaxis()->SetRangeUser(-29.5,500.5);
            h_strip->GetYaxis()->SetTitle("Noise Rate (Hz)");
            h_strip->GetYaxis()->SetTitleOffset(1);
            TLegend * leg_Strip = new TLegend(0.7,0.5,0.9,0.9,"","brNDC");

            c_Overlay_SCurves_Pads[iQ][iL]->cd();
            c_Overlay_SCurves_Pads[iQ][iL]->SetLogy(1);
            TH1F *h_pad = c_Overlay_SCurves_Pads[iQ][iL]->DrawFrame(-55, 0.00001, 500, 10000000, "");
            h_pad->GetXaxis()->SetTitle("Threshold Above Baseline+30mV (DAC Count)");
	    h_pad->GetXaxis()->SetRangeUser(-29.5,500.5);
            h_pad->GetYaxis()->SetTitle("Noise Rate (Hz)");
            h_pad->GetYaxis()->SetTitleOffset(1);
            TLegend * leg_Pad = new TLegend(0.7,0.5,0.9,0.9,"","brNDC");

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                int numVMMs;
                std::string FEBType;
                if(iispFEB==0) { numVMMs = total_vmms_per_sFEB; FEBType = "sFEB"; }
                else if(iispFEB==1) { numVMMs = total_vmms_per_pFEB; FEBType = "pFEB"; }

                c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB]->cd();
                c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB]->SetLogy(1);
                TH1F *h_FEB = c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB]->DrawFrame(-55, 0.00001, 500, 10000000, "");
                h_FEB->GetXaxis()->SetTitle("Threshold Above Baseline (DAC Count)");
		h_FEB->GetXaxis()->SetRangeUser(-29.5,500.5);
                h_FEB->GetYaxis()->SetTitle("Noise Rate (Hz)");
                h_FEB->GetYaxis()->SetTitleOffset(1);
                TLegend * leg_UnConn = new TLegend(0.7,0.5,0.9,0.9,"","brNDC");

                for(int iVMM=0; iVMM<numVMMs; iVMM++){

                    gStyle->SetOptStat(0);
                    gStyle->SetOptTitle(1);

                    for(int iG = 0; iG < g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM].size(); iG++){

                        g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerSize(0.75);

                        // Setting styles                                                                                                                                                                                   

                        //int factor = iG / 8;

                        /*if(factor == 0){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(20);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(1);
                        }
                        else if(factor == 1){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(21);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(2);
                        }
                        else if(factor == 2){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(22);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(3);
                        }
                        else if(factor == 3){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(47);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(4);
                        }
                        else if(factor == 4){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(24);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(5);
                        }
                        else if(factor == 5){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(25);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(6);
                        }
                        else if(factor == 6){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(26);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(9);
                        }
                        else if(factor == 7){
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerStyle(46);
                            g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineStyle(10);
                            }*/

                        // Setting colors                                                                                                                                                                                   

                        //int factor = iG ;

			if(iVMM == 0){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(1);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(1);
                        }
                        else if(iVMM == 1){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(2);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(2);
                        }
                        else if(iVMM == 2){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(3);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(3);
                        }
                        else if(iVMM == 3){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(4);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(4);
                        }
                        else if(iVMM == 4){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(5);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(5);
                        }
                        else if(iVMM == 5){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(6);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(6);
                        }
                        else if(iVMM == 6){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(7);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(7);
                        }
                        else if(iVMM == 7){
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerColor(28);
                          g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetLineColor(28);
                        }

                        if(iG==0 && iispFEB==0){
			  std::string legName = "SCurve - VMM"+std::to_string(iVMM);
                          leg_Strip->AddEntry(g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG], legName.c_str(), "l");
                          leg_UnConn->AddEntry(g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG], legName.c_str(), "l");
                        }
                        if(iG==0 && iispFEB==1){
			  if((iVMM==1 || iVMM==2)){
			    std::string legName = "SCurve - VMM"+std::to_string(iVMM);
			    leg_Pad->AddEntry(g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG], legName.c_str(), "l");
			  }
			  std::string legName = "SCurve - VMM"+std::to_string(iVMM);
                          leg_UnConn->AddEntry(g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG], legName.c_str(), "l");
                        }

			if(iispFEB==0){
			  if(m_ABMap->IsConnected_sFEB( iVMM,  iG, isSmall, isPivot, iQ+1, iL+1 )){
			    //std::cout << "sFEB Channel Connected " << iVMM << " " << iG <<std::endl;
			    c_Overlay_SCurves_Strips[iQ][iL]->cd();
			    g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->Draw("L");
			    gPad->Update();
			  }
			  else{
			    c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB]->cd();
			    g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->Draw("L");
			    gPad->Update();
			  }
                        }
                        else if(iispFEB==1){
                          if(m_ABMap->IsConnected_pFEB( iVMM,  iG, isSmall, isPivot, iQ+1, iL+1 ) && (iVMM==1 || iVMM==2)){
			    c_Overlay_SCurves_Pads[iQ][iL]->cd();
			    g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->Draw("L");
			    gPad->Update();
			  }
                          else if(!m_ABMap->IsConnected_pFEB( iVMM,  iG, isSmall, isPivot, iQ+1, iL+1 )){
			    c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB]->cd();
			    g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->Draw("L");
			    gPad->Update();
			  }

                        }

		    }

		}
		
		c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB]->cd();
                //leg_UnConn->Draw("same");

		std::string Overlay_Canvas_FEB_Name = s_SCurveDir+"c_SCurves_Overlay_unconnected_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+".png";
                c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB]->Print( Overlay_Canvas_FEB_Name.c_str() );
		
	    }
	    
	    c_Overlay_SCurves_Strips[iQ][iL]->cd();
            //leg_Strip->Draw("same");

	    std::string Overlay_Canvas_Strips_Name = s_SCurveDir+"c_SCurves_Overlay_Strips_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+".png";
            c_Overlay_SCurves_Strips[iQ][iL]->Print( Overlay_Canvas_Strips_Name.c_str() );

            c_Overlay_SCurves_Pads[iQ][iL]->cd();
            //leg_Pad->Draw("same");

	    std::string Overlay_Canvas_Pads_Name = s_SCurveDir+"c_SCurves_Overlay_Pads_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+".png";
            c_Overlay_SCurves_Pads[iQ][iL]->Print( Overlay_Canvas_Pads_Name.c_str() );

	}
    }

    for(int iQ=0; iQ<total_quads; iQ++){

       for(int iL=0; iL<total_layers; iL++){

	  for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

	     int numVMMs;
	     std::string FEBType;
	     if(iispFEB==0) { numVMMs = total_vmms_per_sFEB; FEBType = "sFEB"; }
	     else if(iispFEB==1) { numVMMs = total_vmms_per_pFEB; FEBType = "pFEB"; }
	     
	     for(int iVMM=0; iVMM<numVMMs; iVMM++){
	       
	        gStyle->SetOptStat(0);
		gStyle->SetOptTitle(1);
		
		std::cout<<"XMax "<<g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][0]->GetX()[(g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][0]->GetXaxis()->GetNbins() - 1)] << " YMax " << g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][0]->GetY()[(g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][0]->GetYaxis()->GetNbins() - 1)] << std::endl;
		
		TLegend * leg = new TLegend(0.7,0.5,0.9,0.9,"","brNDC");
		
		c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->cd();
		c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->SetLogy(1);
		TH1F *h = c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->DrawFrame(-55, 0.00001, 500, 10000000, "");
		h->GetXaxis()->SetTitle("Threshold Above Baseline (DAC Count)");
		h->GetXaxis()->SetRangeUser(-29.5,500.5);
		h->GetYaxis()->SetTitle("Noise Rate (Hz)");
		h->GetYaxis()->SetTitleOffset(1);
		
		for(int iG = 0; iG < g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM].size(); iG++){

		   g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->SetMarkerSize(0.75);

		   TGraph *Current_TGraph = g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG];
		   
		   if(iG/8 == 0){
		     Current_TGraph->SetMarkerColor(1);
		     Current_TGraph->SetLineColor(1);
		   }
		   else if(iG/8 == 1){
		     Current_TGraph->SetMarkerColor(2);
		     Current_TGraph->SetLineColor(2);
		   }
		   else if(iG/8 == 2){
		     Current_TGraph->SetMarkerColor(3);
		     Current_TGraph->SetLineColor(3);
		   }
		   else if(iG/8 == 3){
		     Current_TGraph->SetMarkerColor(4);
		     Current_TGraph->SetLineColor(4);
		   }
		   else if(iG/8 == 4){
		     Current_TGraph->SetMarkerColor(5);
		     Current_TGraph->SetLineColor(5);
		   }
		   else if(iG/8 == 5){
		     Current_TGraph->SetMarkerColor(6);
		     Current_TGraph->SetLineColor(6);
		   }
		   else if(iG/8 == 6){
		     Current_TGraph->SetMarkerColor(7);
		     Current_TGraph->SetLineColor(7);
		   }
		   else if(iG/8 == 7){
		     Current_TGraph->SetMarkerColor(28);
		     Current_TGraph->SetLineColor(28);
		   }
		   
		   c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->cd();
		   if(iG == 0) Current_TGraph->Draw("L");
		   else Current_TGraph->Draw("L");
		   //g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG]->Draw("L");
                   
		   gPad->Update();
                   
		   std::string legendName = "SCurve - Channel "+std::to_string(iG);  
                   
		   if(iG % 8 == 0){
		     std::string legName = "SCurve - VMM"+std::to_string(iVMM)+"_Channels"+std::to_string(iG)+"TO"+std::to_string(iG+7);
		     leg->AddEntry(Current_TGraph, legName.c_str(), "l");
		     //leg->AddEntry(g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iG], legName.c_str(), "l");
		   }
		   
		}
                
		c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->cd();
		//leg->Draw("same");
		
		//gPad->Modified();
		
		std::string Overlay_Canvas_Name;
                
		Overlay_Canvas_Name = s_SCurveDir+"c_SCurves_Overlay_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_vmm"+std::to_string(iVMM)+".png";
		
		c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->Print( Overlay_Canvas_Name.c_str() );
		
	     }
	     
	  }
	  
       }
       
    }
}

void Combine_AllThresRuns::End(){

    for(int iQ=0; iQ<total_quads; iQ++){

        for(int iL=0; iL<total_layers; iL++){

            outputFile->cd(StripSCurves_PhysChanDirName[iQ][iL].c_str());

            std::cout<<iQ<<" "<<iL<<" "<<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Strip[iQ]<<std::endl;

            std::cout<<g_Strip_SCurves_perPhysChannel[iQ][iL].size()<<std::endl;

            for(int iPhysChanS = 0; iPhysChanS<g_Strip_SCurves_perPhysChannel[iQ][iL].size(); iPhysChanS++){

                c_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChanS]->Write();
                delete c_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChanS];
                delete g_Strip_SCurves_perPhysChannel[iQ][iL][iPhysChanS];

            }

            outputFile->cd(WireSCurves_PhysChanDirName[iQ][iL].c_str());

            for(int iPhysChanW = 0; iPhysChanW<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Wire[iQ][iL]; iPhysChanW++){

                c_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChanW]->Write("",TObject::kOverwrite);
                delete c_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChanW];
                delete g_Wire_SCurves_perPhysChannel[iQ][iL][iPhysChanW];

            }

            outputFile->cd(PadSCurves_PhysChanDirName[iQ][iL].c_str());

            for(int iPhysChanP = 0; iPhysChanP<AnalyzeNoiseThres_Obj_->total_PhysicalChannels_Pad[iQ][iL]; iPhysChanP++){

                c_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChanP]->Write("",TObject::kOverwrite);
                delete c_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChanP];
                delete g_Pad_SCurves_perPhysChannel[iQ][iL][iPhysChanP];

            }
	    outputFile->cd();
	    c_Overlay_SCurves_Strips[iQ][iL]->Write("",TObject::kOverwrite);
	    c_Overlay_SCurves_Pads[iQ][iL]->Write("",TObject::kOverwrite);
            delete c_Overlay_SCurves_Strips[iQ][iL];
            delete c_Overlay_SCurves_Pads[iQ][iL];

            for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){

                delete c_Overlay_SCurves_perFEB_unconnected[iQ][iL][iispFEB];

                if(iispFEB==0){
                    for(int iVMM=0; iVMM<total_vmms_per_sFEB; iVMM++){

                        outputFile->cd(SCurves_ElecChanDirName[iQ][iL][iispFEB][iVMM].c_str());

                        for(int iChan=0; iChan<total_chans_pervmm; iChan++){

                            c_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
                            delete c_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan];
                            delete g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan];

                        }
                        
                        outputFile->cd(OverlaySCurevs_DirName[iQ][iL][iispFEB][iVMM].c_str());
                        c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->Write("",TObject::kOverwrite);
                        delete c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM];
                        

                    }


                }
                
                else if(iispFEB==1){
                    for(int iVMM=0; iVMM<total_vmms_per_pFEB; iVMM++){

                        outputFile->cd(SCurves_ElecChanDirName[iQ][iL][iispFEB][iVMM].c_str());

                        for(int iChan=0; iChan<total_chans_pervmm; iChan++){

                            c_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
                            delete c_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan];
                            delete g_SCurves_perElecChannel[iQ][iL][iispFEB][iVMM][iChan];

                        }
                        
                        outputFile->cd(OverlaySCurevs_DirName[iQ][iL][iispFEB][iVMM].c_str());
                        c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM]->Write("",TObject::kOverwrite);
                        delete c_Overlay_SCurves_perVmm[iQ][iL][iispFEB][iVMM];

                    }


                }

            }
        }
    }
    
    outputFile->Close();

}

void Combine_AllThresRuns::SetAttGraph(TGraph & graph, Size_t MarkerSize, TCanvas & Canvas){

    graph.GetXaxis()->SetTitle("Threshold Above Baseline (DAC Count)");
    graph.GetXaxis()->SetRangeUser(-29.5,200.5);
    graph.GetYaxis()->SetTitle("Noise Rate (Hz)");
    graph.SetMarkerSize(MarkerSize);
    graph.SetMarkerStyle(21);
    graph.SetMarkerColor(kBlue);
    graph.SetLineStyle(1);
    graph.SetLineColor(kBlue);
    
    std::string graphName = graph.GetName();
    
    Canvas.cd();
    graph.Draw();
    
    TLegend * leg = new TLegend(0.7,0.7,0.9,0.9,"","brNDC");
    leg->AddEntry(graphName.c_str(), graphName.c_str(), "p");
    leg->Draw("same");

}

void Combine_AllThresRuns::OverlaySCurves(std::vector<TGraph *> v_graphs, TCanvas & OverlayCanvas){
    
    TLegend * leg = new TLegend(0.7,0.7,0.9,0.9,"","brNDC");

    for(int iG = 0; iG < v_graphs.size(); iG++){
        
        v_graphs[iG]->GetXaxis()->SetTitle("Threshold Above Baseline (DAC Count)");
	v_graphs[iG]->GetXaxis()->SetRangeUser(-29.5,200.5);
        v_graphs[iG]->GetYaxis()->SetTitle("Noise Rate (Hz)");
        v_graphs[iG]->SetMarkerSize(0.75);
        
        // Setting styles

        int factor = iG / 8;

        if(factor == 0){
            v_graphs[iG]->SetMarkerStyle(20);
            v_graphs[iG]->SetLineStyle(1);
        }
        else if(factor == 1){
            v_graphs[iG]->SetMarkerStyle(21);
            v_graphs[iG]->SetLineStyle(2);
        }
        else if(factor == 2){
            v_graphs[iG]->SetMarkerStyle(22);
            v_graphs[iG]->SetLineStyle(3);
        }
        else if(factor == 3){
            v_graphs[iG]->SetMarkerStyle(47);
            v_graphs[iG]->SetLineStyle(4);
        }
        else if(factor == 4){
            v_graphs[iG]->SetMarkerStyle(24);
            v_graphs[iG]->SetLineStyle(5);
        }
        else if(factor == 5){
            v_graphs[iG]->SetMarkerStyle(25);
            v_graphs[iG]->SetLineStyle(6);
        }
        else if(factor == 6){
            v_graphs[iG]->SetMarkerStyle(26);
            v_graphs[iG]->SetLineStyle(9);
        }
        else if(factor == 7){
            v_graphs[iG]->SetMarkerStyle(46);
            v_graphs[iG]->SetLineStyle(10);
        }
        
        // Setting colors

        int remainder = iG % 8;

        if(remainder == 0){
            v_graphs[iG]->SetMarkerColor(1);
            v_graphs[iG]->SetLineColor(1);
        }
        else if(remainder == 1){
            v_graphs[iG]->SetMarkerColor(2);
            v_graphs[iG]->SetLineColor(2);
        }
        else if(remainder == 2){
            v_graphs[iG]->SetMarkerColor(3);
            v_graphs[iG]->SetLineColor(3);
        }
        else if(remainder == 3){
            v_graphs[iG]->SetMarkerColor(4);
            v_graphs[iG]->SetLineColor(4);
        }
        else if(remainder == 4){
            v_graphs[iG]->SetMarkerColor(5);
            v_graphs[iG]->SetLineColor(5);
        }
        else if(remainder == 5){
            v_graphs[iG]->SetMarkerColor(6);
            v_graphs[iG]->SetLineColor(6);
        }
        else if(remainder == 6){
            v_graphs[iG]->SetMarkerColor(7);
            v_graphs[iG]->SetLineColor(7);
        }
        else if(remainder == 7){
            v_graphs[iG]->SetMarkerColor(28);
            v_graphs[iG]->SetLineColor(28);
        }
        
        if(iG == 0) v_graphs[iG]->Draw();
        else v_graphs[iG]->Draw("same");

    }
    
}
